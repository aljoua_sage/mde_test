
#include "_common.h"


//------------------------------------------------------------------------------- 
// Permet d'ex�cuter une commande (batch ou lancer un programme...);
// Et attend la fin du programme avant de passer � la suite 
//-------------------------------------------------------------------------------
void P_Exec_Commande(TStringTST Cmd) {
#if 0
    WshShell = CreateObject("WScript.Shell");
    Call WshShell.Run (Cmd, 1, true);
#endif
}

//------------------------------------------------------------------------------- 
// Recherche si un r�pertoire existe   
//-------------------------------------------------------------------------------
bool F_Find_Folder(TStringTST Path) {
#if 0
    objfso = CreateObject("Scripting.FileSystemObject");
    if( objfso.FolderExists(Path) ) {
        //log.message("Le r�pertoire exite d�j�");
        return true;
    }
#endif
    return false;
}

//------------------------------------------------------------------------------- 
// Recherche si un fichier existe   
//-------------------------------------------------------------------------------
bool F_Find_File(TStringTST Path) {
#if 0
    objfso = CreateObject("Scripting.FileSystemObject");
    if( objfso.FileExists(Path) ) {
        //log.message("Le fichier existe d�j�");
        return true;
    }
#endif
    return false;
}

//------------------------------------------------------------------------------- 
// Cr�ation d'un r�pertoire    
//-------------------------------------------------------------------------------
void P_Create_Folder( TStringTST Path) {
#if 0
    if( ! F_Find_Folder(Path) ) {
        Log.Message "Le r�pertoire n'exite pas on va le cr�er"
        
        objfso = CreateObject("Scripting.FileSystemObject");
        objfso.CreateFolder(Path);
    }
#endif
}

//------------------------------------------------------------------------------- 
// Suppression d'un fichier 
//-------------------------------------------------------------------------------
void P_Del_File( TStringTST Path) {
#if 0
    objfso = CreateObject("Scripting.FileSystemObject");
    if( objfso.FileExists(Path) ) {
        //log.message("On supprime le fichier");
        objfso.DeleteFile(Path);
    }//  
#endif
}

//------------------------------------------------------------------------------- 
// Remplace une chaine par une autre dans un fichier 
//-------------------------------------------------------------------------------
void P_Replace_Text_File(TStringTST Path, TStringTST Old_Text, TStringTST New_Text) {
#if 0
    //log.message("Replace_Text_File");
    //D�claration de constante pour la lecture et l'�criture
    const_js<int> ForReading = 1
    const_js<int> ForWriting = 2

    //On ouvre le fichier en lecture
    objFSO = CreateObject("Scripting.FileSystemObject");
    objFile = objFSO.OpenTextFile(Path, ForReading);
    //On stock le contenu du fichier dans une variable
    strText = objFile.ReadAll
    
    //On ferme le fichier
    objFile.Close

    //On remplace les chaines 
    strNewText = Replace(strText, Old_Text, New_Text);
    //On ouvre le fichier en �criture
    objFile = objFSO.OpenTextFile(Path, ForWriting);
    //On �crit dans le fichier
    objFile.WriteLine strNewText
    
    //On ferme le fichier
    objFile.Close
#endif
}

//------------------------------------------------------------------------------- 
// Regarde si une chaine est pr�sente dans un fichier  
//-------------------------------------------------------------------------------
bool F_Find_Text_File(TStringTST Path, TStringTST Text) {
#if 0
    //log.message("F_Find_Text_File");
    
    //D�claration de constante pour la lecture et l'�criture
    const_js<int> ForReading = 1
    const_js<int> ForWriting = 2

    objFSO = CreateObject("Scripting.FileSystemObject");
    objFile = objFSO.OpenTextFile(Path, ForReading);
    strFichier = objFile.ReadAll

    if( InStr(1,strFichier, Text,1) != 0 ) {
        //log.message("On a trouv� la chaine");
        objFile.Close
        return true;
    }

    objFile.Close
#endif
    return false;

}

//------------------------------------------------------------------------------- 
// Modification de la date systhme 
//-------------------------------------------------------------------------------
void P_MAJ_DateHours_System(TStringTST Date_Sys, TStringTST hours_Sys) {
#if 0
    //Modification de la date systhme du poste au xx/xx/xx
    
    //On passe par un raccourci de CMD pour forcer l'ex�cution en ligne de commande en mode administrateur
    //necessaire pour modifier la date. 
    Machaine = "cmd.exe /C date " & Date_Sys
    //Machaine = "C:\PR_Test\cmd.exe.lnk /C date " & Date_Sys
    P_Exec_Commande(Machaine);;
    Machaine = "cmd.exe /C time " & Hours_Sys
    //Machaine = "C:\PR_Test\cmd.exe.lnk /C time " & Hours_Sys
    P_Exec_Commande(Machaine);;
#endif
}

//------------------------------------------------------------------------------- 
// Synchronisation de la date systhme 
//-------------------------------------------------------------------------------
void P_Sync_DateHours_System() { 
#if 0
    //Modification de la date systhme du poste au xx/xx/xx
    Machaine = "cmd.exe /C net time //yes"
    P_Exec_Commande(Machaine);
#endif
}



