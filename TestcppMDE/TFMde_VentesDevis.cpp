//*****************************************************************************************
// TESTS FONCTIONNELS pour l'activit� d'�dition des DEVIS
//
//*****************************************************************************************

#include "MdeUI_Export.h"

#include "MdeUI_Menu_Unit.cpp"
//USEUNIT MdeUI_Menu_Unit

#include "MdeAppli_Unit.cpp"
//USEUNIT MdeAppli_Unit

#include "JsTestEngine.cpp"
//USEUNIT JsTestEngine

#include "MdeUI_Listes_Unit.cpp"
//USEUNIT MdeUI_Listes_Unit



//*******************************************************************************************
// Ouverture de la liste des devis
void TF_Vente000_Open_dlgDevisList() {

    // On click dans le menu Ventes/DevisQuantEstim
    mdeHeadMenu.menuVentes.smenuDevisQuantEstim.click();

    // On check la bonne fin de l'action
    checkFailAction( mdeHeadMenu.menuVentes.smenuDevisQuantEstim.action );
}


//*******************************************************************************************
// S/P pour ouvrir cr�er et ouvrir un devis par copie
bool SP_OpenNewDevis_byCopy( TCodeTST _codeDup) {
 
    if (!dlgDevisList.win.Exists) {
        // On ouvre la dlgBox "Devis List"
        TF_Vente000_Open_dlgDevisList();
    }

    // On filtre la liste par code ou ref, ce qui donne une seule ligne selectionn�e
    // Rmq: l'article est s�lectionn� et "copiable" m�me s'il n'apparait pas dans la liste en 
    // raison des autres filtres (p�riode par exemple)
    liste_Select_CodeOrRef(dlgDevisList.win, _codeDup );

    // On clique sur le bouton copier 
    dlgDevisList.bt_Copier.click();

    // On v�rifie que le doc devis est ouvert
    varI<bool> bErr = dlgDevisDocu.checkIsOpen();
    if (bErr) return 1;

    return 0;
}

//*******************************************************************************************
// S/P pour fermer la dlgbox "liste des devis"
bool SP_CloseDlgBox_DevisList() {

    // On ferme la liste des devis
    dlgDevisList.bt_Quitter.click();

    // Attente de la fermeture de la liste des devis
    return dlgDevisList.checkIsClosed();
}

//*******************************************************************************************
// S/P pour Valider puis enregistrer et fermer un devis
bool SP_ValidateAndCloseDocDevis() {

    // Validation du nouveau devis  (provoquera l'enregistrement puis le close du doc devis )
    dlgDevisDocu.bt_Valider.click();

    // Attente de la fermeture du devis
    varI<bool> bErr = dlgDevisDocu.checkIsClosed();
    if(bErr) return 1;

    // On ferme la liste des devis
    return SP_CloseDlgBox_DevisList();
}




//*******************************************************************************************
// Copie simple d'un Devis
void TF_Vente001_Copie_Valide_Devis() {   

    // Creation et ouverture d'un devis par copy
    varI<bool> bErr = SP_OpenNewDevis_byCopy("00000001");
    if(bErr) return;

    // On Valide le nouveau devis  (provoquera l'enregistrement puis le close du doc devis )
    SP_ValidateAndCloseDocDevis();

    // On ferme la liste des devis
    SP_CloseDlgBox_DevisList();
}



//*******************************************************************************************
// Copie simple d'un Devis + changement du statut vers "Accept�"
void TF_Vente003_Copie_Devis_ChangeStatus() { 

    // Creation et ouverture d'un devis par copy
    varI<bool> bErr = SP_OpenNewDevis_byCopy("00000001");
    if (bErr) return;
     
    // Modification du statut
    dlgDevisDocu.edt_Status.hitKeys("a");

    // Cela d�clenche une dialogBox d'Avertissement
    bErr = dlgAvertissement.checkIsOpen();
    if (bErr) return;

    // On r�pond Confirmer, ce qui ferme la dlgBox d'Avertissement
    dlgAvertissement.bt_Confirmer.click();

    bErr = dlgAvertissement.checkIsClosed();
    if (bErr) return;

    // On attend que le Bt enregistrer de la fiche soit Enabled = false preuve que la fiche est enregistr�e
    bErr = dlgDevisDocu.btIcon_Enregistrer.checkWin();
    if (bErr) return;
    bErr = ! dlgDevisDocu.btIcon_Enregistrer.win.WaitProperty("Enabled", false, 10000);
    if (bErr) {
       Log.Error("Le devis ne s'est pas enregistr�");
    }

    // Apr�s l'enregistrement, on peut quitter (sans valider)
    delay(300);
    dlgDevisDocu.bt_Quitter.click();
    // Attente de la fermeture du devis
    bErr = dlgDevisDocu.checkIsClosed();
    if (bErr) return;

    // On ferme la dlgBox Liste de devis
    SP_CloseDlgBox_DevisList();

}


// END OF FILE ********************************************************************************
