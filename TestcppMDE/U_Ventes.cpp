//USEUNIT U_Commun

#include "_common.h"

#include "JavaScriptExtens.cpp"  // for toStr...
//USEUNIT JavaScriptExtens

//-------------------------------------------------------------------------------
void T_Ventes_0001() {  //Copie et validation d'un devis
        
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste, "00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    // Validation de la fiche
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0002() {  //Copie et enregistrement d'un devis
    
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,"00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    // Validation de la fiche
    P_Enregistre_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------

void T_Ventes_0003() {  //Copie d'un devis + changement de statut
    
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,"00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    // Modification du statut
    var<TWinTST> Saisie = mafiche.VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat");
    Saisie.Keys("a");
    //Validation du message d'avertissement
    var<TWinTST> Message = F_Message("*","Avertissement");
    F_Message_BtClic( Message,"Confirmer");
    //On attend que le Bt enregistrer de la fiche soit Enabled = false preuve que la fiche est enregistr�e
    var<TWinTST> Bt = F_DocVte_BarIcone_GetBt(mafiche,"Enregistrer");
    if( ! Bt.WaitProperty("Enabled", false) ) {
        Log.Error( "Le devis ne s'est pas enregistr�" );
    }  
    
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------

void T_Ventes_0004() {  //Copie et impression d'un devis
    
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,"00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    // Lancement de l'apergu depuis la fiche
    var<TWinTST> Bouton = F_DocVte_BarIconeBtClic(mafiche,"Apercu");
    // Validation du message d'avertissement
    var<TWinTST> Message = F_Message("*","Enregistrer");
    F_Message_BtClic(Message,"Oui");
    // Fermeture de l'apergu
    var<TWinTST> Appercu = F_Appercu("*");
    F_Appercu_BtClic(Appercu,"Quitter");
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0005() {  //Enregistrement de devis & Export BCD
        
    // On supprime le fichier d'export s'il existe
    P_Del_File("C:\\test_export_bcd.bcd");
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,"00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    // On clique sur le menu Imprimer / Envoyer / Exporter
    var<TWinTST> apibatMenuToolbar = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("tbMenu");
    apibatMenuToolbar.ClickItem("Imp&rimer", false);
    apibatMenuToolbar.PopupMenu.Click("[3]|[9]");
    // Confirmation du message 
    var<TWinTST> Message = F_Message("*","Devis");
    F_Message_BtClic(Message,"Oui");
    //On attend que la fen�tre de recherche de fichier s'ouvre
    var<TWinTST> explorateur = Sys.Process("mde").WaitWindow("#32770", "Recherche fichiers d'export", -1, 10000);
    if( ! explorateur.WaitProperty("Visible", true) ) {
        Log.Error( "L'explorateur ne s'est pas ouvert");
    }
      
    Sys.Keys("c:\\");
    delay(100);
    Sys.Keys("[Enter]");
    delay(200);
    Sys.Keys("test_export_bcd");
    delay(200);
    Sys.Keys("[Enter]");
    // Validation message Export termin�
    Message = F_Message("*","Devis");
    F_Message_BtClic(Message,"Ok");
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0006() {  //Import de devis BCD

    // Si le fichier n'est pas l?, il faut le cr�er
    if( ! F_Find_File("C:\\test_export_bcd.bcd") ) {
        T_Ventes_0005();
    } 
           
    //On lance l'option d'import de fichier BCD
    Sys.Process("mde").VCLObject("fmMenu").MainMenu.Click("[11]|[5]|[8]|[1]");
    var<TWinTST> mafiche = F_Fiche("*","Import");
    Sys.Keys("[F4]");
    //On attend que la fen�tre de recherche de fichier s'ouvre
    var<TWinTST> Explorateur = Sys.Process("mde").WaitWindow("#32770", "Recherche d'archive", -1, 1000);
    if( !Explorateur.WaitProperty("Visible", true) ) {
        Log.Error( "L'explorateur ne s'est pas ouvert" );
    }
      
    Sys.Keys("c:\\");
    delay(100);
    Sys.Keys("[Enter]");
    delay(200);
    Sys.Keys("test_export_bcd.bcd");
    delay(200);
    Sys.Keys("[Enter]");
    delay(500);
    Sys.Keys("[Enter][Enter]");
    delay(1000);
    Sys.Keys(" ");
    Sys.Keys("[F2]");
    // Message Confirmation Import
    var<TWinTST> Message = F_Message("*","Import");
    F_Message_BtClic(Message,"Oui");
    // Message Confirmation Import termin�
    Message = F_Message("*","Import");
    F_Message_BtClic(Message,"Ok");
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0007() {  //Enregistrement par saisie d'un acompte
        
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,"00000001");
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> tfmDocDevCli = stdScrollBox.Window("TfmDocDevCli", "*");
    var<TWinTST> apibatPageControl = tfmDocDevCli.VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet");
    //On va sur le pied
    Sys.Keys("~^p");
    var<TWinTST> apiPanel = apibatPageControl.VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedDroit");
    var<TWinTST> stdSais = apiPanel.VCLObject("panPiedDroitHaut").VCLObject("panAcompteMt").VCLObject("saiAcompte");
    // On saisi le montant de l'acompte
    stdSais.Keys("100[Enter]");
    // On valide le message de confirmation de l'enregistrement 
    mde.Window("TApiMessageDlg", "Enregistrer le document").Window("TApiPanel").Window("TStdBout", "", 2).Click(33, 11);
    // On quitte la fen�tre qui propose d'imprimer
    stdScrollBox.Window("TEdtListeDocFacAcompte", "Edition facture d'acompte").VCLObject("Panel").VCLObject("btQuitter").Click(35, 15);
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0008() {  //Enregistre un devis sans ligne
    
    // Cr�ation du devis
    P_Creat_Devis();
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0009() {  //Enregistre Devis avec insertion de ligne Manuelle/liste/Ref associ�e
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Insertion de lignes saisie manuellement
    P_Ins_Lig_Manuel();
    // Insertion de lignes depuis la liste
    P_Ins_Lig_Liste();
    // Insertion de lignes avec reference associ�es
    P_Ins_Lig_Ref_Ass();
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0010() {  //Enregistre Devis import de section & Copier coller
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Import de section de doc
    P_Ins_Lig_Imp_Sec();
    // Copier / coller de ligne
    P_Ins_Lig_Copier_Coller();
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0011() {  //Enregistre Devis avec import de phase type
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    delay(200);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0012() {  //Enregistre Devis MAJ dans toute l'�tude
    
    // Cr�ation du devis
    P_Creat_Devis();
    // On insert 2 fois les m?mes lignes
    P_Ins_Lig_Manuel();
    P_Ins_Lig_Manuel();
    //On revient sur le dernier ouvrage
    Sys.Keys("[Up]");
    delay(200);
    //On diveloppe le dernier ouvrage
    Sys.Keys("^[NumPlus]");
    delay(200);
    //On active la modif dans toute l'�tude
    Sys.Keys("~o");
    delay(100);
    Sys.Keys("[Up][Enter]");
    delay(100);
    //on se positionne sur la 1er composante de l'ouvrage
    Sys.Keys("[Down]");
    delay(100);
    //on ajoute une ligne 
    Sys.Keys("[F9]");
    delay(100);
    //on tape MAT01 dans la r�f�rence et fait fl?che bas
    Sys.Keys("MAT01");
    delay(100);
    Sys.Keys("[Down]");
    //On valide le message
    var<TWinTST> MonMessage = F_Message("TfmMsgConfirmModifyAllEtudeDlg","Avertissement");
    F_Message_BtClic(MonMessage,"Oui");
    //On desactive la modif dans toute l'�tude
    Sys.Keys("~o");
    delay(100);
    Sys.Keys("[Up][Enter]");
    delay(100);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0013() {  //Enregistre Devis type avenant
    
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[6]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    stdScrollBox.Window("TStdFormListe", "Devis clients : Coordonn�es et montants").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 10).Click(9, 11);
    var<TWinTST> apiPanel = stdScrollBox.Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete");
    var<TWinTST> stdSais = apiPanel.VCLObject("panEnTeteRef").VCLObject("panEnTeteType").VCLObject("saiType");
    stdSais.Click(26, 8);
    stdSais.Keys("a");
    stdSais = apiPanel.VCLObject("panEnTeteDevInit").VCLObject("saiCodeDevInit");
    stdSais.Click(33, 7);
    stdSais.Keys("00000001[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0014() {  //Enregistre Devis avec MO de pose
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert l'�l�ment Fpose et on se positionne sur la ligne suivante dans la colonne r�f�rence 
    Sys.Keys("Fpose");
    delay(200);
    Sys.Keys("[Down]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0015() {  //Enregistre Devis avec Analytique sur ligne
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert l'�l�ment Fpose et on se positionne sur la ligne suivante dans la colonne r�f�rence 
    Sys.Keys("MAT01");
    delay(200);
    Sys.Keys("[Enter]");
    // On renseigne les grilles ana
    var<TWinTST> stdScrollBox = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl");
    stdScrollBox.Window("TfmDocDevCli", "*").VCLObject("StdPanel1").VCLObject("btAnalytique").Click(15, 13);
    var<TWinTST> tfrmVentilAna = stdScrollBox.Window("TfrmVentilAna", "*");
    tfrmVentilAna.VCLObject("panAxe").VCLObject("saiAxe").Keys("[F4][Down][Enter][Tab]");
    var<TWinTST> stdScroll = tfrmVentilAna.VCLObject("ApiPanel1").VCLObject("scrAna");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("001[Tab]");
    var<TWinTST> scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "0,00");
    scrollSaisieEdit2.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit3 = stdScroll.Window("TScrollSaisieEdit", "", 1);
    scrollSaisieEdit3.Keys("50[Tab]");
    scrollSaisieEdit.Keys("002[Tab]");
    scrollSaisieEdit2.Keys("[Tab]");
    scrollSaisieEdit3.Keys("30[Tab]");
    scrollSaisieEdit.Keys("ATE[Tab]");
    scrollSaisieEdit2.Keys("[Tab]");
    var<TWinTST> stdPanel = tfrmVentilAna.VCLObject("panbottom");
    stdPanel.VCLObject("btSolder").Click(41, 16);
    stdPanel.VCLObject("btValider").Click(41, 15);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0016() {  //Enregistre devis avec mitri sur l'ent�te et les lignes
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert l'�l�ment Fpose et on se positionne sur la ligne suivante dans la colonne r�f�rence 
    Sys.Keys("MAT01");
    delay(200);
    Sys.Keys("[Enter]");
    //On ouvre la fen�tre du mitri g�n�ral
    Sys.Keys("^[F8]");
    //On se positionne dans la colonne d�signation
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi Long = 10
    Sys.Keys("Long = 10");
    delay(200);
    //On Valide
    Sys.Keys("[F2]");
    delay(200);
    //On fait F8 pour entrer sur le mitri de la ligne
    Sys.Keys("[F8]");
    //On se positionne dans la colonne d�signation
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi Larg = 5
    Sys.Keys("Larg = 5");
    delay(200);
    //On fait fleche bas et tab pour se positionner sur la ligne suivantes
    Sys.Keys("[Down][Tab]");
    delay(200);
    //On saisi Long * Larg
    Sys.Keys("Long * Larg");
    delay(200);
    //On fait fleche bas et t insirer une ligne de type totalisation en dessous
    Sys.Keys("[Down]t");
    delay(200);
    //On Valide
    Sys.Keys("[F2]");
    delay(200);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0017() {  //Enregistre devis avec actualisation dans les lignes et dans le pied
    
    // Cr�ation du devis
    P_Creat_Devis();
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert l'�l�ment Fpose et on se positionne sur la ligne suivante dans la colonne r�f�rence 
    Sys.Keys("MAT01");
    delay(200);
    Sys.Keys("[Enter]");
    //On ouvre la fen�tre propri�t� de la ligne
    Sys.Keys("[F5]");
    //On va sur l'onglet actualisation
    Sys.Keys("~r");
    delay(200);
    //On saisi une date et on va sur le libell�
    Sys.Keys("010100");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un libelle et on va sur coef
    Sys.Keys("test actu lig");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un coef et on ferme la fen�tre de propri�t�
    Sys.Keys("1.2");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[Esc]");
    delay(200);
    //On va sur le pied dans analyse
    Sys.Keys("~da");
    delay(200);
    //Normalement on a le focus sur le scroll du haut
    //Pour arriver sur le scroll du bas on fait un truc pas jojo (mais pas mieux pour le moment);
    //On se positionne sur le scroll des actu du pied
    Sys.Keys("^[End]");
    delay(200);
    Sys.Keys("[Down][Down][Down][Down]");
    delay(200);
    //On saisi une date et on va sur le libell�
    Sys.Keys("010100");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un libelle et on va sur coef
    Sys.Keys("test actu pied");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un coef et on ferme la fen�tre de propri�t�
    Sys.Keys("1.5");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On retourne sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0018() {  //Enregistre devis avec Variation
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    // On va dans le pied du doc et on force un montant de 4000 
    var<TWinTST> apibatPageControl = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet");
    apibatPageControl.VCLObject("tsLignes").VCLObject("panLignes").VCLObject("panCorps").VCLObject("panScroll").VCLObject("scLignes").Window("TScrollRichEdit", "", 1).Keys("~^p");
    apibatPageControl.VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedTot").VCLObject("panPiedTotTotalHT").VCLObject("saiTotalHT").Keys("4000[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0019() {  //Enregistre devis avec frais saisie de <> fagon et mode de rep global
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    //on va sur les frais
    Sys.Keys("~dff[Enter]");
    delay(200);
    //On va sur la r�f�rence
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //On saisie la r�f�rence PRE01
    Sys.Keys("PRE01");
    delay(200);
    //on se position sur la ligne en dessous
    Sys.Keys("[Down]");
    delay(200);
    //on fait F4 pour ouvrir la liste
    Sys.Keys("[F4]");
    delay(200);
    //Dans la zone de recherche on filtre sur LOC01
    Sys.Keys("LOC01");
    delay(200);
    Sys.Keys("![Tab]");
    delay(200);
    Sys.Keys("[Enter]");
    delay(200);
    //on se positionne sur la ligne en dessous
    Sys.Keys("[Down]");
    delay(200);
    //on se positionne sur la nature
    Sys.Keys("[Left]");
    delay(200);
    //on silection la nature prestation
    Sys.Keys("p");
    delay(200);
    //on se positionne sur la d�signation
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //on saisi un libell�
    Sys.Keys("Saisie en %");
    delay(200);
    //on se positionne sur la colonne %
    Sys.Keys("[Tab][Tab][Tab][Tab][Tab]");
    delay(200);
    //on saisi 10 %
    Sys.Keys("10");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //on Applique 
    Sys.Keys("~a");
    delay(2000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0020() {  //Enregistre devis avec frais saisie de <> fagon et mode de rep nature
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    delay(200);
    //on va sur les frais
    Sys.Keys("~dff[Enter]");
    delay(200);
    //On va sur la r�f�rence
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //On saisie la r�f�rence PRE01
    Sys.Keys("PRE01");
    delay(200);
    Sys.Keys("[Tab]");
    //On se met en ripartition par nature
    Sys.Keys("~n");
    delay(200);
    //On va sur la nature mat�riaux dans la colonne %
    Sys.Keys("[Tab][Tab][Tab]");
    delay(200);
    //On tappe 100 %
    Sys.Keys("100");
    //on Applique 
    Sys.Keys("~a");
    delay(2000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0021() {  //Enregistre devis avec frais saisie de <> fagon et mode de rep lig
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    delay(200);
    //on va sur les frais
    Sys.Keys("~dff[Enter]");
    delay(200);
    //On va sur la r�f�rence
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //On saisie la r�f�rence PRE01
    Sys.Keys("PRE01");
    delay(200);
    Sys.Keys("[Tab]");
    //On se met en ripartition par nature
    Sys.Keys("~l");
    delay(200);
    //On va sur la colonne num�rotation
    Sys.Keys("[Tab]");
    delay(200);
    //On spicifie la ligne 1.1 %
    Sys.Keys("1.1");
    //On va sur la colonne % de ripartition
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi 100 %
    Sys.Keys("100");
    //on Applique 
    Sys.Keys("~a");
    delay(2000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0022() {  //Enregistre devis avec poste HT et TTC positif et nigatif
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Phase_Type();
    delay(200);
    //on va sur le pied
    Sys.Keys("~dm");
    delay(200);
    //On va sur la colonne montant du 1er poste HT (remise);
    Sys.Keys("[Tab][Tab][Tab][Tab]");
    delay(200);
    //On saisie un montant de -100 euro
    Sys.Keys("-100");
    delay(200);
    //On va sur la colonne % du 2eme poste HT (escompte);
    Sys.Keys("[Tab][Tab][Tab]");
    delay(200);
    //On saisie un % de -5 %
    Sys.Keys("-5");
    delay(200);
    //On se positionne sur une ligne vierge 
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //On Saisi un libelle "Frais"
    Sys.Keys("Frais");
    //On va sur la colonne % du 3eme poste HT (frais);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisie un % de 10 %
    Sys.Keys("10");
    delay(200);
    //On se positionne sur une ligne vierge 
    Sys.Keys("[Tab][Tab][Tab]");
    delay(200);
    //On Saisi un libelle "Port"
    Sys.Keys("Port");
    //On va sur la colonne montant du 4eme poste HT (Port);
    Sys.Keys("[Tab][Tab][Tab]");
    delay(200);
    //On saisie un montant de 500 euro
    Sys.Keys("500");
    delay(200);
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedTot").VCLObject("panPiedTotTvaTTC").VCLObject("panPiedTotPostesTTC").VCLObject("panPiedTotPostesTTCScroll").VCLObject("scPostesTTC").SetFocus();
    
    //Dans la colonne libell� on saisie "Poste TTC en %"
    Sys.Keys("Poste TTC en %");
    delay(200);
    //On se positionne sur la colonne %
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi -1 %
    Sys.Keys("-1");
    delay(200);
     //On se positionne sur une ligne vierge 
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //Dans la colonne libell� on saisie "Poste TTC en Montant"
    Sys.Keys("Poste TTC en Montant");
    delay(200);
    //On se positionne sur la colonne 
    Sys.Keys("[Tab][Tab]");
    delay(200);
    //On saisi -20
    Sys.Keys("-20");
    delay(200);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0023() {  //Enregistre devis avec plusieurs echiances manuellement saisies
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Liste();
    //On va sur la section montant du pied
    Sys.Keys("~dm");
    delay(1000);
    //On donne le focus au scroll des ichiances
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedDroit").VCLObject("panPiedRglt").VCLObject("panPiedHautRgl").VCLObject("panTiersPayeur").VCLObject("saiTiersPayeur").SetFocus();
    Sys.Keys("[Tab]");
    //On va sur le montant
    Sys.Keys("[Tab][Tab][Tab]");
    //On stock dans le press papier le Montant 
    Sys.Keys("^c");
    //On calcul le nouveau coef
    varI<int> Mt_ech1 = toInt( Sys.Clipboard()) - 300;
    
    //On saisi le montant de la 1er ech
    Sys.Keys(toStr(Mt_ech1));
    delay(100);
    //On tabule pour aller sur la nouvelle ligne
    Sys.Keys("[Tab][Tab]");
    delay(100);
    //On saisi le libell�
    Sys.Keys("test ech 2");
    delay(100);
    //On tabule pour aller sur la colonne mode de RGT et on fait espace histoire de le changer
    Sys.Keys("[Tab] ");
    delay(100);
    //On tabule pour aller sur la colonne Montant
    Sys.Keys("[Tab]");
    delay(100);
    //on supprime le contenu de la colonne
    Sys.Keys("![End]");
    delay(100);
    Sys.Keys("[Del]");
    //On saisi le montant 
    Sys.Keys("200");
    //On tabule pour aller sur la nouvelle ligne
    Sys.Keys("[Tab][Tab]");
    delay(100);
    //On saisi le libell�
    Sys.Keys("test ech 3");
    delay(100);
    //On tabule pour aller sur la colonne mode de RGT et on fait espace histoire de le changer
    Sys.Keys("[Tab]  ");
    delay(100);
    //On tabule pour aller sur la colonne Montant
    Sys.Keys("[Tab]");
    delay(100);
    //on supprime le contenu de la colonne
    Sys.Keys("![End]");
    delay(100);
    Sys.Keys("[Del]");
    //On saisi le montant 
    Sys.Keys("^m");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0024() {  //Enregistre devis avec plusieurs echiances cr�er par mode de RGT + tiers payeurs
    
    // Cr�ation du devis
    P_Creat_Devis();
    P_Ins_Lig_Liste();
    //On va sur la section montant du pied
    Sys.Keys("~dm");
    delay(1000);
    // On donne le focus ? la zone mode de RGT
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis*", -1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedDroit").VCLObject("panPiedRglt").VCLObject("panPiedHautRgl").VCLObject("panPiedModReg").VCLObject("saiCodeModReg").SetFocus();
    
    //on saisi un mode de RGT avec 3 ech
    Sys.Keys("P3FO");
    delay(100);
    //On va sur le Tiers payeur
    Sys.Keys("[Tab][Tab]");
    delay(100);
    //On saisi le tiers 2
    Sys.Keys("2[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0025() {  //Modif devis en ajoutant des lignes
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On se positionne sur la derni?re ligne
    Sys.Keys("^[End][Down]");
    // On ajoute des lignes manuellement
    P_Ins_Lig_Manuel();
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0026() {  //Modif devis en supprimant des lignes (�l�ments simple/ouvrage/Tranche);
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On supprime un �l�ment simple
    Sys.Keys("[Down][F10]");
    //On supprime un ouvrage
    for(varI<int> i=0; i<14; i++) {
        Sys.Keys("[Down]");
        delay(50);
    }
    
    Sys.Keys("[F10]");
    //On supprime une tranche
    for(varI<int> i=0; i<4; i++) {
        Sys.Keys("[Down]");
        delay(50);
    }
    
    Sys.Keys("[F10]");
    // On initialisation du message
    var<TWinTST> monmessage = F_Message("TApiMessageDlg", "Supprimer la tranche");
    F_Message_BtClic(monmessage,"Oui");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0027() {  //Modif devis changement du client
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    var<TWinTST> stdSais = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteTiers").VCLObject("saiCodeTiers");
    stdSais.Keys("2[Enter]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0028() {  //Modif devis affectation chantier 
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    var<TWinTST> stdSais = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", -1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier");
    stdSais.Keys("00000003");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0029() {  //Modif devis Modification cat�gorie
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    var<TWinTST> stdSais = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("pnCategorie").VCLObject("saiCategorie");
    stdSais.Keys(" ");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0030() {  //Modif devis Modification de la qt� total de la tranche parente
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    //On se positionne sur la qt� totale
    for(varI<int> i=0; i<4; i++) {
        Sys.Keys("[Tab]");
        delay(100);
    }
    
    //On passe la qt� total ? 2
    Sys.Keys("2");
    delay(100);
    Sys.Keys("[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0031() {  //Modif devis Modif ouvrage par ajout de compo manuel et MAJ dans toutes l'�tudes
    
    // On cr�e un nouveau doc et on stock le  code
    var<TCodeTST> newcode = F_Creat_Devis();
        
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On va insirer 2 fois l'ouvrage OUVT01
    Sys.Keys("[Tab]");
    delay(100);
    Sys.Keys("OUVT01");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);
    Sys.Keys("OUVT01");
    delay(100);
    Sys.Keys("[Tab]");
    delay(100);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    //On active la modif dans toute l'�tude
    Sys.Keys("~o");
    delay(100);
    Sys.Keys("[Up][Enter]");
    delay(100);
    //On diveloppe l'ouvrage
    Sys.Keys("^[NumPlus]");
    delay(100);
    //On ajoute la composante MO01
    Sys.Keys("[Down][Down]");
    delay(100);
    Sys.Keys("MO01");
    delay(100);
    Sys.Keys("[Down]");
    //On valide le message
    var<TWinTST> MonMessage = F_Message("TfmMsgConfirmModifyAllEtudeDlg","Avertissement");
    F_Message_BtClic(MonMessage,"Oui");
    //On desactive la modif dans toute l'�tude
    Sys.Keys("~o");
    delay(100);
    Sys.Keys("[Up][Enter]");
    delay(100);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0032() {  //Modif devis Modification des lignes par la mise ? jours des coef du doc
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les coef
    Sys.Keys("~dc");
    delay(1000);
    //On se positionne sur le coef de FG Moyen en cliquant sur le scroll du bas et en faisant fl?che haut
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsCoefficients").VCLObject("panCoefficients").VCLObject("panMO").VCLObject("panMO").VCLObject("panScrollMO").VCLObject("scMO").Click();
    Sys.Keys("[Up]");
    //On stock dans le press papier le coef 
    Sys.Keys("^c");
    //On calcul le nouveau coef
    varI<double> NewCoef = toReal(Sys.Clipboard()) + 0.1;
                
    //On saisi le nouveau coef
    Sys.Keys(toStr(NewCoef));
    delay(100);
    //On revient sur l'ent�te (pour �tre sur que le traitement de maj des lignes est fini
    Sys.Keys("^~t");
    //Une fen�tre de confirmation doit apparantre, on confirme
    var<TWinTST> MonMessage = F_Message("TApiMessageDlg","Coefficients");
    F_Message_BtClic(MonMessage,"Confirmer");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0033() {  //Modif devis Modification des lignes par bt appliquer les coef de MO
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les coef
    Sys.Keys("~dc");
    delay(1000);
    //On clique sur le bouton appliquer les coef de MO
    Sys.Keys("~a");
    //Il faut attendre que le traitement soit fini, mais on a rien pour nous l'indiquer dont on attend
    //arbitrairement 10 secondes
    delay(10000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0034() {  //Modif devis Modification du d�bours� par onglet d�bours�
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur l'onglet d�bours�
    Sys.Keys("~ds");
    delay(100);
    Sys.Keys("[Enter]");
    delay(1000);
    //On va sur le d�bours� du 1er �l�ment du scroll
    for(varI<int> i=0; i<6; i++) {
        Sys.Keys("[Tab]");
        delay(100);
    }
    
    //On saisi un d�bours� de 200
    Sys.Keys("200");
    delay(100);
    //On tabule
    Sys.Keys("[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0035() {  //Modif devis Modification du d�bours� par onglet d�bours� (actualisation);
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur l'onglet d�bours�
    Sys.Keys("~ds");
    delay(100);
    Sys.Keys("[Enter]");
    delay(1000);
    //On va sur le d�bours� du 1er �l�ment du scroll
    for(varI<int> i=0; i<6; i++) {
        Sys.Keys("[Tab]");
        delay(100);
    }
    
    //On saisi un d�bours� de 200
    Sys.Keys("200");
    delay(100);
    //On tabule
    Sys.Keys("[Tab]");
    delay(100);
    //On clique sur le bt Actualiser les d�bours�s
    Sys.Keys("~a");
    //Une fen�tre de confirmation doit apparantre, on confirme
    var<TWinTST> MonMessage = F_Message("TApiMessageDlg","Actualisation des d�bours�s de la biblioth?que");
    F_Message_BtClic(MonMessage,"Confirmer");
    //Une fen�tre indique que le traitement est termin� on fait OK
    MonMessage = F_Message("TApiMessageDlg","Actualisation des d�bours�s");
    F_Message_BtClic(MonMessage,"Ok");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0036() {  //Modif devis Propri�t� non compris de la tranche parente
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    //On passe la tranche en �l�ment non compris
    Sys.Keys("^n");
    delay(3000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0037() {  //Modif devis Modification de l'analytique sur l'ent�te avec application sur ligne
    
    // On cr�e un nouveau doc et on stock le code
    var<TCodeTST> newcode = F_Creat_Devis();
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert Mat01  
    Sys.Keys("mat01[Enter]");
    delay(200);
    //On ouvre les rep analytiques
    var<TWinTST> mafiche =  F_Fiche("TfmDocDevCli", "Devis * [Cr�ation]");
    F_Fiche_BtClic(mafiche,"Analytique");
    //On silectionne le plan produit
    Sys.Keys("[F4]");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);
    Sys.Keys("[Enter]");
    delay(100);
    //On va sur la section
    Sys.Keys("[Tab]");
    delay(100);
    //On saisi la section 001 pour 50 %
    Sys.Keys("001");
    delay(100);
    Sys.Keys("[Tab][Tab]");
    delay(100);
    Sys.Keys("50");
    Sys.Keys("[Tab]");
    delay(100);
    //On saisi la section 002 pour 30 %
    Sys.Keys("002");
    delay(100);
    Sys.Keys("[Tab][Tab]");
    delay(100);
    Sys.Keys("30");
    delay(100);
    Sys.Keys("[Tab]");
    delay(100);
    //On saisi la section ATE pour 20 en soldant
    Sys.Keys("ATE");
    delay(100);
    Sys.Keys("[Tab][Tab]");
    delay(100);
    Sys.Keys("~s");
    delay(100);
    //On valide la fen�tre de ripartition des grilles
    Sys.Keys("[F2]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    mafiche = F_Open_Devis(newcode);
    //On va sur l'onglet option
    Sys.Keys("~do");
    delay(100);
    //On ouvre les rep analytiques
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *[Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTeteOptions").VCLObject("panEnTeteOptions").VCLObject("panOptionsCompta").VCLObject("panOptionsComptaTop").VCLObject("btAnalytiqueEnt").Click();
    
    //On silectionne le plan produit
    Sys.Keys("[F4]");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);
    Sys.Keys("[Enter]");
    delay(100);
    //On va sur la section
    Sys.Keys("[Tab]");
    delay(100);
    //On saisi la section 001 pour 100 %
    Sys.Keys("001");
    delay(100);
    Sys.Keys("[Tab]");
    delay(100);
    Sys.Keys("100");
    //On quitte et on applique
    Sys.Keys("[Esc]");
    delay(100);
    //Une fen�tre de confirmation doit apparantre, on confirme
    var<TWinTST> MonMessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(MonMessage,"Oui");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0038() {  //Modif devis Modification du mitri g�n�rale ayant un impact dans les lignes
    
    // On cr�e un nouveau doc et on stock le code
    var<TCodeTST> newcode = F_Creat_Devis();
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert Mat01  
    Sys.Keys("mat01[Enter]");
    delay(200);
    //On ouvre la fen�tre du mitri g�n�ral
    Sys.Keys("^[F8]");
    var<TWinTST> mafiche = F_Fiche("TfmMetreDoc", "M�tr� - R�capitulatif");
    //On se positionne dans la colonne d�signation
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi Long = 10
    Sys.Keys("Long = 10");
    delay(200);
    //On Valide
    Sys.Keys("[F2]");
    delay(200);
    //On fait F8 pour entrer sur le mitri de la ligne
    Sys.Keys("[F8]");
    mafiche = F_Fiche("TfmMetreDoc", "M�tr� - MAT01");
    //On se positionne dans la colonne d�signation
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi Larg = 5
    Sys.Keys("Larg = 5");
    delay(200);
    //On fait fleche bas et tab pour se positionner sur la ligne suivantes
    Sys.Keys("[Down][Tab]");
    delay(200);
    //On saisi Long * Larg
    Sys.Keys("Long * Larg");
    delay(200);
    //On fait fleche bas et t insirer une ligne de type totalisation en dessous
    Sys.Keys("[Down]t");
    delay(200);
    //On Valide
    Sys.Keys("[F2]");
    delay(200);
    //On Valide le doc
    P_Valide_Devis();
    //On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On ouvre la fen�tre du mitri g�n�ral
    Sys.Keys("^[F8]");
    mafiche = F_Fiche("TfmMetreDoc", "M�tr� - R�capitulatif");
    //On se positionne dans la colonne d�signation
    Sys.Keys("[Tab]");
    delay(200);
    //On supprime le contenu et on le remplace
    Sys.Keys("[Del]");
    delay(200);
    Sys.Keys("Long = 15");
    delay(200);
    //On Valide
    Sys.Keys("[F2]");
    delay(200);
    //On Valide le doc
    P_Valide_Devis();
    //On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0039() {  //Modif Devis Modification Taux & Exigibiliti de TVA
    
    // On cr�e un nouveau doc et on stock le code
    var<TCodeTST> newcode = F_Creat_Devis();
    
    // Insertion de lignes saisie manuellement
    P_Ins_Lig_Manuel();
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On fait un F5
    Sys.Keys("[F5]");
    //On se positionne sur la TVA 
    for(varI<int> i=0; i<8; i++) {
        Sys.Keys("[Tab]");
        delay(100);
    }
    
    //On passe ? une tva de code 2
    Sys.Keys("2");
    delay(100);
    Sys.Keys("[Enter]");
    delay(100);
    //On ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    //On passe sur la ligne du dessous
    Sys.Keys("[Down]");
    delay(100);
    //On va sur la colonne exigibiliti du 1er ouvrage
    for( varI<int> i = 0; i<8; i++) {
        Sys.Keys("[Tab]");
        delay(100);
    }
    
    //On passe au dibit
    Sys.Keys("d");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0040() {  //Modif Devis Modification actualisation ligne et pied
    
    // On cr�e un nouveau doc et on stock le code
    var<TCodeTST> newcode = F_Creat_Devis();
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On insert Mat01  
    Sys.Keys("mat01[Enter]");
    delay(200);
    //On ouvre la fen�tre propri�t� de la ligne
    Sys.Keys("[F5]");
    //On va sur l'onglet actualisation
    Sys.Keys("~r");
    delay(200);
    //On saisi une date et on va sur le libell�
    Sys.Keys("010100");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un libelle et on va sur coef
    Sys.Keys("test actu lig");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un coef et on ferme la fen�tre de propri�t�
    Sys.Keys("1.2");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[Esc]");
    delay(200);
    //On va sur le pied dans analyse
    Sys.Keys("~da");
    delay(200);
    //Normalement on a le focus sur le scroll du haut
    //Pour arriver sur le scroll du bas on fait un truc pas jojo (mais pas mieux pour le moment);
    //On se positionne sur le scroll des actu du pied
    Sys.Keys("^[End]");
    delay(200);
    Sys.Keys("[Down][Down][Down][Down]");
    delay(200);
    //On saisi une date et on va sur le libell�
    Sys.Keys("010100");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un libelle et on va sur coef
    Sys.Keys("test actu pied");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On saisi un coef et on ferme la fen�tre de propri�t�
    Sys.Keys("1.5");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    //On retourne sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On se positionne sur la 2eme ligne
    Sys.Keys("[Down]");
    delay(500);
    //On fait un F5
    Sys.Keys("[F5]");
    //On va sur l'onglet actualisation
    Sys.Keys("~r");
    delay(200);
    //On se positionne sur le coef on le passe ? 2.2
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("2.2");
    Sys.Keys("[Tab]");
    delay(100);
    Sys.Keys("[Esc]");
    //On va sur le pied dans analyse
    Sys.Keys("~da");
    delay(200);
    //Normalement on a le focus sur le scroll du haut
    //Pour arriver sur le scroll du bas on fait un truc pas jojo (mais pas mieux pour le moment);
    //On se positionne sur le scroll des actu du pied
    Sys.Keys("^[End]");
    delay(200);
    Sys.Keys("[Down][Down][Down][Down]");
    delay(200);
    //On va sur le coef du pied et on le passe ? 2.2
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("2.5");
    delay(200);
    Sys.Keys("[Tab]");
    delay(200);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0041() {  //Modif Devis Modification variation
    
    // On cr�e un nouveau doc et on stock le code
    var<TCodeTST> newcode = F_Creat_Devis();
    
    //Insertion de phase type
    P_Ins_Lig_Phase_Type();
    //On va sur le montant et on saisit 4000
    Sys.Keys("~dm");
    delay(500);
    Sys.Keys("4000");
    Sys.Keys("[Tab]");
    //On retourne sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    //On va sur le montant et on saisi 4555
    Sys.Keys("~dm");
    delay(500);
    Sys.Keys("4555");
    Sys.Keys("[Tab]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0042() {  //Modif Devis Modification Frais + Poste HT + Echiances
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //on va sur les frais
    Sys.Keys("~dff[Enter]");
    delay(200);
    //On passe la qt� ? 3 
    Sys.Keys("[Tab][Tab][Tab][Tab]");
    delay(500);
    Sys.Keys("3");
    //On va sur l'onglet Montant
    Sys.Keys("~dm");
    delay(500);
    //Une fen�tre de confirmation doit apparantre, on confirme
    var<TWinTST> MonMessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(MonMessage,"Non");
    //On supprime un poste HT
    Sys.Keys("[F10]");
    delay(200);
    //on modifie le % du 2eme poste
    Sys.Keys("[Tab]");
    delay(100);
    Sys.Keys("5");
    delay(100);
    //On donne le focus au mode de RGT
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsPied").VCLObject("panPied").VCLObject("panPiedDroit").VCLObject("panPiedRglt").VCLObject("panPiedHautRgl").VCLObject("panPiedModReg").VCLObject("saiCodeModReg").SetFocus();
    
    //On change le mode de RGT
    Sys.Keys("CPT");
    delay(500);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0043() {  //Enregistre commande par copie du devis Mod�le
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On ouvre la liste des devis clients
    Open_Lst_Devis();
    // On filtre la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    F_Liste_Filtre(maliste,newcode);
    //On lance le transfert en commande
    Sys.Keys("~a");
    //On attend que la fiche commande s'ouvre
    delay(10000);
    var<TWinTST> mafiche  = F_Fiche("TfmDocCdeCli", "Commandes");
    // Validation de la fiche
    F_Fiche_BtClic(mafiche,"Valider");
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0044() {  //Enregistre BL par copie du devis Mod�le
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On ouvre la liste des devis clients
    Open_Lst_Devis();
    // On filtre la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    F_Liste_Filtre(maliste,newcode);
    //On lance le transfert en BL
    Sys.Keys("~l");
    //On attend que la fiche du BL s'ouvre
    delay(10000);
    var<TWinTST> mafiche = F_Fiche("TfmDocBLCli", "Bon de livraison");
    // Validation de la fiche
    F_Fiche_BtClic(mafiche,"Valider");
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0045() {  //Enregistre facture par copie du devis Mod�le
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On ouvre la liste des devis clients
    Open_Lst_Devis();
    // On filtre la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    F_Liste_Filtre(maliste, newcode);
    //On lance le transfert en commande
    Sys.Keys("~t");
    //On attend que la fiche commande s'ouvre
    delay(5000);
    var<TWinTST> mafiche  = F_Fiche("TfmDocFacCli", "Facture");
    // Validation de la fiche
    F_Fiche_BtClic(mafiche,"Valider");
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0046() {  //Enregistre Base de fac
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur la zone code chantier
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis [*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier").SetFocus();
    
    //On fait F11 pour ouvrir une fiche chantier en cr�ation
    Sys.Keys("[F11]");
    //On attend que la fiche chantier s'ouvre
    mafiche = F_Fiche("TfmChantierV2","Chantier [");
    mafiche.WaitProperty("Visible", true, 50000);
    //On stock le code du Chantier
    Sys.Keys("^c");
    var<TCodeTST> Code_chantier = Sys.Clipboard();
    
    //On tabule et on enregistre la fiche chantier
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[F2]");
    //On revient sur la zone itat du document et on le passe ? accepti
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat").SetFocus();
    Sys.Keys("a");
    //On attend la fen�tre d'avertissement et on confirme
    var<TWinTST> monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Confirmer");
    //On attend que le devis passe en consultation
    mafiche = F_Fiche("TfmDocDevCli", "Devis [*] [Consultation]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0047() {  //Cr�ation facture d'avt avec 2 devis en base de fac
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    //On va sur la zone code chantier
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis [*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier").SetFocus();
    
    //On fait F11 pour ouvrir une fiche chantier en cr�ation
    Sys.Keys("[F11]");
    //On attend que la fiche chantier s'ouvre
    mafiche = F_Fiche("TfmChantierV2","Chantier [");
    mafiche.WaitProperty("Visible", true, 50000);
    //On stock le code du Chantier
    Sys.Keys("^c");
    var<TCodeTST> Code_chantier = Sys.Clipboard();
    
    //On tabule et on enregistre la fiche chantier
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[F2]");
    //On revient sur la zone itat du document et on le passe ? accepti
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat").SetFocus();
    Sys.Keys("a");
    //On attend la fen�tre d'avertissement et on confirme
    var<TWinTST> monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Confirmer");
    //On attend que le devis passe en consultation
    mafiche = F_Fiche("TfmDocDevCli", "Devis [*] [Consultation]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
     // On copie le doc et on stock le nouveau code
    newcode = F_Copie_Devis("00000001");
    // On r�ouvre le doc
    mafiche = F_Open_Devis(newcode);
    //On va sur les coef
    Sys.Keys("~dc");
    delay(1000);
    //On se positionne sur le coef de FG Moyen en cliquant sur le scroll du bas et en faisant fl?che haut
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsCoefficients").VCLObject("panCoefficients").VCLObject("panMO").VCLObject("panMO").VCLObject("panScrollMO").VCLObject("scMO").Click();
    Sys.Keys("[Up]");
    //On stock dans le press papier le coef 
    Sys.Keys("^c");
    //On calcul le nouveau coef
    varI<double> NewCoef = toReal(Sys.Clipboard()) + 0.1;
                
    //On saisi le nouveau coef
    Sys.Keys(toStr(NewCoef));
    delay(100);
    //On revient sur l'ent�te (pour �tre sur que le traitement de maj des lignes est fini
    Sys.Keys("^~t");
    //Une fen�tre de confirmation doit apparantre, on confirme
    var<TWinTST> MonMessage = F_Message("TApiMessageDlg","Coefficients");
    F_Message_BtClic(MonMessage,"Confirmer");
    delay(1000);
    // On renseigne le code chantier
    //Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteTiers").VCLObject("saiCodeChantier").Keys(Code_chantier);
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier").Keys(Code_chantier.toString());
    //On revient sur la zone itat du document et on le passe ? accepti
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat").SetFocus();
    Sys.Keys("a");
    //On attend la fen�tre d'avertissement et on confirme
    monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Confirmer");
    //On attend la fen�tre d'avertissement et on confirme
    monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Non");
    //On attend que le devis passe en consultation
    mafiche = F_Fiche("TfmDocDevCli", "Devis [*] [Consultation]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
    //On ourvre la liste des chantiers
    Sys.Keys("~c");
    delay(200);
    Sys.Keys("l");
    var<TWinTST> maliste = F_Liste("Chantiers");
    F_Liste_Verif_Filtre(maliste);
    F_Liste_Filtre(maliste,Code_chantier);
    //On ouvre la fiche chantier
    Sys.Keys("~m");
    mafiche = F_Fiche("TfmChantierV2","Chantier [");
    //On va sur le noeud des factures d'avt
    Sys.Keys("~d");
    delay(100);
    Sys.Keys("ff");
    delay(100);
    Sys.Keys("[Right]");
    delay(100);
    Sys.Keys("[Right]");
    delay(100);
    //On Cr�er une facture d'avt
    Sys.Keys("~c");
    delay(100);
    var<TWinTST> mafacture = F_Fiche("TfmDocFacCliAvt","Facture d'avancement");
    var<TWinTST> mazone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").WaitWindow("TfmDocFacCliAvt", "Facture d'avancement *", -1, 10000).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteRefLeft").VCLObject("saiCode");
    mazone.WaitProperty("Enabled", true);
    mazone.WaitProperty("Visible", true);
    //On va dans l'avacement et on saisi 10 % en globale
    Sys.Keys("~d");
    delay(100);
    Sys.Keys("c");
    delay(100);
    Sys.Keys("~n");
    delay(100);
    Sys.Keys("~s");
    delay(100);
    Sys.Keys("[Tab]");
    delay(100);
    Sys.Keys("10");
    //On Valide la facture d'avt
    Sys.Keys("[F2]");
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0048() {  //Modif Nature de travaux en ent�te => ""
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On se positionne sur la nature de travaux");
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("panEnTeteTx").VCLObject("saiNatTx");
    MaZone.SetFocus();
    
    //on passe la nature ? ""
    MaZone.Keys("[F4]    ");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0049() {  //Modif Nature de travaux en ent�te => "Construction"
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On se positionne sur la nature de travaux");
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("panEnTeteTx").VCLObject("saiNatTx");
    MaZone.SetFocus();
    
    //on passe la nature ? "" pour modifier les comptes et tva des lignes
    MaZone.Keys("[F4]    ");
    delay(1000);
    //on re-passe la nature ? "Construction" pour v�rifier la MAJ
    MaZone.Keys("[F4]c");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0050() {  //Modif Nature de travaux en ent�te => "R�novation"
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On se positionne sur la nature de travaux");
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("panEnTeteTx").VCLObject("saiNatTx");
    MaZone.SetFocus();
    
    //on passe la nature ? "" pour modifier les comptes et tva des lignes
    MaZone.Keys("[F4]    ");
    delay(1000);
    //on re-passe la nature ? "Construction" pour v�rifier la MAJ
    MaZone.Keys("[F4]r");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0051() {  //Modif cat�gorie en ent�te => "Autres 1"
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On se positionne sur la nature de travaux");
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("pnCategorie").VCLObject("saiCategorie");
    MaZone.SetFocus();
    
   // on passe la cat�gorie ? "Autres 1"
    MaZone.Keys("[F4]1");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0052() {  //Modif cat�gorie en ent�te => "France"
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On se positionne sur la nature de travaux");
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis *", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteDesc").VCLObject("pnCategorie").VCLObject("saiCategorie");
    MaZone.SetFocus();
    
    // on passe la cat�gorie ? "Autres 1"
    MaZone.Keys("[F4]1");
    delay(1000);
    // on re-passe la cat�gorie ? 0
    MaZone.Keys("[F4]0");
    delay(1000);
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

    
//-------------------------------------------------------------------------------
void T_Ventes_0053() {  //Modif Nature de travaux dans les lignes => ""
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // Ouverture de la fen�tre de propri�t�
    Sys.Keys("[F5]");
    var<TWinTST> propriete = F_Fiche("TfmDocDevCliProprietes", "Propri�t�s");
    // On passe la nature de travaux ? ""
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCliProprietes", "Propri�t�s", 1).VCLObject("panNat").VCLObject("ApiPanel8").VCLObject("saiNatTX");
    MaZone.Keys("[F4]    [Enter]");
    delay(1000);
    // on ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0054() {  //Modif Nature de travaux dans les lignes => "Construction" 
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // Ouverture de la fen�tre de propri�t�
    Sys.Keys("[F5]");
    var<TWinTST> propriete = F_Fiche("TfmDocDevCliProprietes", "Propri�t�s");
    // On passe la nature de travaux ? ""
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCliProprietes", "Propri�t�s", 1).VCLObject("panNat").VCLObject("ApiPanel8").VCLObject("saiNatTX");
    MaZone.Keys("[F4]    [Enter]");
    delay(1000);
    // on ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    // Ouverture de la fen�tre de propri�t�
    Sys.Keys("[F5]");
    propriete = F_Fiche("TfmDocDevCliProprietes", "Propri�t�s");
    // On re-passe la nature de travaux ? "Construction"
    MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCliProprietes", "Propri�t�s", 1).VCLObject("panNat").VCLObject("ApiPanel8").VCLObject("saiNatTX");
    MaZone.Keys("[F4]c");
    delay(1000);
    // on ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0055() {  //Modif Nature de travaux dans les lignes => "R�novation" 
    
    // On copie le doc et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000002");
    // On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    // On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
    // Ouverture de la fen�tre de propri�t�
    Sys.Keys("[F5]");
    var<TWinTST> propriete = F_Fiche("TfmDocDevCliProprietes", "Propri�t�s");
    // On passe la nature de travaux ? ""
    var<TWinTST> MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCliProprietes", "Propri�t�s", 1).VCLObject("panNat").VCLObject("ApiPanel8").VCLObject("saiNatTX");
    MaZone.Keys("[F4]    [Enter]");
    delay(1000);
    // on ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    // Ouverture de la fen�tre de propri�t�
    Sys.Keys("[F5]");
    propriete = F_Fiche("TfmDocDevCliProprietes", "Propri�t�s");
    // On re-passe la nature de travaux ? "R�novation"
    MaZone = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCliProprietes", "Propri�t�s", 1).VCLObject("panNat").VCLObject("ApiPanel8").VCLObject("saiNatTX");
    MaZone.Keys("[F4]r");
    delay(1000);
    // on ferme la fen�tre de propri�t�
    Sys.Keys("[Esc]");
    // On Valide le doc
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0056() {  //Transfert Commande en 1 BL
    
    // On cr�er une commande client et on stock le nouveau code et on va sur les lignes
    var<TStringTST> Code_Cmd = F_Creat_Cmd();
    
    // Insertion de lignes dans une commande 
    P_Ins_Lig_Manuel_Cde();
    // On valide la Commande
    P_Valide_Cde();
    // On ferme tout
    P_CloseAllForm();
    Open_Lst_Cmd();
    //On filtre sur le devis que l'on vient de copier
    Sys.Keys(Code_Cmd);
    delay(1500);
    //On lance le transfert en BL
    Sys.Keys("~L");
    //On attend que la fiche du BL s'ouvre
    var<TWinTST> MonBl = F_Fiche("TfmDocBLCli", "Bon de livraison");
    MonBl.WaitProperty("Visible", true);
    //On valide
    Sys.Keys("[F2]");
    // On ferme tout
    P_CloseAllForm();
}


//-------------------------------------------------------------------------------
void T_Ventes_0057() {  //Transfert Commande en 2 BL
    
    // On cr�er une commande client et on stock le nouveau code et on va sur les lignes
    var<TStringTST> Code_Cmd = F_Creat_Cmd();
    
    // Insertion de lignes dans une commande 
    P_Ins_Lig_Manuel_Cde();
    // On valide la Commande
    P_Valide_Cde();
    // On ferme tout
    P_CloseAllForm();
    // Ouverture de la liste des commandes
    Open_Lst_Cmd();
    //On filtre sur la commande que l'on vient de cr�er
    Sys.Keys(Code_Cmd);
    delay(1500);
    //On lance le transfert en BL
    Sys.Keys("~L");
    //On attend que la fiche du BL s'ouvre
    var<TWinTST> MonBl = F_Fiche("TfmDocBLCli", "Bon de livraison");
    MonBl.WaitProperty("Visible", true);
     // On va sur les lignes
    Sys.Keys("~^l");
    //On va sur la 2eme ligne
    Sys.Keys("[Down]");
    //On va sur la colonne quantit�
    for(varI<int> i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(200);
    }
    
    //On renseigne une qt� de 2
    Sys.Keys("2");
    delay(100);
    //On se positionne sur la qt� de la ligne suivante (la 3eme);
    Sys.Keys("[Down]");
    delay(100);
    //On se positionne sur la qt� de la ligne suivante (la 4eme);
    Sys.Keys("[Down]");
    delay(100);
    //On supprime la 4eme ligne
    Sys.Keys("[F10]");
    delay(100);
    //On se positionne sur la qt� de la ligne suivante (la 5eme);
    for (varI<int> i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(200);
    }
    
    //On saisi une qt� de 0
    Sys.Keys("0");
    delay(100);
    //On se positionne sur la ligne suivante
    Sys.Keys("[Down]");
    delay(100);
    //On valide le BL
    Sys.Keys("[F2]");
    // On ferme tout
    P_CloseAllForm();
    // Ouverture de la liste des commandes
    Open_Lst_Cmd();
    //On filtre sur la commande que l'on vient de cr�er
    Sys.Keys(Code_Cmd);
    delay(1500);
    //On lance le transfert en BL
    Sys.Keys("~L");
    //On attend que la fiche du BL s'ouvre
    MonBl = F_Fiche("TfmDocBLCli", "Bon de livraison");
    MonBl.WaitProperty("Visible", true);
    //On valide le BL
    Sys.Keys("[F2]");
    // On ferme tout
    P_CloseAllForm();
}

//-------------------------------------------------------------------------------
void T_Ventes_0058() {  //Transfert Commande en 1 facture et 1 BL
    
    // On cr�er une commande client et on stock le nouveau code et on va sur les lignes
    var<TStringTST> Code_Cmd = F_Creat_Cmd();
    
    // Insertion de lignes dans une commande 
    P_Ins_Lig_Manuel_Cde();
    // On valide la Commande
    P_Valide_Cde();
    // On ferme tout
    P_CloseAllForm();
    // Ouverture de la liste des commandes
    Open_Lst_Cmd();
    // On filtre sur la commande que l'on vient de cr�er
    Sys.Keys(Code_Cmd);
    delay(1500);
    //On lance le transfert en Facture
    Sys.Keys("~t");
    //On attend que la fiche Facture s'ouvre
    var<TWinTST> MaFacture = F_Fiche("TfmDocFacCli", "Facture");
    MaFacture.WaitProperty("Visible", true);
    //On va sur les lignes
    Sys.Keys("~^l");
    delay(1000);
     //On va sur la 2eme ligne
    Sys.Keys("[Down]");
    // On va sur la colonne quantit�
    for (varI<int> i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(200);
    }
    
    // On renseigne une qt� de 2
    Sys.Keys("2");
    delay(100);
    // On se positionne sur la qt� de la ligne suivante (la 3eme);
    Sys.Keys("[Down]");
    delay(100);
    // On se positionne sur la qt� de la ligne suivante (la 4eme);
    Sys.Keys("[Down]");
    delay(100);
    // On supprime la 4eme ligne
    Sys.Keys("[F10]");
    delay(100);
    // On se positionne sur la qt� de la ligne suivante (la 5eme);
    for (varI<int> i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(200);
    }
    
    // On saisi une qt� de 0
    Sys.Keys("0");
    delay(100);
    // On se positionne sur la ligne suivante
    Sys.Keys("[Down]");
    delay(100);
    // On valide
    Sys.Keys("[F2]");
    // On ferme tout
    P_CloseAllForm();
    // Ouverture de la liste des commandes
    Open_Lst_Cmd();
    // On filtre sur la commande que l'on vient de cr�er
    Sys.Keys(Code_Cmd);
    delay(1500);
    //On lance le transfert en Bl
    Sys.Keys("~l");
    //On attend que la fiche du BL s'ouvre
    var<TWinTST> MonBl = F_Fiche("TfmDocBLCli", "Bon de livraison");
    MonBl.WaitProperty("Visible", true);
    //On valide
    Sys.Keys("[F2]");
    // On ferme tout
    P_CloseAllForm();
}

//***** END OF FILE ********************************************