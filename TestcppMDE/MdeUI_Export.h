//*****************************************************************************************
// Publication (exportation) des objects UI de mde
//
// 
//*****************************************************************************************

#ifndef _MDEUIEXPORT_H
#define _MDEUIEXPORT_H

#include "cppToJavaScript.h"

struct MDE_Appli; // FORWARD
extern var<MDE_Appli> mde;

struct TMDE_HeadMenus; // FORWARD
extern var<TMDE_HeadMenus> mdeHeadMenu;

struct TDlgDevisList; // FORWARD
extern var<TDlgDevisList> dlgDevisList;

struct TDlgDevisDocu; // FORWARD
extern var<TDlgDevisDocu> dlgDevisDocu;

struct TDlgAvertissement; // FORWARD
extern var<TDlgAvertissement> dlgAvertissement;

#endif
// END OF FILE ********************************************************************************
