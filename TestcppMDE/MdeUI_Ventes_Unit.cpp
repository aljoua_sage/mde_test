//*****************************************************************************************
// Definition (instanciation) des dlgbox pour les VENTES
//
//*****************************************************************************************
#pragma once

#include "WidgetsExportation.h"

#include "Widgets_Unit.cpp"
//USEUNIT Widgets_Unit

#include "MdeAppli_Unit.cpp"
//USEUNIT MdeAppli_Unit

JSobject TDlgDevisList : TDlgForm {
   JSCODE( TDlgForm.call(this); )
   field<TUIobject> panStatusToolBar = newTUIobject(THIS, "PanVScroll/panStatus/panTools/panButtons" );
   field<UIButton>  bt_Copier = newUIButton(THIS.panStatusToolBar, "IDB_BTCOPIER" );
   field<UIButton>  bt_Quitter = newUIButton(THIS.panStatusToolBar, "IDB_BTANNULER");
};
EXTEND(TDlgDevisList, TDlgForm);

#ifdef IMPLEMENTATION
var<TDlgDevisList> dlgDevisList = newTDlgForm(mde.mainWin, "TStdFormListe", "Devis clients", new TDlgDevisList());
#endif
  
JSobject TDlgDevisDocu : TDlgForm {
   JSCODE(TDlgForm.call(this); )
   field<UIButton>  btIcon_Enregistrer = newUIButton(THIS, "Enregistrer");
   field<UIButton>  bt_Valider = newUIButton(THIS, "btValider");
   field<UIButton>  bt_Quitter = newUIButton(THIS, "btQuitter");
   field<UIEdit>    edt_Status = newUIEdit(THIS, "panGeneral/panPage/panDoc/pcOnglet/tsEnTete/panEnTete/panEnTeteRef/panEnTeteEtat/saiEtat");
};
EXTEND(TDlgDevisDocu, TDlgForm);


JSobject TDlgAvertissement : TDlgForm {
    JSCODE(TDlgForm.call(this); )
    field<UIButton>  bt_Confirmer = newUIButton(THIS, "Confirmer");
};
EXTEND(TDlgAvertissement, TDlgForm);


#ifdef IMPLEMENTATION
var<TDlgDevisDocu> dlgDevisDocu = newTDlgForm(mde.mainWin, "TfmDocDevCli", "Devis", new TDlgDevisDocu());
#endif

#ifdef IMPLEMENTATION
var<TDlgAvertissement> dlgAvertissement = newTDlgForm(mde.process, "TApiMessageDlg", "Avertissement", new TDlgAvertissement());
#endif

// END OF FILE ********************************************************************************