//*****************************************************************************************
// Utilitaires pour action dans des listes (scrolls, fiches)
// 
//*****************************************************************************************
#pragma once

#include "cppTestEngine.h"
#include "typesApplicatif.h"  // pour TCodeTST

#include "MdeUI_Export.h"  // pour l'instance mde

#include "MdeAppli_Unit.cpp" // pour mde
//USEUNIT MdeAppli_Unit

#include "JavaScriptExtens.cpp"  // pour str_startsWith...
//USEUNIT JavaScriptExtens


//**********************************************************************************************
// Recherche d'une liste par son nom
inline TWinTST find_SdtFormListe( TStringTST _nomListe) {

    var<TWinTST> ObjListe;

    // Dans le  cas ou on exploite le mapping (!!! Les noms doivent commencer par Map_ !!!);
    if( str_startsWith(_nomListe, "Map_") ) {
        ObjListe = mde.mainWin.win.WaitNamedChild(_nomListe, 10000);
    }else{
        // Initialisation de l'objet
        ObjListe = mde.mainWin.win.WaitWindow("TStdFormListe", "*" + _nomListe + "*", -1, 10000);
    }

    if (! ObjListe.Exists ) {
        Log.Error("La liste n'est pas pr�sente");
    }
    return ObjListe;
}

//**********************************************************************************************
// Retourne le widget "Label ZoneRecherche" d'une liste 
inline TWinTST getLabelZoneRecherche(TWinTST objListe) {
    var<TWinTST> Label_zone_rch = objListe.FindChild("Name", "*txtQuickSearch*", 2);
    if (!Label_zone_rch.Exists) {
        Log.Error("Le label de la zone de recherche non trouv�");
    }
    return Label_zone_rch;
}

//******************************************************************************
//  V�rifie que la zone de recherche est positionn�e sur la bonne colonne
//  et modifie le colonnage le cas �ch�ant
inline void verif_FiltreCodeOuRef(TWinTST objListe) {

    var<TWinTST> label_zone_rch = getLabelZoneRecherche( objListe );

    // V�rification du label
    varI<bool> Bok = (label_zone_rch.Caption == "Code") || (label_zone_rch.Caption == "R�f�rence");
    if( Bok == true ) return; 

    // La liste n'a pas le bon tri, il faut le changer
    var<TWinTST> stdScroll = objListe.VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
    stdScroll.ClickR(-1, -1);
    stdScroll.PopupMenu.Click("Gestion des vues");
    var<TWinTST> stdPanel = mde.mainWin.win.WaitWindow("TViewsListDlg", "Vues", -1, 1000).VCLObject("StdPanel");
    stdPanel.VCLObject("btnModify").Click(-1, -1);
    var<TWinTST> viewDlg = mde.mainWin.win.WaitWindow("TViewDlg", "Param�trage d'une vue", -1, 1000);
    var<TWinTST> stdSais = viewDlg.FindChild("Name", "*saiOrderField*", 5);
    stdSais.Keys("[F4]");
    mde.process.win.Window("TFrmTables", "Champ de tris").VCLObject("StdPanel1").VCLObject("btOK").Click(-1, -1);
    viewDlg.VCLObject("StdPanel").VCLObject("BtnValider").Click(-1, -1);
    stdPanel.VCLObject("btnActivateView").Click(-1, -1);

    // Initialisation de l'objet
    label_zone_rch = getLabelZoneRecherche( objListe );
    if (!label_zone_rch.Exists) {
        Log.Error("Le label de la zone de recherche non trouv�");
    }
}


//------------------------------------------------------------------------------- 
// Filtre une liste (pour n'avoir qu'un seul article)
//-------------------------------------------------------------------------------
inline void liste_Select_CodeOrRef(TWinTST objListe, TCodeTST _filtre) {

    // On s'assure que la zone de recherche se fait sur la colonne Code ou R�f�rence
    verif_FiltreCodeOuRef(objListe);

    // Initialisation de l'objet
    var<TWinTST> zone_rch = objListe.FindChild("Name", "*saiQuickSearch*", 2);
    if (!zone_rch.Exists) {
        Log.Error("Zone de recherche non trouv�e");
    }

    //On saisit les caract�res
    zone_rch.Keys(_filtre.toString());
    //On donne un delai
    delay(1000);
}



// END OF FILE ********************************************************************************
