//*****************************************************************************************
// Description statique des menus et submenu
//
//*****************************************************************************************
#pragma once

#include "WidgetsExportation.h"

#include "MdeUI_Export.h"
//USEUNIT Widgets_Unit

#include "MdeUI_Ventes_Unit.cpp" // pour la d�claration de TDlgDevisList et dlgDevisList
//USEUNIT MdeUI_Ventes_Unit      


JSobject TMenuDossier : UIsubMenu {

};

JSobject TMenuInitialisation : UIsubMenu {

};

JSobject TMenuBibliotheques : UIsubMenu {

};

JSobject TMenuRessources : UIsubMenu {

};

JSobject TMenuTiers : UIsubMenu {

};

JSobject TMenuAffaires : UIsubMenu {

};

//*********************************************************************************
// MENUS "VENTES"
JSobject TSMenuDevisQuantEstim : UIsubMenu {
  field<TUIaction> action = newAction( ACT_OPEN, dlgDevisList );
};
JSCODE(TSMenuDevisQuantEstim.prototype = new UIsubMenu();)

JSobject TSMenuCommandes : UIsubMenu {

};

JSobject TMenuVentes : UIsubMenu {
   field<TSMenuDevisQuantEstim> smenuDevisQuantEstim = newMenu(this, 0,"Devis Quantitatifs et estimatifs", new TSMenuDevisQuantEstim );
   field<TSMenuCommandes> sMenuCommandes             = newMenu(this, 2,"Commandes", new TSMenuCommandes );
};

//*********************************************************************************
// MENUS "ACHATS"
JSobject TMenuAchats : UIsubMenu {

};

JSobject TMenuChantiers : UIsubMenu {

};

JSobject TMenuComptabilite : UIsubMenu {

};

JSobject TMenuStocks : UIsubMenu {

};

JSobject TMenuOutils : UIsubMenu {

};

JSobject TMenuAdministration : UIsubMenu {

};

JSobject TMenuFenetres : UIsubMenu {
  
};

JSobject TMDE_HeadMenus : UIMainMenu {
    field<TMenuDossier> menuDossiers = newMenu(this, 0,"", new TMenuDossier);
    field<TMenuInitialisation> menuInitialisation = newMenu(this, 1, "Initialisation", new TMenuInitialisation);
    field<TMenuBibliotheques> menuBibliotheques = newMenu(this, 2, "Biblioth�ques", new TMenuBibliotheques);
    field<TMenuRessources> menuRessources = newMenu(this, 3, "", new TMenuRessources);
    field<TMenuTiers> menuTiers = newMenu(this, 4, "Tiers", new TMenuTiers);
    field<TMenuAffaires> menuAffaires = newMenu(this, 5, "", new TMenuAffaires);
    field<TMenuVentes> menuVentes = newMenu(this, 6, "Ventes", new TMenuVentes);
    field<TMenuAchats> menuAchats = newMenu(this, 7, "", new TMenuAchats);
    field<TMenuChantiers> menuChantiers = newMenu(this, 8, "", new TMenuChantiers);
    field<TMenuComptabilite> menuComptabilite = newMenu(this, 9, "", new TMenuComptabilite);
    field<TMenuStocks> menuStocks = newMenu(this, 10, "", new TMenuStocks);
    field<TMenuOutils> menuOutils = newMenu(this, 11, "", new TMenuOutils);
    field<TMenuAdministration> menuAdministration = newMenu(this, 12, "", new TMenuAdministration);
    field<TMenuFenetres> menuFenetres = newMenu(this, 13, "", new TMenuFenetres);
};

 

#ifdef IMPLEMENTATION
var<TMDE_HeadMenus> mdeHeadMenu = newMenu(0, -1, "", new TMDE_HeadMenus );
#endif


// END OF FILE ********************************************************************************
