
#include "_common.h"

//-------------------------------------------------------------------------------
TCodeTST F_Creat_Doc_Ach_Manuel(TStringTST TypeDoc) {
    var<TStringTST> TypeDocListe = "";
    var<TStringTST> ClasseFiche = "";
    var<TStringTST> NomFiche="";
    var<TStringTST> Tiers="";

    if(TypeDoc == "DF") {
        ClasseFiche = "TFch_PCF_Devis";
        NomFiche = "Demande de prix";
        Tiers = "F1"; 
    }else if( TypeDoc == "CF" ) {
        ClasseFiche = "TFch_PCF_Cde";
        NomFiche = "Commande fournisseur";
        Tiers = "F1";
    }else if( TypeDoc ==  "LF" ) {
        ClasseFiche = "TFch_PCF_Reception";
        NomFiche = "Bon de r�ception";
        Tiers = "F1";
    }else if( TypeDoc ==  "FF" ) {
        ClasseFiche = "TFch_PCF_Facture";
        NomFiche = "Facture fournisseur";
        Tiers = "F1";
    }else if( TypeDoc ==  "RF" ) {
        ClasseFiche = "TFch_PCF_Renvoi";
        NomFiche = "Renvoi de marchandises";
        Tiers = "F1";
            
        // On alimente un peu le stock
        P_Creat_Ent_Stk();
        
    }else if( TypeDoc ==  "AF" ) {
        TypeDocListe = "FF";
        ClasseFiche = "TFch_PCF_Facture";
        NomFiche = "Facture fournisseur";
        Tiers = "F1";
            
        // On alimente un peu le stock
        P_Creat_Ent_Stk();
        
    }else if( TypeDoc ==  "AO" ) {
        ClasseFiche = "TFch_PCSst_AO";
        NomFiche = "Appel d'offre";
        Tiers = "ST1";
    }else if( TypeDoc ==  "CS" ) {
        ClasseFiche = "TFch_PCSst_Cde";
        NomFiche = "Commande sous-traitant";
        Tiers = "ST1";
    }else if( TypeDoc ==  "FS" ) {
        ClasseFiche = "TFch_PCSst_Facture";
        NomFiche = "Facture sous-traitant";
        Tiers = "ST1";
    }else if( TypeDoc ==  "AS" ) {
        TypeDocListe = "FS";
        ClasseFiche = "TFch_PCSst_Facture";
        NomFiche = "Facture sous-traitant";
        Tiers = "ST1";
    }             
    
    if( TypeDocListe == "" ) {
       TypeDocListe = TypeDoc ;
    }
    
    // Ouverture de la liste
    P_Open_Lst_Ach(TypeDocListe,"");
    // Cr�ation du doc   
    Sys.Keys("[Ins]");
    // On attend que la fiche s'ouvre
    var<TWinTST> mafiche = F_Fiche(ClasseFiche, NomFiche);
    //On stock dans le press papier le code du doc 
    Sys.Keys("^c");
    var<TCodeTST> CodeDoc = Sys.Clipboard();
            
    if( TypeDoc == "AF" || TypeDoc == "AS" ) {
        //On passe au type avoir
        Sys.Keys("[Tab]");
        delay(50);
        Sys.Keys("a");
        delay(50);
    }
    
    // On se positionne sur le code tiers
    if( TypeDoc == "FF" || TypeDoc == "FS" ) {
        Sys.Keys("[Tab][Tab]");
        delay(50);
    }else{
        Sys.Keys("[Tab]");
        delay(50);
    }

    // On saisi le tiers
    Sys.Keys(Tiers);
    delay(50);
    Sys.Keys("[Tab]");
    // On va sur les lignes
    Sys.Keys("~l");
    delay(1000);
    // Saisie de ligne
    P_Inser_Lig_Ach();
    
    // On valide le document
    Sys.Keys("[F2]");
    delay(500);
    // On ferme tout
    P_CloseAllForm();
    
    return CodeDoc;
}

//-------------------------------------------------------------------------------
void P_Transfert_Doc_Ach(TStringTST TypeDoc, TCodeTST CodeDocATransferer) {
    var<TStringTST> TypeDocListe = "";
    var<TStringTST> ClasseFiche = "";
    var<TStringTST> NomFiche = "";
    var<TStringTST> Tiers = "";

    if( TypeDoc ==  "DF" ) {
        ClasseFiche = "TFch_PCF_Cde";
        NomFiche = "Commande fournisseur";
        Tiers = "F1"; 
    }else if( TypeDoc ==  "CF" ) {
        ClasseFiche = "TFch_PCF_Reception";
        NomFiche = "Bon de r�ception";
        Tiers = "F1";
    }else if( TypeDoc ==  "LF" ) {
        ClasseFiche = "TFch_PCF_Facture";
        NomFiche = "Facture fournisseur";
        Tiers = "F1";
    }else if( TypeDoc ==  "FF" ) {
        ClasseFiche = "TFch_PCF_Facture";
        NomFiche = "Facture fournisseur";
        Tiers = "F1";
    }else if( TypeDoc ==  "AO" ) {
        ClasseFiche = "TFch_PCSst_Cde";
        NomFiche = "Commande sous-traitant";
        Tiers = "ST1";
    }else if( TypeDoc ==  "CS" ) {
        ClasseFiche = "TFch_PCSst_Facture";
        NomFiche = "Facture sous-traitant";
        Tiers = "ST1";
    }else if( TypeDoc ==  "FS" ) {
        ClasseFiche = "TFch_PCSst_Facture";
        NomFiche = "Facture sous-traitant";
        Tiers = "ST1";           
    }

    
    if( TypeDocListe == "" ) {
       TypeDocListe = TypeDoc; 
    }
    
    // Ouverture de la liste
    P_Open_Lst_Ach(TypeDocListe,CodeDocATransferer);
    //Lancement du transfert   
    Sys.Keys("~t");
    // On attend que la fiche s'ouvre
    var<TWinTST> mafiche = F_Principale().WaitWindow("TFrmTransforme*", "Transformation*", -1, 50000);
    //On Valide la fen�tre de transformation
    Sys.Keys("[F2]");
    //On attend que la fen�tre de confirmation s'ouvre
    var<TWinTST> monmessage = F_Message("TApiMessageDlg", "Transformation");
    //On confirme
    F_Message_BtClic(monmessage,"Oui");
	// On attend que la fiche s'ouvre
    mafiche = F_Fiche(ClasseFiche,NomFiche);
    // On valide le document
    Sys.Keys("[F2]");
    delay(500);
    // On ferme tout
    P_CloseAllForm();
}





//-------------------------------------------------------------------------------
void P_Open_Lst_Ach(TStringTST TypeDoc, TCodeTST _code) {

    F_Principale();
    
    //On ouvre la liste 
    Sys.Keys("~h");
    delay(200);

    var<TWinTST> maliste;
    if( TypeDoc ==  "DF" ) {
        Sys.Keys("d");
        maliste = F_Liste("Demandes de prix");
    }else if( TypeDoc ==  "CF" ) {
        Sys.Keys("c");
        maliste = F_Liste("Commandes fournisseurs");
    }else if( TypeDoc ==  "LF" ) {
        Sys.Keys("b");
        maliste = F_Liste("Bons de r�ception");
    }else if( TypeDoc ==  "FF" ) {
        Sys.Keys("f");
        maliste = F_Liste("Factures fournisseurs");
    }else if( TypeDoc ==  "RF" ) {
        Sys.Keys("r");
        maliste = F_Liste("Renvois de marchandises");
    }else if( TypeDoc ==  "AO" ) {
        Sys.Keys("p");
        maliste = F_Liste("Appels d'offre");
    }else if( TypeDoc ==  "CS" ) {
        Sys.Keys("n");
        maliste = F_Liste("Commandes sous-traitants");
    }else if( TypeDoc ==  "FS" ) {
        Sys.Keys("u");
        maliste = F_Liste("Factures sous-traitants");
    }else{
        Log.Error("Type de liste inconnue");
    } 
    
    //On donne le focus � la recherche
    maliste.VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").SetFocus();
    delay(200);
    
    //On filtre la liste
    F_Liste_Filtre( maliste, _code);
}


//------------------------------------------------------------------------------- 
void P_Inser_Lig_Ach() {

    //On saisit une ligne de commentaire
    Sys.Keys("[Tab][Tab]");
    delay(50);
    Sys.Keys("Ligne de commentaire 1");
    delay(50);
    //On se positionne sur la 3 eme ligne 
    Sys.Keys("[Down][Down]");
    delay(50);
    //On insert la fourniture MAT01 on saisie une qt� de 5
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("MAT01");
    delay(500);
    for(int i=0; i<4; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    Sys.Keys("5");
    delay(50);
    //On se positionne sur la 4 eme ligne 
    Sys.Keys("[Down]");
    delay(50);
    //On insert la fourniture MAT02 
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("MAT02");
    delay(500);
    //On se positionne sur la 5 eme ligne 
    Sys.Keys("[Down]");
    delay(50);
    //On insert la fourniture CIM 
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("CIM");
    delay(500);
    //On se positionne sur la 6 eme ligne 
    Sys.Keys("[Down]");
    delay(50);
    //On insert une fourniture non rifirencie pour une qt� de 3 et un PU de 15.17
    Sys.Keys("m");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("MAT non ref");
    for(int i=0; i<3; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    Sys.Keys("3.3");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("15.17");
    delay(50);
    //On se positionne sur la 8 eme ligne 
    Sys.Keys("[Down]");
    delay(50);
    Sys.Keys("[Down]");
    delay(50);
    //On insert la fourniture MAT02 
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("+");
    delay(50);
    Sys.Keys("[Tab]");
    delay(500);
} 

//========================================================= 


//========================================================= 
void P_Creat_Ent_Stk() {

    //On active la fenetre principale de l'application
    F_Principale();
            
    //On ouvre la liste des entr�es de stock 
    Sys.Keys("~s");
    delay(200);
    Sys.Keys("e");
    delay(200);
    
    var<TWinTST> maliste = F_Liste("Entr�es de stock");
    //Cr�ation d'une entr�e de stock   
    Sys.Keys("[Ins]");
    //On attend que la fiche s'ouvre
    var<TWinTST> mafiche = F_Fiche("TfchMvtEntStock", "Entr�e de stock");
    //On se positionne sur la r�f�rence
    for(int i=0; i<4; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    
    //On insert MAT01 pour une qt� de 10
    Sys.Keys("MAT01");
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("10");
    Sys.Keys("[Down]");
    delay(100);
    //On insert MAT02 pour une qt� de 10
    Sys.Keys("MAT02");
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("10");
    Sys.Keys("[Down]");
    delay(100);
    //On insert CIM pour une qt� de 10
    Sys.Keys("CIM");
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    Sys.Keys("10000");
    Sys.Keys("[Down]");
    delay(100);

    //On valide la fiche
    Sys.Keys("[F2]");
    delay(500);

    //On ferme tout
    P_CloseAllForm();
    
} 
// END OF FILE ****************************************************************************
