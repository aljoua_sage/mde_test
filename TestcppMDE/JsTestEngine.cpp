//*****************************************************************************************
// TEST ENGINE Java script implementation
//
//*****************************************************************************************
#pragma once

#include "cppTestEngine.h"

#include "MdeUI_Export.h"  // pour l'instance mde

#include "Widgets_Unit.cpp"  // pour UIaction
//USEUNIT Widgets_Unit

#include "MdeAppli_Unit.cpp" // pour mde
//USEUNIT MdeAppli_Unit


//***************************************************************************************
inline bool checkFailAction( TUIaction _action )  {
    if (_action.typAction == ACT_OPEN ) {
       
       return _action.dlgForm.checkIsOpen();

    }else{
       Log.Error("Type d'action inconnue");
       return 1; // fail
    }
}

// END OF FILE ********************************************************************************
