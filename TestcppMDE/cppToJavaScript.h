//*****************************************************************************************
// Java Script emulation, from C++ 
//
// Here, we propose C++ classes that emulate Java script classes and function 
// and that allow easy C++ to java script translation.
//   We need, at least:
//     - Strings...
//     - Arrays...
// 
//*****************************************************************************************

#ifndef _CPPTOJAVASCRIPT_H
#define _CPPTOJAVASCRIPT_H

#define JSobject struct
#define DECLAR_METHOD( typRet, methodName, protoTyp ) typRet methodName protoTyp
#define CAST( destTyp, obj ) (*(destTyp*)&obj)
#define JSCODE( code )
#define NULLREF( typ ) (*(var<typ>*)0)
#define THIS (*this)
#define EXTEND( filleClass, baseClass )

// An alias to var, for integral types.
template<class typ> class varI {
public:
    varI() {}
    varI(const typ &_other) {}
    varI& operator = (const typ &_other) { return *this; }
    varI& operator = (const varI &_other) { return *this; }
    operator typ & () { return *(typ*)(0); } // cast operator
};

// An alias to var, for object types.
template<class typ> class var : public varI<typ>, public typ  {
public:
   var() {}
   var(const typ *_pOther) {}
   var(const typ &_other) {}
   var& operator = (const typ &_other) { return *this; }
   var& operator = (const var &_other) { return *this; }
   bool operator == (const typ &_other) { return false; }
   bool operator != (const typ &_other) { return false; }
};


class TStringTST; //FORWARD

// The Java script Array
struct Array {
   // Note: we DO NOT propose constructor with one int param (array length)
   Array() {}
   Array(const TStringTST &_str) {}
   Array(const TStringTST &_str1, const TStringTST &_str2) {}
   Array(const TStringTST &_str1, const TStringTST &_str2, const TStringTST &_str3) {}
};

// The java script array
template<class typ> class varArray {
public:
    varArray(const varArray &_other) {}  // copie constructor
    varArray(Array * _newlyCreated) {}   // copie constructor for varArray<> = new Array()
    int length;
    typ & operator [](int _idx) { return *(typ*)(0); }
    void push(const typ & _val) {}
};


template<class typ> using field = var<typ>;
template<class typ> using fieldI = varI<typ>;
template<class typ> using fieldR = var<typ> &;




// An alias to constante
template<class typ> class const_js : public typ {
public:
    const_js() {}
    const_js(const char *_iniVal) {}
    const_js( int _iniVal) {}
    const_js( double _iniVal) {}
    bool operator == (const typ &_other) { return false; }
    bool operator != (const typ &_other) { return false; }
    const_js & operator = (const char *_iniVal) = delete;
    const_js & operator = (int _iniVal) = delete; 
    const_js & operator = (double _iniVal) = delete; 
};
template<class typ> bool operator == (const typ &_val1, const const_js<typ> &_val2 ) { return true;}
template<class typ> bool operator != (const typ &_val1, const const_js<typ> &_val2) { return true; }

// A string class, like in java.
class TStringTST {
  public:
    TStringTST() {}
    TStringTST( const char *_str) {}
    explicit TStringTST( int number) {}
    bool operator == (const TStringTST &_other) const { return false;}
    bool operator != (const TStringTST &_other) const { return false; }
    bool operator == (const char *_other) const { return false; }
    bool operator != (const char *_other) const { return false; }

    varArray<TStringTST> split(const TStringTST &_sep ) { return 0; }
    bool indexOf( const TStringTST &_other) { return false; }
};
// Java script concatenation of 2 strings
inline TStringTST operator + (const TStringTST &_str1, const TStringTST &_str2 ) { return TStringTST(); }

// Fonctions JAVASCRIPT résidentes
inline int parseInt(TStringTST _str) { return 0;}
inline double parseFloat(TStringTST _str) { return 0.0; }
inline void delay(int _dTms) {}


// Some string functions, comming from VB script.
int Len( TStringTST _str);
TStringTST Left(TStringTST _str, int len);
TStringTST Right( TStringTST _str, int len);
TStringTST Trim( TStringTST _str);
TStringTST UCase( TStringTST _str);
TStringTST Mid0( TStringTST _str, int _start);
int InStr0( TStringTST _str, TStringTST _pattern);
TStringTST Replace( TStringTST _what, TStringTST _searched, TStringTST _replaceBy );
bool Contains(TStringTST _str, TStringTST _pattern);
varArray<TStringTST> Split( TStringTST _str, TStringTST _sep);


//extern TStringTST Array( TStringTST  _elem1);
//extern TStringTST Array( TStringTST  _elem1, TStringTST  _elem2);
//extern TStringTST Array( TStringTST  _elem1, TStringTST  _elem2, TStringTST  _elem3);


#endif
// END OF FILE ********************************************************************************
