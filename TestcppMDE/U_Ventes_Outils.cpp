
#include "_common.h"
//------------------------------------------------------------------------------- 
// Fonction permettant de Renvoyer un bouton de la barre d'icones des devis
// ObjFiche soit �tre d�fini par une F_Fiche
// Btaction : Enregistrer...
//-------------------------------------------------------------------------------
TWinTST F_DocVte_BarIcone_GetBt( TWinTST objFiche, TStringTST Btaction) {
  return objFiche;
#if 0
    var<TWinTST> Bt = objFiche.FindChild(Array("Params"),Array("*" + Btaction + "*"),5);
    if( ! Bt.Exists ) {        
        Log.Error("Bouton non trouv�");
    }

    return Bt;
#endif
} 

//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un bouton de la barre d'ic�ne des devis
// ObjFiche soit �tre d�fini par une F_Fiche
// Btaction : Enregistrer...
//-------------------------------------------------------------------------------
TWinTST F_DocVte_BarIconeBtClic(TWinTST objFiche, TStringTST Btaction) {
    if( Btaction == "Enregistrer" ) {
        Btaction = "Enregistrer";
    }else if(Btaction == "Imprimer" ) {
        Btaction = "Imprimer";
    }else if (Btaction == "Apercu") {
         Btaction = "Apercu";    
    }else{             
         Log.Error("L'action du bouton n'est pas d�finie.");
    }//End Select
    
    var<TWinTST> Bt = F_DocVte_BarIcone_GetBt(objFiche,Btaction);
    Bt.Click();
       
    return Bt;
} 


//' Correspond � des objets (ex : fen�tre) qui sont fr�quement pass�es entre les proc�dures
//var W;
//var X;
//var Y;
//var Z;
//
//var Code_Devis;

//-------------------------------------------------------------------------------
void Open_Lst_Devis() { 
    //log.message "Ouverture de la liste des devis client"
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    tfmMenu.Keys("~v");
    tfmMenu.MainMenu.Click("[6]|[0]");
    stdScrollBox.Window("TStdFormListe", "Devis clients : Coordonn�es et montants").VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").Click(11, 6);
}

//-------------------------------------------------------------------------------
void Open_Lst_Cmd() { 
    //log.message "Ouverture de la liste des devis client"
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    tfmMenu.Keys("~v");
    tfmMenu.MainMenu.Click("[6]|[2]");
    stdScrollBox.Window("TStdFormListe", "Commandes clients : Coordonn�es et montants").VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").Click(11, 6);
}

//-------------------------------------------------------------------------------
// On cr�er un devis, on saisi le client, on va sur les lignes
//-------------------------------------------------------------------------------
void P_Creat_Devis() { 

    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[6]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> stdFormListe = stdScrollBox.Window("TStdFormListe", "Devis clients : Coordonn�es et montants");
    stdFormListe.VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 10).Click(10, 13);
    var<TWinTST> tfmDocDevCli = stdScrollBox.Window("TfmDocDevCli", "Devis [*] [Cr�ation]");
    var<TWinTST> apiPanel = tfmDocDevCli.VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteTiers");
    var<TWinTST> stdSais = apiPanel.VCLObject("saiCodeTiers");
    stdSais.Click(30, 4);
    stdSais.Keys("c1[Enter]");
    //On va sur les lignes
    apiPanel.VCLObject("saiCivilite").Keys("~^l");
}

//-------------------------------------------------------------------------------
// On cr�er un devis, on saisi le client, on va sur les lignes et on retourne le 
// code du document
//-------------------------------------------------------------------------------
TCodeTST F_Creat_Devis() {
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[6]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> stdFormListe = stdScrollBox.Window("TStdFormListe", "Devis clients : Coordonn�es et montants");
    stdFormListe.VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 10).Click(10, 13);
    var<TWinTST> tfmDocDevCli = stdScrollBox.Window("TfmDocDevCli", "Devis [*] [Cr�ation]");
    // On stock dans le clipboard le code du document
    Sys.Keys("^c");
    var<TCodeTST> outF_Creat_Devis = TCodeTST( Sys.Clipboard() );
    
    var<TWinTST> apiPanel = tfmDocDevCli.VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteTiers");
    var<TWinTST> stdSais = apiPanel.VCLObject("saiCodeTiers");
    stdSais.Click(30, 4);
    stdSais.Keys("c1[Enter]");
    //On va sur les lignes
    apiPanel.VCLObject("saiCivilite").Keys("~^l");
    return outF_Creat_Devis;
}

//-------------------------------------------------------------------------------
void P_Valide_Devis() { 
    
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    // Il faut savoir si la fiche et en mode cr�ation car si tel est le cas la validation ne fermera pas la fiche.
    // Il faudra faire des manip en plus
    var<TStringTST> Mode_Fiche="";

    if( Contains(mafiche.FullName,"[Cr�ation]") ) {
        Mode_Fiche = "Creation";            
    }
 
    // Validation de la fiche
    F_Fiche_BtClic(mafiche,"Valider");
    // Si on est en mode cr�ation il faut cliquer sur le bouton Quitter pour fermer la fiche
    if( Mode_Fiche == "Creation" ) {
        F_Fiche_BtClic(mafiche,"Quitter");
        var<TWinTST> monmessage = F_Message("*", "Devis");
        F_Message_BtClic(monmessage,"Oui");
    }
    
    var<TWinTST> ObjMessage = Sys.Process("mde").WaitWindow("TApiMessageDlg", "Avertissement", -1, 1000);
    if( ObjMessage.Exists ) {
        F_Message_BtClic(ObjMessage,"Non");
    }
    
    // On attent que la fiche soit ferm�e
    P_Fiche_WaitClose(mafiche,0);
}

//-------------------------------------------------------------------------------
void P_Valide_Cde() { 
    
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocCdeCli","Commandes");
    // Il faut savoir si la fiche et en mode cr�ation car si tel est le cas la validation ne fermera pas la fiche.
    // Il faudra faire des manip en plus

    var<TStringTST> Mode_Fiche = "";
    if( Contains(mafiche.FullName,"[Cr�ation]") ) {
        Mode_Fiche = "Creation";            
    }
 
    // Validation de la fiche
    F_Fiche_BtClic(mafiche,"Valider");
    // Si on est en mode cr�ation il faut cliquer sur le bouton Quitter pour fermer la fiche
    if( Mode_Fiche == "Creation" ) {
        F_Fiche_BtClic(mafiche,"Quitter");;
        var<TWinTST> monmessage = F_Message("*", "Commandes");
        F_Message_BtClic(monmessage,"Oui");
    }
    
    var<TWinTST> ObjMessage = Sys.Process("mde").WaitWindow("TApiMessageDlg", "Avertissement", -1, 1000);
    if( ObjMessage.Exists ) {
        F_Message_BtClic(ObjMessage,"Non");
    }
    
    // On attent que la fiche soit ferm�e
    P_Fiche_WaitClose(mafiche,0);
}


//-------------------------------------------------------------------------------
void P_Enregistre_Devis() { 
    
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    // Enregistrement de la fiche
    var<TWinTST> Bt = F_DocVte_BarIconeBtClic(mafiche,"Enregistrer");
    // On attent que le bouton soit grisi preuve que le doc est enregistr�e
    varI<int> V_Timeout=0;
    if( V_Timeout == 0 ) { V_Timeout = 10000; }
    
    if( ! Bt.WaitProperty("Enabled", false, V_Timeout) ) {
        Log.Error( "Le devis ne s'est pas enregistr�");
    }
}


//-------------------------------------------------------------------------------
// On cr�er une commande client, on saisi le client, on va sur les lignes et on retourne le 
// code du document
//-------------------------------------------------------------------------------
TStringTST F_Creat_Cmd() {
    
    Open_Lst_Cmd();

    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    stdScrollBox.Window("TStdFormListe", "Commandes clients : Coordonn�es et montants").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 10).Click(13, 12);
    var<TWinTST> TfmDocCdeCli = stdScrollBox.Window("TfmDocCdeCli", "Commandes [*] [Cr�ation]");
    // On stock dans le clipboard le code du document
    Sys.Keys("^c");
    var<TStringTST> outF_Creat_Cmd = Sys.Clipboard();
    
    var<TWinTST> stdSais = TfmDocCdeCli.VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteTiers").VCLObject("saiCodeTiers");
    stdSais.Keys("c1");
    // On va sur les lignes
    Sys.Keys("~^l");
    return outF_Creat_Cmd;
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Manuel() { 
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Saisie de la r�f�rence");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On saisi les �l�ments 
    Sys.Keys("01071806006[Down]");
    delay(200);
    Sys.Keys("01310303012[Down]");
    delay(200);
    Sys.Keys("01310306006[Down]");
    delay(200);
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Manuel_Cde() { 
    
    // On insert une tranche
    Sys.Keys("^t");
    delay(500);
    // On se position sur la d�signation
    Sys.Keys("[Enter]");
    delay(100);
    // On saisi un libell�
    Sys.Keys("Tranche");
    delay(100);
    // On va sur la colonne r�f�rence de la ligne du dessous
    Sys.Keys("[Down][Left]");
    delay(100);
    // On saisi la r�f�rence MAT01
    Sys.Keys("MAT01");
    delay(100);
    // On va sur la colonne qt�  
    for(int i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(100);
    }
    
    // On renseigne une qt� de 5
    Sys.Keys("5");
    // On va sur la colonne r�f�rence de la ligne du dessous
    Sys.Keys("[Down][Enter]");
    delay(100);
    // On saisi la r�f�rence 
    Sys.Keys("F01");
    delay(100);
    // On va sur la colonne qt�  
    for(int i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(100);
    }
    
    // On renseigne une qt� de 50
    Sys.Keys("50");
    // On va sur la colonne r�f�rence de la ligne du dessous
    Sys.Keys("[Down][Enter]");
    delay(100);
    // On saisi la r�f�rence 
    Sys.Keys("M01");
    delay(100);
    //On va sur la colonne qt�  
    for(int i = 0; i<2; i++) {
        Sys.Keys("[Enter]");
        delay(100);
    }
    
    // On renseigne une qt� de 100
    Sys.Keys("100");
    // On va sur la colonne r�f�rence de la ligne du dessous
    Sys.Keys("[Down][Enter]");
    delay(100);
    // On saisi la r�f�rence 
    Sys.Keys("A01");
    delay(100);
    // On va sur la colonne qt�  
    for (int i=0; i<3; i++) {
        Sys.Keys("[Enter]");
        delay(100);
    }
    
    // On renseigne une qt� de 8
    Sys.Keys("8");
    // On va sur la colonne r�f�rence de la ligne du dessous
    Sys.Keys("[Down][Enter]");
    delay(100);
    // On insert une ligne de type sous-total
    Sys.Keys("^o");
    delay(100);
    Sys.Keys("Sous-total");
    delay(100);
}


//-------------------------------------------------------------------------------
void P_Ins_Lig_Liste() { 
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("insertion depuis liste");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
    // On ouvre la liste
    Sys.Keys("[F4]");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("El�ments");
    // On v�rifie qu'elle est bien triie sur le code ou la r�f�rence
    F_Liste_Verif_Filtre(maliste);;
    // On insert un �l�ment (simple silection);
    //Call stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("[Enter]");
    maliste.VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("[Enter]");
    delay(500);
    // On se positionne sur la ligne suivante
    Sys.Keys("[Down][F4]");
    // Initialisation de la liste
    maliste = F_Liste("El�ments");
    // On insert 3 �l�ments (Multi-silection);
    maliste.VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("[Down] [Down] [Down] [Enter]");
    delay(200);
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Ref_Ass() { 
    
    // Ajout de la tranche
    Sys.Keys("^t");
    // On va sur la colonne D�signation
    Sys.Keys("[Enter]");
    // On saisi la d�signation
    Sys.Keys("Insertion ligne avec �l�ment associ�s");
    // On va sur la colonne r�f�rence de la ligne suivante
    Sys.Keys("[Down][Left]");
     // On saisi les �l�ments 
    Sys.Keys("MAT03[Down]");
    delay(200);
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Imp_Sec() { 

    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> stdScrollBox = mde.VCLObject("fmMenu").VCLObject("ClientControl");
    var<TWinTST> apibatMenuToolbar = stdScrollBox.Window("TfmDocDevCli", "*").VCLObject("tbMenu");
    //On clique sur l'option du menu Insertion / Import
    apibatMenuToolbar.ClickItem("&Insertion", false);
    //PR - Correction V6 ajout d'un menu etarif 
    //Call apibatMenuToolbar.PopupMenu.Click("[10]|[0]");
    apibatMenuToolbar.PopupMenu.Click("[11]|[0]");
    //On renseigne la fen�tre d'import de section et on valide
    var<TWinTST> tfmDocImpSections = stdScrollBox.Window("TfmDocImpSections", "Insirer une section");
    var<TWinTST> stdSais = tfmDocImpSections.VCLObject("panTop").VCLObject("ApiPanel5").VCLObject("saiCodeDoc");
    stdSais.Click(28, 8);
    stdSais.Keys("00000001");
    tfmDocImpSections.VCLObject("StdPanel1").VCLObject("btValider").Click(8, 9);
    //On a un message de confirmation que l'on valide
    mde.Window("TApiMessageDlg", "Insirer une section").Window("TApiPanel").Window("TStdBout", "", 2).Click(28, 9);
    delay(1000);
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Copier_Coller() { 
    var<TWinTST> stdScroll = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsLignes").VCLObject("panLignes").VCLObject("panCorps").VCLObject("panScroll").VCLObject("scLignes");
    stdScroll.SetFocus();
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "MOCHANTIER_01");
    scrollSaisieEdit.Keys("^a");
    delay(200);
    stdScroll.Keys("^c");
    delay(500);
    scrollSaisieEdit.Keys("^[End][Down]^v");
    delay(1000);
}

//-------------------------------------------------------------------------------
void P_Ins_Lig_Phase_Type() { 
    var<TWinTST> stdScroll = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "*").VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsLignes").VCLObject("panLignes").VCLObject("panCorps").VCLObject("panScroll").VCLObject("scLignes");
    stdScroll.Keys("^t");
    stdScroll.Keys("01[Enter]");
}

//-------------------------------------------------------------------------------
TCodeTST F_Copie_Devis(TCodeTST CodeDevisACopier) {
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");;
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste,CodeDevisACopier);;
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Copier");;
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    // On stock dans le clipboard le code du document
    Sys.Keys("^c");
    var<TStringTST> outF_Copie_Devis = Sys.Clipboard();
    
    // Validation de la fiche
    P_Valide_Devis();
    // On ferme tout
    P_CloseAllForm();

    return TCodeTST(outF_Copie_Devis);
}

//-------------------------------------------------------------------------------
TWinTST F_Open_Devis(TCodeTST CodeDevisAOuvrir) {
    // On lance l'option du menu Ventes|Devis Quantitatifs et estimatifs
    F_Menu_Click("Ventes|Devis Quantitatifs et estimatifs");
    // Initialisation de la liste
    var<TWinTST> maliste = F_Liste("Devis clients");
    //On filtre la liste
    F_Liste_Filtre(maliste, CodeDevisAOuvrir);
    // Lancement de la copie
    F_Liste_BtClic(maliste,"Modifier");
    // Initialisation de la Fiche
    var<TWinTST> mafiche = F_Fiche("TfmDocDevCli","Devis");
    return mafiche;
}  

//**** END OF FILE **********************************************************************************************
