//'USEUNIT U_Bib_Outils

#include "_common.h"

//-------------------------------------------------------------------------------
void T_Compta_0001() { //Transfert comptable
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[9]|[0]");
    var<TWinTST> fch_Transfert = tfmMenu.VCLObject("ClientControl").Window("TFch_Transfert", "Transfert comptable");
    var<TWinTST> stdSais = fch_Transfert.VCLObject("panTop").VCLObject("PeriodTransfert").Window("TStdSais", "", 2);
    stdSais.Click(82, 10);
    stdSais.Click(80, 10);
    var<TWinTST> tfmCalendrier = mde.Window("TfmCalendrier", "Calendrier");
    var<TWinTST> stdCalendar = tfmCalendrier.VCLObject("Calendar");
    var<TWinTST> stdBoutImage = stdCalendar.VCLObject("btPrevious");
    stdBoutImage.Click(5, 6);
    stdBoutImage.Click(5, 6);
    stdBoutImage.Click(5, 6);
    stdBoutImage.Click(5, 6);
    stdCalendar.Click(132, 43);
    tfmCalendrier.VCLObject("StdPanel1").VCLObject("StdBout2").Click(21, 16);
    fch_Transfert.VCLObject("panCrit").VCLObject("pcCrit").VCLObject("tsCritVte").VCLObject("coTypDocCli_F").Click(4, 3);
    fch_Transfert.VCLObject("Panel").VCLObject("btValider").Click(41, 10);
    mde.Window("TApiMessageDlg", "Sage 100 Multi Devis Entreprise").Window("TApiPanel").Window("TStdBout").Click(18, 6);
    fch_Transfert.Close();
    tfmMenu.Keys("~m");
    tfmMenu.MainMenu.Click("[9]|[0]");
}

//**** END OF FILE **************************************