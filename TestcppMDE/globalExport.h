//*****************************************************************************************
// Publication (exportation) of all JavaScript global functions and vars
//
// 
//*****************************************************************************************

#ifndef _GLOBALEXPORT_H
#define _GLOBALEXPORT_H

#include "cppToJavaScript.h"  // for TStringTST, var<> ....
#include "typesApplicatif.h"  // for TCodeTST...

// TEST SUITE global var

// Test Automaton
extern varI<bool> G_Restart;
extern varI<bool> G_IsError;
extern varI<bool> G_P_Init_Automate;


extern TCodeTST newcode;

extern void P_Transfert_Doc_Ach(TStringTST _typDoc, TCodeTST _code );
extern TCodeTST F_Creat_Doc_Ach_Manuel( TStringTST _typDoc);
extern TWinTST F_Open_Devis(TCodeTST _code );
extern void P_Enregistre_Devis();
extern TCodeTST F_Copie_Devis(TCodeTST _code );
extern TWinTST F_Fiche( TStringTST _winClass, TStringTST _titleBegin);
extern TWinTST F_Message( TStringTST _winClass, TStringTST _titleBegin);
extern void P_Fiche_WaitClose(TWinTST Fiche, int Timeout);

extern bool F_Message_BtClic(TWinTST _win, TStringTST _btnName );
extern bool F_Fiche_BtClic(TWinTST _winFich, TStringTST _btnName );
extern TWinTST F_Liste(TStringTST _listname);
extern void F_Liste_Filtre(TWinTST _listWin, TCodeTST _code);
extern void F_Liste_BtClic(TWinTST _listWin, TStringTST _btnAction );
extern void P_Open_Lst_Ach( TStringTST _typDoc, TCodeTST _code);
extern void F_Menu_Click(TStringTST _optionMenu);
extern TStringTST F_Liste_Verif_Filtre(TWinTST objListe);

extern TWinTST F_DocVte_BarIcone_GetBt(TWinTST objFiche, TStringTST Btaction);
extern TWinTST F_DocVte_BarIconeBtClic(TWinTST objFiche, TStringTST Btaction);
extern TWinTST F_Appercu(TStringTST ClasseAppercu);
extern bool F_Appercu_BtClic(TWinTST objFiche, TStringTST Btaction);

extern void P_Creer_Copier_Modifier_Supprimer(TStringTST _path, TStringTST _title, TStringTST _subTitle, int _code );
extern int F_Creer(TStringTST _optionMenu, TStringTST _nomListe, TStringTST _nomFiche, int _code);
extern int F_Copier(TStringTST _optionMenu, TStringTST _nomListe, TStringTST _nomFiche, int _code);
extern int F_Modifier(TStringTST _optionMenu, TStringTST _nomListe, TStringTST _nomFiche, int _code);
extern int F_Supprimer(TStringTST _optionMenu, TStringTST _nomListe, TStringTST _nomFiche, int _code);
extern void P_Fiche_Creation_Saisies(TWinTST _fiche);
extern void P_Fiche_Modif_Saisies(TWinTST _fiche);

extern TWinTST F_Principale();
extern void P_Creat_Ent_Stk();
extern void P_Creat_Ent_Stk();
extern void P_Inser_Lig_Ach();
extern TCodeTST F_Creat_Ouvtx();
extern void P_Open_Lst_Elem();

extern void Open_Lst_Devis();
extern void P_Creat_Devis();
extern TCodeTST F_Creat_Devis();
extern void P_Valide_Devis();
extern void P_CloseAllForm();
extern void P_Insert_Compo();

extern void P_Ins_Lig_Manuel();
extern void P_Ins_Lig_Liste();
extern void P_Ins_Lig_Ref_Ass();
extern void P_Ins_Lig_Imp_Sec();
extern void P_Ins_Lig_Copier_Coller();
extern void P_Ins_Lig_Phase_Type();


extern void Open_Lst_Cmd();
extern void P_Ins_Lig_Manuel_Cde();
extern void P_Valide_Cde();
extern TStringTST F_Creat_Cmd();


extern void P_Del_File( TStringTST  _filePath);
extern bool F_Find_File( TStringTST  _filePath);

extern void P_Init_Automate();
extern void P_Fin_Automate();
extern void P_FermeApp();
extern void F_Get_Test_Name();
extern TStringTST F_Get_Last_TestNameCount();
extern void F_Init_Rep_Oracles();
extern void P_Debut();
extern void P_Fin();

extern void P_Sync_DateHours_System();
extern void P_MAJ_DateHours_System(TStringTST _date, TStringTST _hour);

extern void P_Exec_Commande(TStringTST _cmd);
extern TStringTST F_Get_ParamsDecode(TStringTST  Params);
extern TTypSaisie F_Get_TypeSaisieParams(TStringTST  Params);

extern const_js<TTypSaisie> C_TypeSaisie_Nombre, C_TypeSaisie_Alpha, C_TypeSaisie_Multi,
                            C_TypeSaisie_Date, C_TypeSaisie_Inconnu;

// SOME FORWARD DECLARATIONS *********
varArray<TStringTST> F_Get_DicoParamsSMDE(); // FORWARD

struct MDE_Appli;
extern var<MDE_Appli> mde;

#endif
// END OF FILE ********************************************************************************
