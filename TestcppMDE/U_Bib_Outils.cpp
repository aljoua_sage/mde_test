//'USEUNIT U_Commun

#include "_common.h"

//-------------------------------------------------------------------------------
void P_Open_Lst_Elem() { //Ouverture de la liste des �l�ments

    //On active la fenetre principale de l'application
    F_Principale().SetFocus();
    
    //On ouvre la liste des �l�ments
    Sys.Keys("~b");
    delay(200);
    Sys.Keys("e");
    var<TWinTST> mafiche = F_Fiche("TStdFormListe", "El�ments :");
    //On donne le focus � la recherche
    mafiche.VCLObject("PanVScroll").VCLObject("panFiltres").VCLObject("saiQuickSearch").SetFocus();
}       

//-------------------------------------------------------------------------------    
TCodeTST F_Creat_Ouvtx() {

    //On ouvre une fiche �l�ments en cr�ation
    Sys.Keys("[Ins]");
    var<TWinTST> mafiche = F_Fiche("TfmElement", "Fiche Eliment");
    //On v�rifie la nature Travaux
    var<TStringTST> Nature = mafiche.VCLObject("panEntete").VCLObject("saiNature").WndCaption;
    if( Nature != "Travaux" ) { 
        mafiche.VCLObject("panEntete").VCLObject("saiNature").Keys("T");
    }
    
    //On stocke le code �l�ment que l'on va cr�er
    Sys.Keys("^c");
    var<TCodeTST> Code_Elem = Sys.Clipboard();
    
    //On saisi le code de l'�l�ment
    Sys.Keys( Code_Elem.toString() );
    delay(50);
    //On va sur le libell�
    Sys.Keys("[Tab]");
    //On saisi le libell�
    Sys.Keys(Code_Elem.toString());
    delay(50);
    return Code_Elem;
}

//-------------------------------------------------------------------------------
void P_Insert_Compo() {

    //On va sur l'onglet composante
    Sys.Keys("~o");
    delay(50);
    //On se positionne sur la r�f�rence
    Sys.Keys("[Tab]");
    delay(50);
    //On ajoute la compo F01 pour une qt� de 4
    Sys.Keys("F01");
    delay(50);
    Sys.Keys("[Tab][Tab][Tab][Tab]");
    delay(50);
    Sys.Keys("4");
    delay(50);
    //On va sur la colonne r�f�rence de la compo suivante
    Sys.Keys("[Tab][Tab][Tab]");
    //On ajoute la compo M01 pour une qt� de 2
    Sys.Keys("M01");
    delay(50);
    Sys.Keys("[Tab][Tab][Tab]");
    delay(50);
    Sys.Keys("2");
    //On va sur la colonne r�f�rence de la compo suivante
    for(int i=0; i<4; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    
    //On ajoute la compo 38860306003 pour une qt� de 1
    Sys.Keys("38860306003");
    Sys.Keys("[Tab]");
    delay(500);
    //On insert une ligne de composante
    Sys.Keys("^[Ins]");
    delay(50);
    //On choisi la nature St ma�onnerie
    Sys.Keys("s");
    delay(50);
    //On va sur le libell�
    Sys.Keys("[Tab][Tab]");
    //On saisi le libell� Non ref st ma�onnerie 
    Sys.Keys("Non ref st ma�onnerie");
    delay(500);
    //On se positionne sur la colonne d�bours�
    for(int i=0; i<7; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    
    //On saisi un d�bours� de 100
    Sys.Keys("100");
}

//***** END OF FILE ******************************