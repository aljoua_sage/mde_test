//*****************************************************************************************
// Java script extension et utilitaires
//
//*****************************************************************************************
#pragma once

#include "cppToJavaScript.h"

// Extension de JavaScript: String.startsWith(..)
inline bool str_startsWith( TStringTST _strIn, TStringTST _pattern ) {
    return _strIn.indexOf(_pattern) == 0;
}

// Numeric <--> string conversion

//*************************************************************************************
inline int toInt(TStringTST _str) {
    return parseInt(_str);
}

//*************************************************************************************
inline double toReal(TStringTST _str) {
    return parseFloat(_str);
}

//*************************************************************************************
inline TStringTST toStr(double fVal) {
  JSCODE(return fVal.toString(); )
  return ""; // for C++ stub
}

//*************************************************************************************
// Implementation JS de NULLREF
JSCODE( function NULLREF( typ ) { return 0; }  )

//*************************************************************************************
// Implementation JS extend
JSCODE( function EXTEND(sub, base ) { )

JSCODE(  for(var key in base.prototype) { )
JSCODE(      sub.prototype[key] = base.prototype[key]; )
JSCODE(  } )
JSCODE(})



// END OF FILE ********************************************************************************
