
#include "_common.h"

#include "JavaScriptExtens.cpp"  // for toStr...
//USEUNIT JavaScriptExtens

void T_Initialisation_0001() {  //Famille fournisseur

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Tiers|Familles|Familles Fournisseurs","Familles Fournisseurs :","Famille Fournisseurs",0);
}

void T_Initialisation_0002() { //Secteurs g�ographiques

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Secteurs g�ographiques","Secteurs g�ographiques :","Secteur giographique",0);
}


void T_Initialisation_0003() { //d�p�t

    P_Creer_Copier_Modifier_Supprimer("Initialisation|D�p�ts","d�p�ts","d�p�t",3);
}

void T_Initialisation_0004() { //Biblioth�ques
    
    varI<int> Code = F_Creer("Initialisation|El�ments|Biblioth�ques","Biblioth�ques","Biblioth�que",1);
    Code = F_Modifier("Initialisation|El�ments|Biblioth�ques","Biblioth�ques","Biblioth�que",Code);
    Code = F_Supprimer("Initialisation|El�ments|Biblioth�ques","Biblioth�ques","Biblioth�que",Code);
}


void T_Initialisation_0005() { //Unit�s de mesure

    P_Creer_Copier_Modifier_Supprimer("Initialisation|El�ments|Unit�s de mesure","Unit�s","Unit�",0);
}


void T_Initialisation_0006() { //Bordereaux de prix

    P_Creer_Copier_Modifier_Supprimer("Initialisation|El�ments|Bordereaux de prix","Bordereaux","Bordereau",0);
}

void T_Initialisation_0007() { //Famille clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Tiers|Familles|Familles Clients","Familles Clients :","Famille Clients",0);
}

void T_Initialisation_0008() {  //Famille sous-traitants

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Tiers|Familles|Familles Sous-traitants","Familles Sous-traitants :","Famille Sous-traitants",0);
}

void T_Initialisation_0016() {  //Textes Standards

    P_Creer_Copier_Modifier_Supprimer("Initialisation|El�ments|Textes standards","Textes standards :","Texte standard",3);
}

void T_Initialisation_0021() {  //Lettres-types

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Lettres-types","Lettres-types :","Lettre-type",3);
}

void T_Initialisation_0022() {  //Types d'actions commerciales

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Affaires|Types d'actions commerciales","Types d'actions commerciales :","Type d'action commerciale",5);
}

void T_Initialisation_0023() {  //Calendriers-types

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Affaires|Calendriers-types","Calendriers-types :","Calendrier-type",3);
}

void T_Initialisation_0024() {  //Documents clients Groupes Devis Clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Devis Clients","Groupes de devis Clients :","Groupe de devis Clients",4);
}

void T_Initialisation_0025() {  //Documents clients Groupes Commandes Clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Commandes Clients","Groupes de commandes Clients :","Groupe de commandes Clients",0);
}

void T_Initialisation_0026() {  //Documents clients Groupes Bons de livraisons Clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Bons de livraisons Clients","Groupes de BL Clients :","Groupe de BL Clients",0);
}

void T_Initialisation_0027() {  //Documents clients Groupes Bons de retour Clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Bons de retour Clients","Groupes de BR Clients :","Groupe de BR Clients",0);
}

void T_Initialisation_0028() {  //Documents clients Groupes Situations de travaux

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Situations de travaux","Groupes de situations de travaux :","Groupe de situations de travaux",0);
}

void T_Initialisation_0029() {  //Documents clients Groupes Factures Clients

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Factures Clients","Groupes de factures Clients :","Groupe de factures Clients",0);
}

void T_Initialisation_0030() {  //Documents Fournisseurs Groupes Demandes de prix Fournisseurs

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Demandes de prix Fournisseurs","Groupes de demandes de prix Fournisseurs :","Groupe de demandes de prix Fournisseurs",0);
}

void T_Initialisation_0031() {  //Documents Fournisseurs Groupes Commandes Fournisseurs

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Commandes Fournisseurs","Groupes de commandes Fournisseurs :","Groupe de commandes Fournisseurs",0);
}

void T_Initialisation_0032() {  //Documents Fournisseurs Groupes Bons de r�ception Fournisseurs

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Bons de r�ception Fournisseurs","Groupes Bons de r�ception Fournisseurs :","Groupe Bons de r�ception Fournisseurs",0);
}

void T_Initialisation_0033() {  //Documents Fournisseurs Groupes Bons de retour Fournisseurs

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Bons de retour Fournisseurs","Groupes Bons de retour Fournisseurs :","Groupe Bons de retour Fournisseurs",0);
}

void T_Initialisation_0034() {  //Documents Fournisseurs Groupes Factures Fournisseurs

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Factures Fournisseurs","Groupes de factures Fournisseurs :","Groupe de factures Fournisseurs",0);
}

void T_Initialisation_0035() {  //Documents Sous-traitants Groupes Appels d'offre Sous-traitants

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Appels d'offre Sous-traitants","Groupes d'appels d'offre Sous-traitants :","Groupe d'appels d'offre Sous-traitants",0);
}

void T_Initialisation_0036() {  //Documents Sous-traitants Groupes Commandes Sous-traitants

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Commandes Sous-traitants","Groupes de commandes Sous-traitants :","Groupe de commandes Sous-traitants",0);
}

void T_Initialisation_0037() {  //Documents Sous-traitants Groupes Factures Sous-traitants

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Factures Sous-traitants","Groupes de factures Sous-traitants :","Groupe de factures Sous-traitants",0);
}

void T_Initialisation_0039() {  //Postes de travaux

    P_Creer_Copier_Modifier_Supprimer("Initialisation|Chantiers|Postes de travaux","Postes de travaux :","Poste de travaux",4);
}

void T_Initialisation_0009() {  //Cr�ation Nature d'�l�ments Fournitures et mat�riaux
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmNatureElement = stdScrollBox.Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> stdScroll = wndTfmNatureElement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(107, 74);
    stdScroll.Window("TScrollSaisieEdit").Keys("xxx");
    stdScroll.Click(144, 78);
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("test[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("1.3[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(28, 5);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(453, 417);
}

void T_Initialisation_0010() {  //Cr�ation Nature d'�l�ments Main d'oeuvre
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(48, 42);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(79, 50);
    stdScroll.Click(87, 53);
    stdScroll.Window("TScrollSaisieEdit").Keys("mo1[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("mo1[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("1.2[Down]");
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(61, 16);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[2]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0011() {  //Cr�ation Nature d'�l�ments Mat�riel
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(28, 58);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(91, 59);
    stdScroll.Window("TScrollSaisieEdit").Keys("mt[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("mat[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("1.2[Tab]");
    stdScroll.Click(158, 78);
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(63, 18);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0012() {  //Cr�ation Nature d'�l�ments Prestations
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(44, 75);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(79, 62);
    stdScroll.Window("TScrollSaisieEdit", "", 6).Keys("ps[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("presta[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("1.2[Tab]");
    stdScroll.Click(171, 79);
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(57, 7);
    tfmMenu.Keys("~i");
    mde.Window("#32768").Click(2, 1);
}

void T_Initialisation_0013() {  //Cr�ation Nature d'�l�ments Sous-traitance
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MouseWheel(6);
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(58, 92);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(80, 76);
    stdScroll.Window("TScrollSaisieEdit").Keys("ST[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("st[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("1.2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("1.2[Down]");
    stdScroll.Click(161, 96);
    stdScroll.DblClick(145, 91);
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(50, 14);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0014() {  //Cr�ation Nature d'�l�ments Ouvrages travaux
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(51, 106);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(90, 55);
    stdScroll.Window("TScrollSaisieEdit").Keys("ouvt[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("ouvrage travaux[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Click(162, 75);
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(60, 7);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[2]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0015() {  //Cr�ation Nature d'�l�ments Ouvrages ditaillis
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> wndTfmNatureElement = tfmMenu.VCLObject("ClientControl").Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> apiPanel = wndTfmNatureElement.VCLObject("panGeneral");
    apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList").Click(62, 123);
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(81, 37);
    stdScroll.Window("TScrollSaisieEdit").Keys("oud[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("ouvrage ditaillis[BS][Tab]");
    stdScroll.Click(152, 58);
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(63, 22);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[2]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0017() {  //Types de mat�riels
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmTypeMateriel = stdScrollBox.Window("TfmTypeMateriel", "Types de mat�riels");
    var<TWinTST> stdScroll = wndTfmTypeMateriel.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(82, 91);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Click(87, 112);
    wndTfmTypeMateriel.VCLObject("StdPanel1").VCLObject("btValider").Click(47, 14);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]|[3]");
    wndTfmTypeMateriel = stdScrollBox.Window("TfmTypeMateriel", "Types de mat�riels");
    stdScroll = wndTfmTypeMateriel.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(94, 99);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Vihicules utilitaires");
    scrollSaisieEdit.Click(56, 7);
    scrollSaisieEdit.Keys(" modif");
    stdScroll.Click(259, 128);
    stdScroll.Click(150, 105);
    wndTfmTypeMateriel.VCLObject("StdPanel1").VCLObject("btValider").Click(40, 18);
    tfmMenu.Keys("~i");
    tfmMenu.Click(67, 38);
    tfmMenu.MainMenu.Click("[1]|[3]");
    wndTfmTypeMateriel = stdScrollBox.Window("TfmTypeMateriel", "Types de mat�riels");
    wndTfmTypeMateriel.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Click(117, 90);
    var<TWinTST> stdPanel = wndTfmTypeMateriel.VCLObject("StdPanel1");
    stdPanel.VCLObject("btHaut").Click(13, 10);
    stdPanel.VCLObject("btBas").Click(10, 12);
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(11, 12);
    stdPanel.VCLObject("btValider").Click(25, 18);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[2]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0018() {  //Tiers civilitis

    Sys.Process("explorer").Window("Shell_TrayWnd").Window("ReBarWindow32").Window("MSTaskSwWClass", "Applications en cours d'ex�cution").Window("MSTaskListWClass", "Applications en cours d'ex�cution").Click(265, 25);
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[5]|[0]");
    var<TWinTST> wndCivilit_s = tfmMenu.VCLObject("ClientControl").Window("TFRCivilites", "Civilitis");
    var<TWinTST> stdPanel = wndCivilit_s.VCLObject("StdPanel1");
    var<TWinTST> panel = stdPanel.Window("TPanel");
    panel.Window("TStdBoutImage", "", 2).Click(12, 11);
    var<TWinTST> stdScroll = wndCivilit_s.VCLObject("ApiPanelSousScroll").VCLObject("scrl_FrmRes");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "E.U.R.L.");
    scrollSaisieEdit.Keys("TEst[Enter]");
    stdScroll.Click(50, 27);
    scrollSaisieEdit.Click(50, 11);
    scrollSaisieEdit.Keys("[BS][BS][BS]est modif[Enter]");
    stdScroll.Click(60, 23);
    panel.Window("TStdBoutImage", "", 1).Click(16, 18);
    stdPanel.VCLObject("btValider").Click(21, 18);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0019() {  //Tiers Secteurs d'activitis
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[5]|[2]");
    var<TWinTST> wndTfmActivite = tfmMenu.VCLObject("ClientControl").Window("TfmActivite", "Secteurs d'activitis");
    var<TWinTST> apiPanel = wndTfmActivite.VCLObject("panGeneral");
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(100, 58);
    stdScroll.Window("TScrollSaisieEdit").Keys("archi[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Click(112, 59);
    var<TWinTST> stdPanel = wndTfmActivite.VCLObject("StdPanel1");
    var<TWinTST> stdBoutImage = stdPanel.VCLObject("btHaut");
    stdBoutImage.Click(10, 12);
    var<TWinTST> stdBoutImage2 = stdPanel.VCLObject("btBas");
    stdBoutImage2.Click(7, 17);
    var<TWinTST> stdBoutImage3 = stdPanel.Window("TPanel").Window("TStdBoutImage");
    stdBoutImage3.Click(20, 13);
    var<TWinTST> stdScroll2 = apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList");
    stdScroll2.Click(92, 43);
    stdScroll.Click(83, 76);
    stdScroll.Window("TScrollSaisieEdit").Keys("charg�[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Click(102, 76);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Charg�");
    scrollSaisieEdit.Click(73, 7);
    scrollSaisieEdit.Keys(" modif");
    stdScroll.Click(203, 85);
    stdScroll.Click(168, 78);
    stdBoutImage.Click(21, 14);
    stdBoutImage2.Click(8, 13);
    stdBoutImage3.Click(20, 13);
    stdScroll2.Click(99, 61);
    stdScroll.Click(85, 94);
    stdScroll.Window("TScrollSaisieEdit").Keys("clients[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Click(99, 110);
    stdScroll.Click(106, 93);
    stdScroll.Window("TScrollSaisieEdit", "Clients").Keys(" Clients modif[Enter]");
    stdScroll.Click(133, 112);
    stdScroll.Click(136, 90);
    stdBoutImage.Click(18, 15);
    stdBoutImage2.Click(7, 14);
    stdBoutImage3.Click(15, 10);
    stdScroll2.Click(88, 79);
    stdScroll.Click(96, 113);
    stdScroll.Window("TScrollSaisieEdit").Keys("fournisseurs[Down]");
    stdScroll.Click(96, 113);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Fournisseurs");
    scrollSaisieEdit.Click(118, 12);
    scrollSaisieEdit.Keys(" modif[Enter]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Enter]");
    stdScroll.Click(183, 113);
    stdBoutImage.Click(23, 13);
    stdBoutImage2.Click(17, 15);
    stdBoutImage3.Click(11, 13);
    stdScroll2.Click(40, 90);
    stdScroll.Click(90, 72);
    stdScroll.Window("TScrollSaisieEdit").Keys("sous-traitant[Enter]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Enter]");
    stdScroll.Click(90, 72);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Sous-traitant");
    scrollSaisieEdit.Click(88, 5);
    scrollSaisieEdit.Keys(" modif[Enter]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Enter]");
    stdScroll.Click(147, 81);
    stdBoutImage.Drag(12, 10, 7, 0);
    stdBoutImage2.Click(12, 16);
    stdBoutImage3.Click(16, 12);
    stdPanel.VCLObject("btValider").Click(31, 10);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0020() {  //Tiers Statuts des clients
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[5]|[4]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmStatutClient = stdScrollBox.Window("TfmStatutClient", "Statuts des clients");
    var<TWinTST> stdScroll = wndTfmStatutClient.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(108, 72);
    stdScroll.Window("TScrollSaisieEdit").Keys("test statut[Enter]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Enter]");
    stdScroll.Click(130, 75);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Test statut");
    scrollSaisieEdit.Click(95, 8);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("modif[Enter]");
    stdScroll.Click(155, 105);
    stdScroll.Click(150, 88);
    wndTfmStatutClient.VCLObject("StdPanel1").VCLObject("btValider").Click(54, 13);
    mde.Window("TApiMessageDlg", "Statuts des clients").Window("TApiPanel").Window("TStdBout").Click(48, 17);
    tfmMenu.Keys("~i");
    tfmMenu.DblClick(67, 38);
    tfmMenu.MainMenu.Click("[1]|[5]|[4]");
    wndTfmStatutClient = stdScrollBox.Window("TfmStatutClient", "Statuts des clients");
    wndTfmStatutClient.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Click(130, 79);
    var<TWinTST> stdPanel = wndTfmStatutClient.VCLObject("StdPanel1");
    stdPanel.VCLObject("btHaut").Click(14, 16);
    stdPanel.VCLObject("btBas").Click(8, 10);
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(15, 11);
    stdPanel.VCLObject("btValider").Click(61, 15);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]");
    tfmMenu.MainMenu.Close();
}

void T_Initialisation_0038() {  //Conditions de r�glement
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[11]|[6]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "Conditions de r�glement : Code et Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(5, 14);
    var<TWinTST> fchReglemEdit = stdScrollBox.Window("TFchReglemEdit", "Conditions de r�glement [Cr�ation]");
    fchReglemEdit.VCLObject("saiCode").Keys("TEST[Tab]");
    fchReglemEdit.VCLObject("ApiPanel2").VCLObject("saiLibelle").Keys("TEST[Tab]");
    var<TWinTST> stdScroll = fchReglemEdit.VCLObject("ApiPanel3").VCLObject("ssReglem");
    stdScroll.Window("TScrollSaisieEdit", "100,00").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Comptant").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Chhque").Keys("[Tab]");
    var<TWinTST> stdPanel = fchReglemEdit.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(36, 6);
    stdPanel.VCLObject("btQuitter").Click(38, 15);
    apiPanel.Window("TStdBout", "", 6).Click(16, 10);
    tfmMenu.Keys("~i");
    tfmMenu.MainMenu.Click("[1]|[11]|[6]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "Conditions de r�glement : Code et Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(57, 52);
    fchReglemEdit = stdScrollBox.Window("TFchReglemEdit", "Conditions de r�glement");
    stdScroll = fchReglemEdit.VCLObject("ApiPanel3").VCLObject("ssReglem");
    stdScroll.Click(40, 22);
    stdScroll.Window("TScrollSaisieEdit", "100,00").Keys("50[Tab]");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Comptant");
    scrollSaisieEdit.Click(113, 7);
    scrollSaisieEdit.Click(113, 7);
    stdScroll.Click(448, 28);
    stdScroll.Click(416, 27);
    stdScroll.Click(317, 24);
    stdScroll.Window("TScrollSaisieEdit", "Chhque").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "50,00").Keys("[Tab]");
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Comptant");
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll.Click(389, 33);
    stdScroll.Click(394, 23);
    stdScroll.Click(323, 24);
    stdScroll.Click(152, 29);
    scrollSaisieEdit.Click(108, 11);
    mde.Window("TCbxPopupList").Click(77, 28);
    stdScroll.Click(307, 27);
    stdScroll.Window("TScrollSaisieEdit", "1").Keys("10[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Chhque", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "50,00", 8).Keys("[Tab]");
    scrollSaisieEdit.Click(111, 10);
    mde.Window("TCbxPopupList").Click(68, 25);
    stdScroll.Click(305, 43);
    stdScroll.Window("TScrollSaisieEdit", "1").Keys("30[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Chhque", 1).Keys("[Tab]");
    fchReglemEdit.VCLObject("StdPanel1").VCLObject("btValider").Click(15, 8);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 6).Click(18, 16);
    tfmMenu.Keys("~i");
    tfmMenu.DblClick(73, 37);
    tfmMenu.MainMenu.Click("[1]|[11]|[6]");
    panel = stdScrollBox.Window("TStdFormListe", "Conditions de r�glement : Code et Libell�").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.DblClick(85, 51);
    fchReglemEdit = stdScrollBox.Window("TFchReglemEdit", "Conditions de r�glement");
    var<TWinTST> stdSais = fchReglemEdit.VCLObject("ApiPanel2").VCLObject("saiLibelle");
    stdSais.Click(91, 2);
    stdSais.Keys(" modif");
    fchReglemEdit.VCLObject("StdPanel1").VCLObject("btValider").Click(45, 27);
    stdScroll.Click(97, 35);
    stdScroll.Click(92, 54);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 3).Click(14, 15);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(48, 14);
    apiPanel.Window("TStdBout", "", 6).Click(10, 9);
    tfmMenu.Keys("~i");
    tfmMenu.Click(69, 40);
}

void T_Initialisation_0040() {  //Ajout d'un logo
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmRensGx = stdScrollBox.Window("TfmRensGx", "Renseignements g�n�raux ");
    var<TWinTST> stdScrollBox2 = wndTfmRensGx.VCLObject("pcRenseignementsGeneraux").VCLObject("tsSociete").VCLObject("panSocieteXp").VCLObject("sbImage");
    stdScrollBox2.Click(51, 43);
    stdScrollBox2.ClickR(51, 43);
    stdScrollBox2.PopupMenu.Click("Modifier");
    mde.Window("#32770", "Ouvrir").OpenFile("C:\\Users\\Public\\Pictures\\Sample Pictures\\Chrysanthemum.jpg", "Tout (*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)");
    wndTfmRensGx.VCLObject("StdPanel1").VCLObject("btValider").Click(38, 17);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(281, 324);
}

void T_Initialisation_0041() {  //Modifications natures d'�l�ments
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmNatureElement = stdScrollBox.Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> stdScroll = wndTfmNatureElement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Window("TScrollSaisieEdit", "MX").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Mat�riaux").Keys("[Right] modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,200000").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,400000").Keys("1.3[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,680000").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Click(188, 69);
    var<TWinTST> stdPanel = wndTfmNatureElement.VCLObject("StdPanel1");
    stdPanel.VCLObject("btHaut").Click(22, 9);
    stdPanel.VCLObject("btBas").Click(15, 16);
    stdScroll.Click(234, 97);
    stdPanel.VCLObject("btValider").Click(60, 15);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(433, 403);
}

void T_Initialisation_0042() {  //M�tr�s types
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[5]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "M�tr�s-types : Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(13, 14);
    var<TWinTST> tfmMetreType = stdScrollBox.Window("TfmMetreType", "M�tr�-type [Cr�ation]");
    tfmMetreType.VCLObject("panInfos").VCLObject("saiLibelle").Keys("test");
    var<TWinTST> stdScroll = tfmMetreType.VCLObject("panGeneral").VCLObject("panMetre").VCLObject("scrMetre");
    stdScroll.Click(39, 22);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "F");
    scrollSaisieEdit.Click(34, 8);
    mde.Window("TCbxPopupList").Click(14, 5);
    stdScroll.Click(85, 25);
    stdScroll.Window("TScrollSaisieEdit", "", 24).Keys("test");
    stdScroll.Click(62, 45);
    var<TWinTST> scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "", 24);
    scrollSaisieEdit2.Keys("a=5[Down]");
    scrollSaisieEdit.Keys("[Tab]");
    scrollSaisieEdit2.Keys("b=6[Down]");
    scrollSaisieEdit.Keys("[Tab]");
    scrollSaisieEdit2.Keys("a=[BS][BS]c=a[NumAsterisk]b[Enter]");
    stdScroll.Click(86, 92);
    var<TWinTST> stdPanel = tfmMetreType.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(39, 13);
    stdPanel.VCLObject("btQuitter").Click(47, 12);
    apiPanel.Window("TStdBout", "", 6).Click(12, 3);
    tfmMenu.Keys("~i");
    tfmMenu.DblClick(73, 38);
    tfmMenu.MainMenu.Click("[1]|[2]|[5]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "M�tr�s-types : Libell�").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.DblClick(16, 52);
    tfmMetreType = stdScrollBox.Window("TfmMetreType", "M�tr�-type");
    var<TWinTST> stdSais = tfmMetreType.VCLObject("panInfos").VCLObject("saiLibelle");
    stdSais.DblClick(71, 5);
    stdSais.Keys("[Tab]");
    stdSais.Click(71, 5);
    stdSais.Keys(" modification");
    stdSais.Click(71, 5);
    stdSais.Keys(" ");
    var<TWinTST> stdScroll2 = tfmMetreType.VCLObject("panGeneral").VCLObject("panMetre").VCLObject("scrMetre");
    stdScroll2.Click(90, 87);
    stdScroll2.Click(38, 99);
    stdScroll2.Window("TScrollSaisieEdit", "F").Keys("i");
    stdScroll2.Click(71, 99);
    stdScroll2.Window("TScrollSaisieEdit", "", 3).Keys("modification[Tab]");
    stdSais.Click(54, 13);
    stdSais.Keys(" modif[Tab]");
    tfmMetreType.VCLObject("StdPanel1").VCLObject("btValider").Click(57, 17);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(17, 6);
    tfmMetreType = stdScrollBox.Window("TfmMetreType", "M�tr�-type [Cr�ation]");
    tfmMetreType.VCLObject("panInfos").VCLObject("saiLibelle").Keys("essai[Tab]");
    stdScroll2 = tfmMetreType.VCLObject("panGeneral").VCLObject("panMetre").VCLObject("scrMetre");
    stdScroll2.Window("TScrollSaisieEdit", "F").Keys("i");
    stdScroll2.Window("TScrollSaisieEdit", "I").Keys("[Tab]");
    stdScroll2.Window("TScrollSaisieEdit", "", 24).Keys("esszi[BS][BS]ai[Tab]");
    stdPanel = tfmMetreType.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(31, 10);
    stdPanel.VCLObject("btQuitter").Click(60, 18);
    stdScroll.Click(34, 71);
    stdScroll.Click(34, 80);
    apiPanel.Window("TStdBout", "", 3).Click(19, 5);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(27, 18);
    apiPanel.Window("TStdBout", "", 6).Click(17, 10);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(229, 422);
}

void T_Initialisation_0043() {  //Suppression Natures d'�l�ments
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmNatureElement = stdScrollBox.Window("TfmNatureElement", "Natures d'�l�ments");
    var<TWinTST> stdScroll = wndTfmNatureElement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Window("TScrollSaisieEdit", "MX").Keys("[Down][Down][Down]");
    stdScroll.Window("TScrollSaisieEdit").Keys("t[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("test[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 4).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "1,000000", 2).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmNatureElement.VCLObject("StdPanel1").VCLObject("btValider").Click(59, 10);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(206, 368);
    tfmMenu.Click(136, 28);
    tfmMenu.MainMenu.Click("[1]|[2]|[2]");
    wndTfmNatureElement = stdScrollBox.Window("TfmNatureElement", "Natures d'�l�ments");
    wndTfmNatureElement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Window("TScrollSaisieEdit", "MX").Keys("[Down][Down][Down]");
    var<TWinTST> stdPanel = wndTfmNatureElement.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(9, 9);
    stdPanel.VCLObject("btValider").Click(56, 0);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(204, 358);
}

void T_Initialisation_0044() {  //Etats d'affaires
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[8]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmEtatAffaire = stdScrollBox.Window("TfmEtatAffaire", "Etats des affaires");
    var<TWinTST> stdScroll = wndTfmEtatAffaire.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(81, 96);
    stdScroll.Click(86, 108);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Click(35, 12);
    stdScroll.Click(137, 124);
    scrollSaisieEdit.Keys("test2[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("test3[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    wndTfmEtatAffaire.VCLObject("StdPanel1").VCLObject("btValider").Click(23, 25);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(359, 461);
    tfmMenu.MainMenu.Click("[1]|[8]|[0]");
    wndTfmEtatAffaire = stdScrollBox.Window("TfmEtatAffaire", "Etats des affaires");
    stdScroll = wndTfmEtatAffaire.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(142, 152);
    stdScroll.Click(132, 126);
    stdScroll.Window("TScrollSaisieEdit", "Nigociation").Keys("[End][Del] modif[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Click(28, 10);
    stdScroll.Click(381, 133);
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "", 3);
    scrollCoche.Click(52, 15);
    scrollCoche.Click(49, 14);
    stdScroll.Click(246, 155);
    wndTfmEtatAffaire.VCLObject("StdPanel1").VCLObject("btValider").Click(39, 9);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(376, 426);
    tfmMenu.MainMenu.Click("[1]|[8]|[0]");
    wndTfmEtatAffaire = stdScrollBox.Window("TfmEtatAffaire", "Etats des affaires");
    stdScroll = wndTfmEtatAffaire.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(101, 146);
    var<TWinTST> stdPanel = wndTfmEtatAffaire.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(14, 13);
    stdScroll.Click(98, 125);
    var<TWinTST> stdBoutImage = stdPanel.VCLObject("btHaut");
    stdBoutImage.DblClick(14, 13);
    stdBoutImage.Click(14, 13);
    stdScroll.Click(110, 49);
    stdScroll.Click(147, 77);
    stdPanel.VCLObject("btBas").DblClick(13, 12);
    stdScroll.Click(162, 126);
    stdScroll.Click(132, 146);
    stdPanel.VCLObject("btValider").Click(51, 16);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(386, 428);
}

void T_Initialisation_0045() {  //Types de marchis
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[9]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmTypeMarche = stdScrollBox.Window("TfmTypeMarche", "Types de marchis");
    var<TWinTST> stdScroll = wndTfmTypeMarche.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(92, 95);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test1[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("test1[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("test2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("test2[Tab]");
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "", 1);
    scrollCoche.Click(18, 10);
    scrollCoche.Keys("[Tab]");
    scrollSaisieEdit.Keys("test3[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmTypeMarche.VCLObject("StdPanel1").VCLObject("btValider").Click(6, 18);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(355, 460);
    tfmMenu.MainMenu.Click("[1]|[9]");
    wndTfmTypeMarche = stdScrollBox.Window("TfmTypeMarche", "Types de marchis");
    stdScroll = wndTfmTypeMarche.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(106, 126);
    stdScroll.Click(103, 105);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Appels d'offre");
    scrollSaisieEdit.Click(56, 10);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    wndTfmTypeMarche.VCLObject("StdPanel1").VCLObject("btValider").Click(36, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(397, 457);
    tfmMenu.MainMenu.Click("[1]|[9]");
    wndTfmTypeMarche = stdScrollBox.Window("TfmTypeMarche", "Types de marchis");
    wndTfmTypeMarche.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Click(120, 129);
    var<TWinTST> stdPanel = wndTfmTypeMarche.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(13, 11);
    stdPanel.VCLObject("btValider").Click(57, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(418, 457);
}

void T_Initialisation_0046() {  //Modes de r�glements
    Sys.Process("explorer").Window("Shell_TrayWnd").Window("ReBarWindow32").Window("MSTaskSwWClass", "Applications en cours d'ex�cution").Window("MSTaskListWClass", "Applications en cours d'ex�cution").Click(230, 22);
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[11]|[5]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmModePaiement = stdScrollBox.Window("TfmModePaiement", "Modes de r�glement");
    var<TWinTST> stdScroll = wndTfmModePaiement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Window("TScrollSaisieEdit", "Chhque").Keys("[Down][Down][Down][Down][Down][Down][Down][Down]");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("e[BS]essai[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("essai[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("toto[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("toto[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmModePaiement.VCLObject("StdPanel1").VCLObject("btValider").Click(31, 23);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(337, 374);
    tfmMenu.MainMenu.Click("[1]|[11]|[5]");
    wndTfmModePaiement = stdScrollBox.Window("TfmModePaiement", "Modes de r�glement");
    stdScroll = wndTfmModePaiement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Chhque");
    scrollSaisieEdit.Keys("[Down][Down][Down][Down][Down][Down][Down][Down][Down][Down]");
    scrollSaisieEdit.Click(87, 5);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Mode de r�glement Chhque").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmModePaiement.VCLObject("StdPanel1").VCLObject("btValider").Click(41, 20);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(362, 441);
    tfmMenu.MainMenu.Click("[1]|[11]|[5]");
    wndTfmModePaiement = stdScrollBox.Window("TfmModePaiement", "Modes de r�glement");
    wndTfmModePaiement.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Window("TScrollSaisieEdit", "Chhque").Keys("[Down][Down][Down][Down][Down][Down][Down][Down][Down][Down]");
    var<TWinTST> stdPanel = wndTfmModePaiement.VCLObject("StdPanel1");
    stdPanel.Click(690, 19);
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(17, 14);
    stdPanel.VCLObject("btValider").Click(38, 18);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(344, 369);
}

void T_Initialisation_0047() {  //Retenues
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[11]|[7]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndRetenues = stdScrollBox.Window("TfmRetenue", "Retenues");
    var<TWinTST> stdScroll = wndRetenues.VCLObject("pnScroll").VCLObject("pnScrollCenter").VCLObject("scRetenue");
    stdScroll.Click(42, 39);
    stdScroll.Click(25, 46);
    stdScroll.Window("TScrollSaisieEdit", "Retenue de garantie sur fin de travaux").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "5,00").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Montant TTC").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sur totalit�").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "A �ch�ance").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sur premi�res factures").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "12").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "4117").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit").Keys("RG2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 3).Keys("REtenue2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 3).Keys("10[Tab]");
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "", 2);
    scrollCoche.Click(18, 9);
    scrollCoche.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Montant TTC").Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Selon travaux");
    scrollSaisieEdit.Click(91, 5);
    mde.Window("TCbxPopupList").Click(77, 23);
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "A �ch�ance").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sur premi�res factures").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "12").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "4117").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    wndRetenues.VCLObject("StdPanel1").VCLObject("btValider").Click(49, 13);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(213, 366);
    tfmMenu.MainMenu.Click("[1]|[11]|[7]");
    wndRetenues = stdScrollBox.Window("TfmRetenue", "Retenues");
    stdScroll = wndRetenues.VCLObject("pnScroll").VCLObject("pnScrollCenter").VCLObject("scRetenue");
    stdScroll.Click(101, 33);
    stdScroll.Click(130, 45);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Retenue de garantie sur fin de travaux");
    scrollSaisieEdit.Click(73, 12);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "10,00").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Montant TTC").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sur totalit�").Click(93, 7);
    mde.Window("TCbxPopupList").Click(70, 4);
    stdScroll.Click(480, 48);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "A �ch�ance");
    scrollSaisieEdit.Click(89, 11);
    mde.Window("TCbxPopupList").Click(83, 19);
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sur premi�res factures").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "12").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "4117").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit").Keys("[Tab][Tab]");
    wndRetenues.VCLObject("StdPanel1").VCLObject("btValider").Click(37, 13);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(284, 407);
    tfmMenu.MainMenu.Click("[1]|[11]|[7]");
    wndRetenues = stdScrollBox.Window("TfmRetenue", "Retenues");
    stdScroll = wndRetenues.VCLObject("pnScroll").VCLObject("pnScrollCenter").VCLObject("scRetenue");
    stdScroll.Click(112, 44);
    var<TWinTST> stdPanel = wndRetenues.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(12, 16);
    stdScroll.Window("TScrollSaisieEdit", "Retenue de garantie sur fin de travaux").Keys("[Tab]");
    stdPanel.VCLObject("btValider").Click(55, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(341, 488);
}

void T_Initialisation_0048() {  //Etats des chantiers
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[14]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmEtatChantier = stdScrollBox.Window("TfmEtatChantier", "Etats des chantiers");
    var<TWinTST> stdScroll = wndTfmEtatChantier.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(79, 60);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test");
    stdScroll.Click(221, 56);
    stdScroll.Window("TScrollCoche", "", 5).Click(40, 9);
    stdScroll.Click(112, 75);
    scrollSaisieEdit.Keys("test2[Tab]");
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "", 5);
    scrollCoche.Click(39, 10);
    scrollCoche.Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmEtatChantier.VCLObject("StdPanel1").VCLObject("btValider").Click(26, 9);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(254, 401);
    tfmMenu.MainMenu.Click("[1]|[14]|[0]");
    wndTfmEtatChantier = stdScrollBox.Window("TfmEtatChantier", "Etats des chantiers");
    stdScroll = wndTfmEtatChantier.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(111, 61);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Ouvert");
    scrollSaisieEdit.Click(72, 11);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollCoche", "", 5).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmEtatChantier.VCLObject("StdPanel1").VCLObject("btValider").Click(32, 7);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(347, 457);
    tfmMenu.MainMenu.Click("[1]|[14]|[0]");
    wndTfmEtatChantier = stdScrollBox.Window("TfmEtatChantier", "Etats des chantiers");
    wndTfmEtatChantier.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Click(92, 75);
    var<TWinTST> stdPanel = wndTfmEtatChantier.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage").Click(11, 14);
    stdPanel.VCLObject("btValider").Click(41, 17);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(356, 471);
}

void T_Initialisation_0049() {  //Natures de travaux
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[14]|[2]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmNatureTravaux = stdScrollBox.Window("TfmNatureTravaux", "Natures de travaux");
    var<TWinTST> stdScroll = wndTfmNatureTravaux.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(84, 80);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("test2[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmNatureTravaux.VCLObject("StdPanel1").VCLObject("btValider").Click(41, 13);
    mde.Window("TApiMessageDlg", "Natures de travaux").Window("TApiPanel").Window("TStdBout").Click(34, 7);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(416, 398);
    tfmMenu.MainMenu.Click("[1]|[14]|[2]");
    wndTfmNatureTravaux = stdScrollBox.Window("TfmNatureTravaux", "Natures de travaux");
    stdScroll = wndTfmNatureTravaux.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(129, 94);
    stdScroll.Click(129, 80);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Construction");
    scrollSaisieEdit.Click(73, 10);
    scrollSaisieEdit.Keys(" modif[Enter]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Enter]");
    wndTfmNatureTravaux.VCLObject("StdPanel1").VCLObject("btValider").Click(53, 17);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(410, 437);
    tfmMenu.Click(64, 34);
    tfmMenu.MainMenu.Click("[1]|[14]|[2]");
    wndTfmNatureTravaux = stdScrollBox.Window("TfmNatureTravaux", "Natures de travaux");
    wndTfmNatureTravaux.VCLObject("panGeneral").VCLObject("panList").VCLObject("scInitList").Click(94, 90);
    var<TWinTST> stdPanel = wndTfmNatureTravaux.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(22, 16);
    stdPanel.VCLObject("btValider").Click(46, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(402, 458);
}

void T_Initialisation_0050() {  //Heures de travail
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[14]|[5]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmDetailHeure = stdScrollBox.Window("TfmDetailHeure", "Heures de travail");
    var<TWinTST> apiPanel = wndTfmDetailHeure.VCLObject("pnGenreral").VCLObject("pnGenCenter");
    var<TWinTST> stdPageControl = apiPanel.VCLObject("pcHeures");
    var<TWinTST> stdScroll = stdPageControl.VCLObject("tsHeureTrav").VCLObject("pnHeureTrav").VCLObject("scHeureTrav");
    stdScroll.Window("TScrollHeader").Click(109, 11);
    stdScroll.Keys("[Down]");
    stdScroll.Window("TScrollSaisieEdit", "125,00").Keys("[Down][Down][Down][Down]");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("HTEST[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("Heures test[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "100,00").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 5).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    scrollSaisieEdit.Keys("HESSAI[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("Heures essai[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "100,00").Keys("110[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    apiPanel.Click(220, 11);
    stdPageControl.Click(210, 14);
    stdScroll = stdPageControl.VCLObject("tsHeureSup").VCLObject("pnHeureSup").VCLObject("scHeureSup");
    stdScroll.Window("TScrollSaisieEdit", "125,00").Keys("[Down][Down][Down][Down]");
    stdScroll.Window("TScrollSaisieEdit").Keys("[Up]");
    stdScroll.Window("TScrollSaisieEdit", "200,00").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "0,000").Keys("![ReleaseLast]45[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("220[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "0,000", 1).Keys("4-6[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("[Tab]");
    wndTfmDetailHeure.VCLObject("StdPanel1").VCLObject("btValider").Click(28, 17);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(384, 530);
    tfmMenu.MainMenu.Click("[1]|[14]|[5]");
    wndTfmDetailHeure = stdScrollBox.Window("TfmDetailHeure", "Heures de travail");
    stdScroll = wndTfmDetailHeure.VCLObject("pnGenreral").VCLObject("pnGenCenter").VCLObject("pcHeures").VCLObject("tsHeureTrav").VCLObject("pnHeureTrav").VCLObject("scHeureTrav");
    stdScroll.Click(163, 137);
    stdScroll.Click(211, 118);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Heures test");
    scrollSaisieEdit.Click(137, 9);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "100,00").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 5).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    var<TWinTST> stdPanel = wndTfmDetailHeure.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(11, 14);
    stdPanel.VCLObject("btValider").Click(41, 8);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(424, 142);
}

void T_Initialisation_0051() {  //Rubriques salari�s
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[14]|[6]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmRubrSalarie = stdScrollBox.Window("TfmRubrSalarie", "Rubriques salari�s");
    var<TWinTST> stdScroll = wndTfmRubrSalarie.VCLObject("pnGeneral").VCLObject("scRubrique");
    stdScroll.Click(44, 156);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("TEST");
    stdScroll.Click(75, 160);
    stdScroll.Window("TScrollSaisieEdit", "", 3).Keys("TEST2");
    stdScroll.Click(313, 160);
    stdScroll.Click(341, 159);
    stdScroll.Click(379, 164);
    var<TWinTST> scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "", 4);
    scrollSaisieEdit2.Keys("10");
    scrollSaisieEdit2.MouseWheel(-1);
    scrollSaisieEdit.Keys("TEST3[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 3).Keys("TEST3");
    stdScroll.Click(273, 162);
    stdScroll.Window("TScrollSaisieEdit", "Quantifiable").Click(67, 10);
    mde.Window("TCbxPopupList").Click(60, 21);
    stdScroll.Click(344, 162);
    stdScroll.Click(379, 156);
    scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "", 4);
    scrollSaisieEdit2.Keys("50");
    scrollSaisieEdit2.MouseWheel(-1);
    scrollSaisieEdit.MouseWheel(-1);
    stdScroll.Click(138, 157);
    wndTfmRubrSalarie.VCLObject("StdPanel1").VCLObject("btValider").Click(50, 11);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(365, 444);
    tfmMenu.MainMenu.Click("[1]|[14]|[6]");
    wndTfmRubrSalarie = stdScrollBox.Window("TfmRubrSalarie", "Rubriques salari�s");
    stdScroll = wndTfmRubrSalarie.VCLObject("pnGeneral").VCLObject("scRubrique");
    stdScroll.Click(121, 164);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Panier repas");
    scrollSaisieEdit.Click(56, 10);
    scrollSaisieEdit.Keys("[BS] modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Quantifiable").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "10,00").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "H").Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 2).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    wndTfmRubrSalarie.VCLObject("StdPanel1").VCLObject("btValider").Click(37, 16);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(372, 501);
    tfmMenu.MainMenu.Click("[1]|[14]|[6]");
    wndTfmRubrSalarie = stdScrollBox.Window("TfmRubrSalarie", "Rubriques salari�s");
    stdScroll = wndTfmRubrSalarie.VCLObject("pnGeneral").VCLObject("scRubrique");
    stdScroll.Click(149, 162);
    stdScroll.Window("TScrollSaisieEdit", "Panier repas").MouseWheel(-1);
    var<TWinTST> stdPanel = wndTfmRubrSalarie.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(7, 18);
    stdPanel.VCLObject("btValider").Click(31, 13);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(382, 466);
}

void T_Initialisation_0052() {  //Param�trage Comptabiliti
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmCmpParamGen = stdScrollBox.Window("TfmCmpParamGen", "Param�trage comptable g�n�ral ");
    wndTfmCmpParamGen.VCLObject("panLiaison").VCLObject("saiPathFic").Click(59, 5);
    var<TWinTST> stdPageControl = wndTfmCmpParamGen.VCLObject("panParam").VCLObject("pgParamGen");
    var<TWinTST> apiPanel = stdPageControl.VCLObject("tsGeneral").VCLObject("panGeneral");
    apiPanel.Click(96, 108);
    var<TWinTST> stdSais = apiPanel.VCLObject("saiRacTiersCli");
    stdSais.Click(13, 6);
    stdSais.Keys("CLi[Tab]");
    apiPanel.VCLObject("saiRacTiersFrs").Keys("FOUR[Tab]");
    apiPanel.VCLObject("saiRacTiersSsr").Keys("ST[Tab]");
    stdPageControl.Click(98, 9);
    stdSais = stdPageControl.VCLObject("tsComptes").VCLObject("panComptes").VCLObject("saiCptColCli");
    stdSais.Click(108, 9);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(102, 37);
    stdSais.Click(105, 8);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(85, 24);
    wndTfmCmpParamGen.VCLObject("StdPanel1").VCLObject("btValider").Click(30, 19);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(395, 522);
}

void T_Initialisation_0053() {  //Plan comptable g�n�ral
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[1]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 4).Click(18, 14);
    var<TWinTST> tfchComPlanCP = stdScrollBox.Window("TfchComPlanCP", "Comptes [Cr�ation]");
    var<TWinTST> stdSais = tfchComPlanCP.VCLObject("saiCode");
    stdSais.Keys("999[Tab]");
    var<TWinTST> stdSais2 = tfchComPlanCP.VCLObject("saiLibelle");
    stdSais2.Keys("test[Tab]");
    var<TWinTST> stdPanel = tfchComPlanCP.VCLObject("StdPanel1");
    var<TWinTST> stdBout = stdPanel.VCLObject("btValider");
    stdBout.Click(32, 11);
    stdSais.Keys("9998[Tab]");
    stdSais2.Keys("test2[Tab]");
    stdBout.Click(39, 15);
    stdPanel.VCLObject("btQuitter").Click(33, 14);
    apiPanel.Window("TStdBout", "", 5).Click(7, 12);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(222, 459);
    tfmMenu.MainMenu.Click("[1]|[17]|[1]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll");
    var<TWinTST> stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.Window("TScrollBar").wPosition = 168;
    stdScroll.DblClick(68, 290);
    tfchComPlanCP = stdScrollBox.Window("TfchComPlanCP", "Comptes");
    tfchComPlanCP.Click(115, 57);
    stdSais = tfchComPlanCP.VCLObject("saiLibelle");
    stdSais.Click(53, 8);
    stdSais.Keys(" modif");
    tfchComPlanCP.VCLObject("StdPanel1").VCLObject("btValider").Click(44, 22);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 5).Click(8, 15);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(224, 462);
    tfmMenu.MainMenu.Click("[1]|[17]|[1]");
    panel = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.Window("TScrollBar").wPosition = 168;
    stdScroll.Click(99, 294);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 2).Click(13, 10);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(32, 18);
    apiPanel.Window("TStdBout", "", 5).Click(13, 13);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(228, 484);
}

void T_Initialisation_0054() {  //Plans analytiques
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "Plans analytiques : Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 3).Click(11, 11);
    var<TWinTST> tfchAna = stdScrollBox.Window("TfchAna", "Plan analytique [Cr�ation]");
    var<TWinTST> apiPanel2 = tfchAna.VCLObject("panAxe");
    var<TWinTST> stdSais = apiPanel2.VCLObject("saiLibelle");
    stdSais.Keys("PLan test[Tab]");
    var<TWinTST> stdSais2 = apiPanel2.VCLObject("saiNumPlan");
    stdSais2.Keys("3[Tab]");
    var<TWinTST> stdCoche = apiPanel2.VCLObject("cocheObl");
    stdCoche.Keys("[Tab]");
    var<TWinTST> stdScroll = tfchAna.VCLObject("panScrollPosteAna").VCLObject("scrPoste");
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("999[Tab]");
    var<TWinTST> scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "", 1);
    scrollSaisieEdit2.Keys("attente[Tab]");
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "N15.5", 1);
    scrollCoche.Click(35, 1);
    scrollCoche.Click(32, 12);
    scrollCoche.Keys("[Tab]");
    var<TWinTST> stdPanel = tfchAna.VCLObject("StdPanel1");
    var<TWinTST> stdBout = stdPanel.VCLObject("btValider");
    stdBout.Click(46, 13);
    stdSais.Keys("Plan test2[Tab]");
    stdSais2.Keys("4[Tab]");
    stdCoche.Keys("[Tab]");
    stdBout.Click(48, 11);
    mde.Window("TApiMessageDlg", "Plan analytique [Cr�ation]").Window("TApiPanel").Window("TStdBout").Click(25, 15);
    scrollSaisieEdit.Keys("999[Tab]");
    scrollSaisieEdit2.Keys("atte n[BS][BS]nte[Tab]");
    scrollCoche.Click(36, 9);
    scrollCoche.Keys("[Tab]");
    scrollSaisieEdit.Keys("[Tab]");
    stdBout.Click(44, 10);
    stdPanel.VCLObject("btQuitter").Click(41, 12);
    apiPanel.Window("TStdBout", "", 4).Click(16, 12);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(265, 387);
    tfmMenu.MainMenu.Click("[1]|[17]|[3]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "Plans analytiques : Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(50, 53);
    tfchAna = stdScrollBox.Window("TfchAna", "Plan analytique [Modification]");
    stdSais = tfchAna.VCLObject("panAxe").VCLObject("saiLibelle");
    stdSais.Click(75, 8);
    stdSais.Keys(" modif[Tab]");
    tfchAna.VCLObject("StdPanel1").VCLObject("btValider").Click(44, 15);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 4).Click(5, 7);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(255, 406);
    tfmMenu.MainMenu.Click("[1]|[17]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "Plans analytiques : Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(71, 65);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 1).Click(9, 14);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(21, 9);
    apiPanel.Window("TStdBout", "", 4).Click(16, 11);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(266, 412);
}

void T_Initialisation_0055() {  //Devises
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[18]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndFRDevises = stdScrollBox.Window("TFRDevises", "Mise � jour des devises");
    var<TWinTST> stdScroll = wndFRDevises.VCLObject("ApiPanelSousScroll").VCLObject("scrl_FrmRes");
    stdScroll.Click(25, 74);
    stdScroll.Window("TScrollSaisieEdit").Keys("FB");
    stdScroll.Click(60, 72);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "", 4);
    scrollSaisieEdit.Click(57, 14);
    stdScrollBox.Window("TStdFormListe", "Devises ISO : Code et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(139, 280);
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Franc belge").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 4).Keys("FB[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 4).Keys("1[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "0").Keys("[Tab]");
    wndFRDevises.VCLObject("StdPanel1").VCLObject("btQuitter").Click(17, 12);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(191, 341);
    tfmMenu.MainMenu.Click("[1]|[18]");
    wndFRDevises = stdScrollBox.Window("TFRDevises", "Mise � jour des devises");
    stdScroll = wndFRDevises.VCLObject("ApiPanelSousScroll").VCLObject("scrl_FrmRes");
    stdScroll.Click(153, 78);
    stdScroll.Click(207, 61);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Franc fran�ais");
    scrollSaisieEdit.Click(98, 11);
    scrollSaisieEdit.Keys(" modif[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "FB").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "6,559570").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "0").Keys("[Tab]");
    wndFRDevises.VCLObject("StdPanel1").VCLObject("btQuitter").Click(49, 6);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(237, 362);
    tfmMenu.MainMenu.Click("[1]|[18]");
    wndFRDevises = stdScrollBox.Window("TFRDevises", "Mise � jour des devises");
    wndFRDevises.VCLObject("ApiPanelSousScroll").VCLObject("scrl_FrmRes").Click(173, 58);
    var<TWinTST> stdPanel = wndFRDevises.VCLObject("StdPanel1");
    stdPanel.Window("TPanel").Window("TStdBoutImage", "", 1).Click(19, 14);
    mde.Window("TApiMessageDlg", "Mise � jour des devises").Window("TApiPanel").Window("TStdBout", "", 2).Click(28, 11);
    stdPanel.VCLObject("btQuitter").Click(36, 11);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(243, 395);
}

void T_Initialisation_0056() {  //Import du PCG SAGE 100
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[2]");
    mde.Window("TApiMessageDlg", "Import du PCG de SAGE100 Comptabiliti").Window("TApiPanel").Window("TStdBout", "", 2).Click(39, 13);
    mde.Window("TApiMessageDlg", "Sage 100 Multi Devis Entreprise").Window("TApiPanel").Window("TStdBout").Click(19, 13);
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    stdScrollBox.Click(678, 350);
    tfmMenu.MainMenu.Click("[1]|[17]|[1]");
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 5).Click(13, 11);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(393, 567);
}

void T_Initialisation_0057() {  //Param�trage paie
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[14]|[8]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> tfmPaieParam = stdScrollBox.Window("TfmPaieParam", "Param�trage de la paie");
    var<TWinTST> stdScroll = tfmPaieParam.VCLObject("panScroll").VCLObject("scCorresp");
    stdScroll.Click(456, 25);
    stdScroll.Window("TScrollSaisieEdit").Keys("0011[Tab][Tab][Tab][Tab]1250[Tab]1260[Tab][Tab]");
    tfmPaieParam.VCLObject("panAffect").VCLObject("cbAffect").Click(5, 10);
    tfmPaieParam.VCLObject("StdPanel1").VCLObject("btValider").Click(39, 19);
    mde.Window("TApiMessageDlg", "Param�trage de la paie").Window("TApiPanel").Window("TStdBout", "", 2).Click(24, 16);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(631, 388);
}

void T_Initialisation_0058() {  //Postes de pied
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[11]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmPostePied = stdScrollBox.Window("TfmPostePied", "Postes de pied");
    var<TWinTST> stdScroll = wndTfmPostePied.VCLObject("pnGeneral").VCLObject("pnScroll").VCLObject("scPoste");
    stdScroll.Click(100, 43);
    var<TWinTST> stdPanel = wndTfmPostePied.VCLObject("StdPanel1");
    var<TWinTST> panel = stdPanel.Window("TPanel");
    var<TWinTST> stdBoutImage = panel.Window("TStdBoutImage", "", 2);
    stdBoutImage.Click(17, 10);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Poste");
    scrollSaisieEdit.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "Remise");
    scrollSaisieEdit2.Keys("Remise1[Tab]");
    var<TWinTST> scrollSaisieEdit3 = stdScroll.Window("TScrollSaisieEdit", "Montant et %");
    scrollSaisieEdit3.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit4 = stdScroll.Window("TScrollSaisieEdit", "", 5);
    scrollSaisieEdit4.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit5 = stdScroll.Window("TScrollSaisieEdit", "", 4);
    scrollSaisieEdit5.Keys("10[Tab]");
    var<TWinTST> scrollSaisieEdit6 = stdScroll.Window("TScrollSaisieEdit", "", 3);
    scrollSaisieEdit6.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit7 = stdScroll.Window("TScrollSaisieEdit", "70850000");
    scrollSaisieEdit7.Keys("[Tab]");
    var<TWinTST> stdFormListe = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�");
    stdFormListe.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("708500[Enter]");
    stdFormListe.VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").Keys("[Enter]");
    var<TWinTST> scrollCoche = stdScroll.Window("TScrollCoche", "", 5);
    scrollCoche.Keys("[Tab]");
    var<TWinTST> scrollSaisieEdit8 = stdScroll.Window("TScrollSaisieEdit", "", 1);
    scrollSaisieEdit8.Keys("[Tab]");
    scrollSaisieEdit.Keys("[Tab]");
    scrollSaisieEdit2.Keys("[Tab]");
    scrollSaisieEdit3.Keys("[Tab]");
    scrollSaisieEdit4.Keys("[Tab]");
    scrollSaisieEdit5.Keys("[Tab]");
    scrollSaisieEdit6.Keys("[Tab]");
    scrollSaisieEdit7.Keys("[Tab]");
    scrollCoche.Keys("[Tab]");
    scrollSaisieEdit8.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Sous-total").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Total HT Net Commercial").Keys("[Down]");
    stdBoutImage.Click(1, 11);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Poste");
    scrollSaisieEdit.Keys("[Tab]");
    scrollSaisieEdit2 = stdScroll.Window("TScrollSaisieEdit", "Escompte");
    scrollSaisieEdit2.Keys("compte[BS][BS][BS][BS][BS][BS]acompte[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "Montant et %").Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 5).Keys("100[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "70850000").Keys("[Tab]");
    stdFormListe = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�");
    stdFormListe.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("7085[Enter]");
    stdFormListe.VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").Keys("[Enter]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("[Tab]");
    scrollSaisieEdit2.Keys("[Tab]");
    stdScroll.Click(187, 93);
    scrollSaisieEdit2.Keys("[End] modif[Tab]");
    stdScroll.Click(134, 46);
    panel.Window("TStdBoutImage", "", 1).Click(18, 7);
    stdPanel.VCLObject("btValider").Click(40, 26);
    tfmMenu.Keys("~i");
    stdScrollBox.Drag(290, 471, 12, -10);
}

void T_Initialisation_0059() {  //Etats des documents
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[11]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmEtatDocument = stdScrollBox.Window("TfmEtatDocument", "Etats des documents");
    var<TWinTST> apiPanel = wndTfmEtatDocument.VCLObject("panGeneral");
    var<TWinTST> stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(86, 86);
    stdScroll.Window("TScrollSaisieEdit").Keys("test");
    stdScroll.Click(248, 100);
    stdScroll.Window("TScrollCoche", "", 6).Click(28, 11);
    stdScroll.Click(102, 107);
    var<TWinTST> stdScroll2 = apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList");
    stdScroll2.Click(82, 37);
    stdScroll.Click(70, 90);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(106, 53);
    stdScroll.Click(65, 94);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(103, 71);
    stdScroll.Click(84, 93);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(137, 92);
    stdScroll.Click(87, 77);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(78, 105);
    stdScroll.Click(70, 77);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit");
    scrollSaisieEdit.Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll2.Click(99, 129);
    stdScroll.Click(72, 90);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(119, 143);
    stdScroll.Click(71, 75);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(153, 155);
    stdScroll.Click(68, 73);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(157, 183);
    stdScroll.Click(74, 77);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(120, 192);
    stdScroll.Click(81, 80);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(121, 207);
    stdScroll.Click(77, 92);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(139, 229);
    stdScroll.Click(79, 80);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll2.Click(154, 223);
    stdScroll2.Keys("[Down]");
    stdScroll.Click(103, 85);
    stdScroll.Click(94, 73);
    stdScroll.Window("TScrollSaisieEdit").Keys("test[Tab]");
    stdScroll.Window("TScrollCoche", "", 6).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 4).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 3).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    wndTfmEtatDocument.VCLObject("StdPanel1").VCLObject("btValider").Click(71, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(432, 510);
    tfmMenu.MainMenu.Click("[1]|[11]|[0]");
    wndTfmEtatDocument = stdScrollBox.Window("TfmEtatDocument", "Etats des documents");
    apiPanel = wndTfmEtatDocument.VCLObject("panGeneral");
    stdScroll = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll.Click(133, 95);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "En cours");
    scrollSaisieEdit.Click(79, 10);
    scrollSaisieEdit.Keys(" devis modif");
    stdScroll.Click(111, 109);
    stdScroll2 = apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList");
    stdScroll2.Click(66, 85);
    stdScroll.Click(109, 76);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "En cours");
    scrollSaisieEdit.Click(65, 13);
    scrollSaisieEdit.Keys(" sit modif[Tab]");
    stdScroll.Click(134, 93);
    stdScroll2.Click(46, 162);
    stdScroll.Click(80, 95);
    stdScroll.Click(98, 80);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "Test");
    scrollSaisieEdit.Click(47, 13);
    scrollSaisieEdit.Keys(" br frns modif[Enter]");
    stdScroll.Click(122, 88);
    stdScroll2.Click(121, 225);
    stdScroll.Click(100, 80);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "En cours");
    scrollSaisieEdit.Click(49, 13);
    scrollSaisieEdit.Keys(" cmd frns[BS][BS][BS][BS]st modif");
    scrollSaisieEdit.MouseWheel(1);
    stdScroll.Click(104, 106);
    stdScroll.Click(102, 93);
    wndTfmEtatDocument.VCLObject("StdPanel1").VCLObject("btValider").Click(38, 20);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(433, 511);
    tfmMenu.MainMenu.Click("[1]|[11]|[0]");
    wndTfmEtatDocument = stdScrollBox.Window("TfmEtatDocument", "Etats des documents");
    apiPanel = wndTfmEtatDocument.VCLObject("panGeneral");
    stdScroll = apiPanel.VCLObject("panTypedList").VCLObject("scInitTypedList");
    stdScroll.Click(94, 73);
    stdScroll2 = apiPanel.VCLObject("panList").VCLObject("scInitList");
    stdScroll2.Click(93, 94);
    var<TWinTST> stdPanel = wndTfmEtatDocument.VCLObject("StdPanel1");
    var<TWinTST> stdBoutImage = stdPanel.Window("TPanel").Window("TStdBoutImage");
    stdBoutImage.Click(16, 12);
    stdScroll.Click(137, 153);
    stdScroll2.Click(131, 80);
    stdBoutImage.Click(7, 7);
    stdScroll.Click(73, 218);
    stdScroll2.Click(103, 90);
    stdBoutImage.Click(17, 10);
    stdPanel.VCLObject("btValider").Click(39, 13);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(331, 507);
}

void T_Initialisation_0060() {  //Taux de TVA
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[6]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmTVAParCategorie = stdScrollBox.Window("TfmTVAParCategorie", "Taux de TVA");
    var<TWinTST> stdPageControl = wndTfmTVAParCategorie.VCLObject("StdPageControl");
    var<TWinTST> stdScroll = stdPageControl.VCLObject("CatFrance").VCLObject("Panel_France").VCLObject("ApiPanelScroll_France").VCLObject("Scroll_France");
    stdScroll.Click(420, 26);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", " ", 4);
    scrollSaisieEdit.Click(115, 10);
    mde.Window("TCbxPopupList").Click(99, 9);
    stdScroll.Click(404, 41);
    scrollSaisieEdit.Click(114, 10);
    var<TWinTST> cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(112, 33);
    cbxPopupList.Click(112, 33);
    cbxPopupList.Click(61, 37);
    stdScroll.Click(361, 58);
    scrollSaisieEdit.Click(113, 9);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(114, 28);
    cbxPopupList.Click(114, 28);
    cbxPopupList.Click(67, 24);
    stdScroll.Click(354, 74);
    scrollSaisieEdit.Click(112, 13);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(120, 19);
    cbxPopupList.Click(120, 22);
    cbxPopupList.Click(43, 52);
    stdScroll.Click(355, 99);
    scrollSaisieEdit.Click(114, 12);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.DblClick(113, 33);
    cbxPopupList.Click(60, 33);
    stdScroll.Click(351, 112);
    scrollSaisieEdit.Click(114, 8);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.DblClick(112, 24);
    cbxPopupList.Click(50, 29);
    stdScroll.Click(472, 28);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", " ", 3);
    scrollSaisieEdit.Click(114, 10);
    mde.Window("TCbxPopupList").Click(95, 113);
    stdScroll.Click(534, 43);
    scrollSaisieEdit.Click(114, 8);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(114, 22);
    cbxPopupList.Click(48, 34);
    stdScroll.Click(486, 59);
    scrollSaisieEdit.Click(113, 11);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(116, 29);
    cbxPopupList.Click(70, 22);
    stdScroll.Click(481, 76);
    scrollSaisieEdit.Click(115, 9);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(117, 25);
    cbxPopupList.Click(57, 23);
    stdScroll.Click(480, 95);
    scrollSaisieEdit.Click(117, 15);
    scrollSaisieEdit.Click(117, 13);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(116, 24);
    cbxPopupList.Click(53, 32);
    stdScroll.Click(475, 109);
    scrollSaisieEdit.Click(115, 16);
    scrollSaisieEdit.Click(116, 14);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(116, 36);
    cbxPopupList.Click(46, 26);
    stdPageControl.Click(342, 5);
    stdScroll = stdPageControl.VCLObject("CatAutre1").VCLObject("Panel_Autre1").VCLObject("ApiPanelScroll_Autre1").VCLObject("Scroll_Autre1");
    stdScroll.Click(417, 21);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", " ");
    scrollSaisieEdit.Click(114, 5);
    mde.Window("TCbxPopupList").Click(89, 6);
    stdScroll.Click(406, 46);
    scrollSaisieEdit.Click(114, 13);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(108, 29);
    cbxPopupList.Click(113, 23);
    cbxPopupList.Click(71, 33);
    stdScroll.Click(401, 58);
    scrollSaisieEdit.Click(121, 8);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(114, 21);
    cbxPopupList.Click(114, 21);
    cbxPopupList.Click(56, 27);
    stdScroll.Click(444, 27);
    scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", " ", 3);
    scrollSaisieEdit.Click(116, 13);
    mde.Window("TCbxPopupList").Click(74, 112);
    stdScroll.Click(516, 45);
    scrollSaisieEdit.Click(119, 7);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(108, 27);
    cbxPopupList.Click(53, 37);
    stdScroll.Click(468, 60);
    scrollSaisieEdit.Click(118, 9);
    cbxPopupList = mde.Window("TCbxPopupList");
    cbxPopupList.Click(115, 25);
    cbxPopupList.Click(80, 28);
    wndTfmTVAParCategorie.VCLObject("StdPanel1").VCLObject("btValider").Click(46, 8);
    mde.Window("TApiMessageDlg", "Taux de TVA").Window("TApiPanel").Window("TStdBout").Click(36, 9);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(552, 498);
    tfmMenu.MainMenu.Click("[1]|[17]|[6]");
    wndTfmTVAParCategorie = stdScrollBox.Window("TfmTVAParCategorie", "Taux de TVA");
    var<TWinTST> apiPanel = wndTfmTVAParCategorie.VCLObject("StdPageControl").VCLObject("CatFrance").VCLObject("Panel_France").VCLObject("PanelBas");
    apiPanel.VCLObject("CompteVPort").Click(92, 10);
    stdScroll = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
    var<TWinTST> scrollBar = stdScroll.Window("TScrollBar");
    scrollBar.wPosition = 92;
    scrollBar.wPosition = 111;
    scrollBar.wPosition = 130;
    scrollBar.wPosition = 149;
    scrollBar.wPosition = 130;
    scrollBar.wPosition = 111;
    stdScroll.DblClick(23, 98);
    apiPanel.VCLObject("CompteAPort").Click(31, 8);
    apiPanel.VCLObject("CompteVFrais").Click(41, 6);
    apiPanel.VCLObject("CompteAFrais").Click(22, 8);
    wndTfmTVAParCategorie.VCLObject("StdPanel1").VCLObject("btValider").Click(36, 20);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(278, 499);
}

void P_Creer_Copier_Modifier_Supprimer(TStringTST OptionMenu, TStringTST NomListe, TStringTST NomFiche, int Code) {

    // On prendra le code en index s'il est renseigni 
    if( Code = 0 ) {
        Code = 1;
    }

    Code = F_Creer(OptionMenu,NomListe,NomFiche,Code);
    Code = F_Copier(OptionMenu,NomListe,NomFiche,Code);
    Code = F_Modifier(OptionMenu,NomListe,NomFiche,Code);
    Code = F_Supprimer(OptionMenu,NomListe,NomFiche,Code);
}

int F_Creer(TStringTST OptionMenu, TStringTST NomListe, TStringTST NomFiche, int Code) {
    // Cr�ation Manuelle
    F_Menu_Click(OptionMenu);
    var<TWinTST> maliste = F_Liste(NomListe);
    F_Liste_BtClic(maliste,"Cr�er");
    var<TWinTST> mafiche = F_Fiche("*",NomFiche);
    mafiche.VCLObject("saiCode").Keys( toStr(Code) );
    //mafiche.VCLObject("saiLibelle").Keys("Famille " & Code);
    P_Fiche_Creation_Saisies(mafiche);
    F_Fiche_BtClic(mafiche,"Valider");
    P_CloseAllForm();
    
    return Code + 1;
    
}

int F_Copier(TStringTST OptionMenu, TStringTST NomListe, TStringTST NomFiche, int Code) {
    // Cr�ation Par copie 
    F_Menu_Click(OptionMenu);
    var<TWinTST> maliste = F_Liste(NomListe);
    F_Liste_Filtre(maliste, TCodeTST(Code-1) );
    F_Liste_BtClic(maliste,"Copier");
    var<TWinTST> mafiche = F_Fiche("*",NomFiche);
    mafiche.VCLObject("saiCode").Keys( toStr(Code) );
    F_Fiche_BtClic(mafiche,"Valider");
    P_CloseAllForm();
    
    return Code + 1;
}

int F_Modifier(TStringTST OptionMenu, TStringTST NomListe, TStringTST NomFiche, int Code) {
    Code = F_Creer(OptionMenu,NomListe,NomFiche,Code);
    Code = Code-1;
    
    F_Menu_Click(OptionMenu);
    var<TWinTST> maliste = F_Liste(NomListe);
    F_Liste_Filtre(maliste, TCodeTST(Code) );
    F_Liste_BtClic(maliste,"Modifier");
    var<TWinTST> mafiche = F_Fiche("*",NomFiche);
    P_Fiche_Modif_Saisies(mafiche);
    F_Fiche_BtClic(mafiche,"Valider");
    P_CloseAllForm();
    
    return Code + 1;
    
}

int F_Supprimer(TStringTST OptionMenu, TStringTST NomListe, TStringTST NomFiche, int Code) {
#if 0    
    Code = F_Creer(OptionMenu,NomListe,NomFiche,Code);
    Code = Code-1;
    
    F_Menu_Click(OptionMenu);
    var<TWinTST> maliste = F_Liste(NomListe);
    F_Liste_Filtre(maliste, TCodeTST(Code) );
    F_Liste_BtClic(maliste,"Supprimer");

    // Dans certains cas la suppression d'un �l�ment de la liste Affiche un 1er message avvertissement
    // sur lequel on n'a qu'un bouton Ok
    var<TWinTST> MessageATTENTION = Sys.Process("mde").WaitWindow("TApiMessageDlg", "Suppression", -1, 500);
    if( MessageATTENTION.Exists ) {
        var<TWinTST> Bt = MessageATTENTION.FindChild( Array("Params"), Array("*Ok*"),3);
        if( Bt.Exists ) {
            Bt.Click();
        }    
    }
    
    var<TWinTST> monmessage = F_Message("*","Suppression");
    F_Message_BtClic(monmessage,"Oui");
    P_CloseAllForm();
    
#endif    
    return Code;
}

void T_Initialisation_0061() {  //Comptes/TVA par type d'�l�ment
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[1]|[17]|[7]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> wndTfmTVATravaux = stdScrollBox.Window("TfmTVATravaux", "Comptes/TVA par type d'�l�ment");
    var<TWinTST> apibatPageControl = wndTfmTVATravaux.VCLObject("panGeneral").VCLObject("pcCategorie");
    var<TWinTST> tabSheet = apibatPageControl.VCLObject("tsFrance");
    var<TWinTST> apiPanel = tabSheet.VCLObject("panScFrance");
    var<TWinTST> stdScroll = apiPanel.VCLObject("pnNatureFrance").VCLObject("scNatureTxFrance");
    stdScroll.Click(38, 41);
    stdScroll.Click(42, 53);
    stdScroll.Click(48, 71);
    stdScroll.Click(44, 57);
    stdScroll.Click(42, 42);
    stdScroll.Click(39, 26);
    var<TWinTST> stdScroll2 = apiPanel.VCLObject("pnCompteTVAFrance").VCLObject("scCptTVAFrance");
    stdScroll2.Window("TScrollHeader").Drag(238, 5, -51, -9);
    stdScroll2.Click(232, 21);
    var<TWinTST> scrollSaisieEdit = stdScroll2.Window("TScrollSaisieEdit", "701FCPF20");
    scrollSaisieEdit.Click(73, 13);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(101, 64);
    stdScroll2.Click(253, 47);
    scrollSaisieEdit.Click(69, 14);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(38, 68);
    stdScroll2.Click(324, 24);
    var<TWinTST> scrollSaisieEdit2 = stdScroll2.Window("TScrollSaisieEdit", "701FCPM20");
    scrollSaisieEdit2.Click(61, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(81, 62);
    stdScroll2.Click(287, 44);
    scrollSaisieEdit2.Keys("[Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del][Del]701020[Tab]");
    var<TWinTST> scrollSaisieEdit3 = stdScroll2.Window("TScrollSaisieEdit", "701FCPA20");
    scrollSaisieEdit3.Click(58, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(61, 72);
    stdScroll2.Click(404, 24);
    scrollSaisieEdit3.Click(66, 8);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(32, 68);
    stdScroll2.Click(461, 21);
    var<TWinTST> scrollSaisieEdit4 = stdScroll2.Window("TScrollSaisieEdit", "701FCPS20");
    scrollSaisieEdit4.Click(63, 9);
    var<TWinTST> stdScroll3 = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
    stdScroll3.DblClick(107, 64);
    stdScroll3.DblClick(90, 66);
    stdScroll2.Click(466, 37);
    scrollSaisieEdit4.Click(65, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(75, 71);
    stdScroll2.Click(524, 38);
    var<TWinTST> scrollSaisieEdit5 = stdScroll2.Window("TScrollSaisieEdit", "701FCPO20");
    scrollSaisieEdit5.Click(65, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(104, 63);
    stdScroll2.Click(524, 19);
    scrollSaisieEdit5.Click(61, 9);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(102, 64);
    stdScroll3 = tabSheet.VCLObject("panScFranceAc").VCLObject("pnCompteTVAFranceAc").VCLObject("scCptTVAFranceAc");
    stdScroll3.Click(62, 43);
    stdScroll2.Click(607, 27);
    var<TWinTST> scrollSaisieEdit6 = stdScroll2.Window("TScrollSaisieEdit", "701FCPP20");
    scrollSaisieEdit6.Click(67, 6);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(86, 64);
    stdScroll2.Click(595, 43);
    scrollSaisieEdit6.Click(68, 13);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(104, 67);
    stdScroll3.Click(185, 25);
    var<TWinTST> scrollSaisieEdit7 = stdScroll3.Window("TScrollSaisieEdit", "607100", 5);
    scrollSaisieEdit7.Click(57, 12);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(64, 198);
    stdScroll.Click(59, 41);
    stdScroll2.Click(239, 26);
    scrollSaisieEdit.Click(71, 8);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(155, 54);
    scrollSaisieEdit.Drag(48, 9, -97, -9);
    scrollSaisieEdit.Keys("^c");
    stdScroll2.Click(252, 40);
    scrollSaisieEdit.Drag(65, 8, -107, -4);
    scrollSaisieEdit.Keys("^v");
    stdScroll2.Click(310, 21);
    scrollSaisieEdit2.Click(61, 8);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(118, 52);
    stdScroll2.Click(320, 46);
    scrollSaisieEdit2.Click(62, 13);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(144, 52);
    stdScroll2.Click(387, 25);
    scrollSaisieEdit3.Click(67, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(152, 55);
    stdScroll2.Click(386, 39);
    scrollSaisieEdit3.Click(65, 8);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(152, 53);
    stdScroll2.Click(446, 20);
    scrollSaisieEdit4.Click(61, 9);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(154, 56);
    stdScroll2.Click(439, 34);
    scrollSaisieEdit4.Click(63, 12);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(152, 55);
    stdScroll2.Click(538, 21);
    scrollSaisieEdit5.Click(66, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(151, 54);
    stdScroll2.Click(523, 44);
    scrollSaisieEdit5.Click(63, 11);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(149, 55);
    stdScroll2.Click(582, 27);
    scrollSaisieEdit6.Click(71, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(152, 54);
    stdScroll2.Click(585, 43);
    scrollSaisieEdit6.Click(66, 9);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(147, 68);
    stdScroll3.Click(194, 31);
    scrollSaisieEdit7.Click(58, 10);
    stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�").VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(51, 202);
    stdScroll.Click(56, 59);
    stdScroll2.Click(260, 23);
    scrollSaisieEdit.Click(78, 10);
    scrollSaisieEdit.Click(74, 9);
    var<TWinTST> stdFormListe = stdScrollBox.Window("TStdFormListe", "Liste des comptes du plan comptable g�n�ral : Compte et Libell�");
    stdFormListe.VCLObject("panFiltres").VCLObject("saiQuickSearch").MouseWheel(-4);
    stdScroll = stdFormListe.VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
    var<TWinTST> scrollBar = stdScroll.Window("TScrollBar");
    scrollBar.wPosition = 19;
    scrollBar.wPosition = 38;
    scrollBar.wPosition = 57;
    scrollBar.wPosition = 76;
    scrollBar.wPosition = 95;
    scrollBar.wPosition = 114;
    scrollBar.wPosition = 133;
    stdScroll.DblClick(40, 194);
    apibatPageControl.Click(91, 10);
    wndTfmTVATravaux.VCLObject("StdPanel1").VCLObject("btValider").Click(53, 14);
    tfmMenu.Keys("~i");
    stdScrollBox.Click(424, 300);
}

//********* END OF FILE *************************************************************** 