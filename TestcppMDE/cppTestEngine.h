//*****************************************************************************************
// TEST ENGINE (TestComplete) C++ emulation 
//
// These objects emulates the TestComplete API (objects), in C++
// 
//*****************************************************************************************

#ifndef _CPPTESTENGINE_H
#define _CPPTESTENGINE_H

#include "cppToJavaScript.h"  // for TStringTST...

class TWinTST;
class TStringTST;

class TRunTObj  {
public:
    bool Exists;
    bool WaitProperty( TStringTST _propName, bool _bVal, int _timoutMs = 0);
    TWinTST WaitWindow( TStringTST _winClass, TStringTST _title, int xxx, int _timeout);
    TWinTST Window(TStringTST _winClass, TStringTST _title = "*", int _grpIdx = -1);
    TWinTST VCLObject(TStringTST _objName);
    TWinTST WaitVCLObject(TStringTST _objName, int _timeout);
protected:
    TRunTObj() {}
};

class TProcessTST : public TRunTObj  {
public:
   void Terminate();
};

enum TClickOption { skNONE = 0, skShift = 1, skCtrl = 2 };

class TMenuTST {
public:
    void Click(TStringTST _subMenuPath);
    void Close();
};

class TScrollTST {
public:
    int Pos;
};


class TWinTST : public TRunTObj {
public:
    TStringTST WndCaption;
    TMenuTST   MainMenu;
    TMenuTST   PopupMenu;
    TScrollTST VScroll;
    TStringTST Params;
    TStringTST FullName;
    static int wPosition;  // pour les scrollBar ??
    bool Enabled;
    bool Focused;
    bool VisibleOnScreen;
    TStringTST Caption;
    int TabOrder;

    void Click(); // pour les boutons ??
    void Click(int _x, int _y, int _opt = skNONE);
    void ClickR(int _x, int _y);
    void DblClick(int _x, int _y);
    void ClickItem(TStringTST _ItemPath);
    void ClickItem(TStringTST _ItemPath, bool xxx);
    void DblClickItem(TStringTST _ItemPath);
    void MouseWheel(int _move);
    void Drag(int _x, int _y, int _z, int _t);
    void Keys(TStringTST _keyName);
    void SetFocus();
    void Close();
    void OpenFile(TStringTST _filename, TStringTST _filter);

    TWinTST WaitNamedChild( TStringTST _nomChild, int _timeout);
    TWinTST FindChild(Array _propName, Array _propVal, int _depth=0);
    TWinTST FindChild(TStringTST _propName, TStringTST _propVal, int _depth=0);
    varArray<TWinTST> FindAllChildren(TStringTST arrayX, TStringTST arrayY, int xxx);
};


class TRunnerTST {
public:
    void stop(bool xxx);
};
extern TRunnerTST Runner;


class TSystemTST {
public:
    TProcessTST WaitProcess(TStringTST _name, int _timeout);
    TProcessTST WaitProcess(TStringTST _name);
    TProcessTST Process( TStringTST _name);
    TWinTST WaitChild(TStringTST ChildName, int _timeout);
    void Keys( TStringTST _keyName);
    TStringTST Clipboard();
    struct {
        int Picture;
    } Desktop;
}; 
extern TSystemTST Sys;

struct TTestSuiteVariables;  // FORWARD  ( la liste des variables de conf d�finies plus loin)

class TProjectSuiteTST {
 public:
    TStringTST Path;
    TTestSuiteVariables & Variables;
    TProjectSuiteTST() : Variables(*(TTestSuiteVariables*)(0)) { }
};
extern TProjectSuiteTST ProjectSuite;


class TFileItemTST {
public:
   void Check(TStringTST filePath);
};


class TFilesUtilsTST {
public:
    bool Compare( TStringTST file1, TStringTST file2, int xxx, bool yyy);
    bool Contains(TStringTST fileName);
    void Remove(TStringTST fileName);
    bool Add(TStringTST filePath, TStringTST name ); 
    TFileItemTST Items(TStringTST name);
};
extern TFilesUtilsTST Files;

class TDbgServicesTST {
public:
    TWinTST LaunchApplication(TStringTST exePath, TStringTST argList);
};
extern TDbgServicesTST DbgServices;

class TElementTST {
public:
  bool isNothing;
  TStringTST Caption;
};

class TTestItemTST {
public:
  bool isNothing;
  TStringTST Name;
  int        Iteration;
  int        Count;
  bool Enabled;
  int  ItemCount;
  TElementTST ElementToBeRun;
  static TTestItemTST Current;  // propri�t� de Project.TestItems

  TTestItemTST TestItem(int idx);
};

class TProjectTST {
public:
    TTestItemTST TestItems;
};
extern TProjectTST Project;


class TTestedAppTST {
public:
  void Run(int, bool);  
};

class TTestedAppsListTST {
 public:    
     TTestedAppTST mde;
};
extern TTestedAppsListTST TestedApps;

class TLoggerTST {
public:
    void Error( TStringTST _messErr);
    void Message( TStringTST  _mess);
    void Warning( TStringTST _messWarn);
    void Picture(int _picture, TStringTST  _xxx );
    void LockEvents(int);
};
extern TLoggerTST Log;

class TErrorListTST {
 public:
   int number;
};
extern TErrorListTST err;

class TAqUtilTST {
 public: 
   void Delay( int dTms );
};
extern TAqUtilTST aqUtils;


class TBuiltInTST {
 public:
   void ShowMessage(TStringTST _msg ) {} // Displays a message Box with OK button
};
extern TBuiltInTST BuiltIn;


// Other objects
extern int Timer;
extern int Instance;


#endif
// END OF FILE ********************************************************************************
