//**********************************************************************************
// LANCEMENT DE MDE... jusqu'au stade de la "main win"
//**********************************************************************************

#define IMPLEMENTATION

#include "_common.h"

//USEUNIT JavaScriptExtens

//USEUNIT JsTestEngine

#include "MdeAppli_Unit.cpp"
//USEUNIT MdeAppli_Unit

#include "MdeUI_Menu_Unit.cpp"
//USEUNIT MdeUI_Menu_Unit

#include "MdeUI_Ventes_Unit.cpp"
//USEUNIT MdeUI_Ventes_Unit

//***********************************************************************
// PARAMETERS OF TESTSUITE 
var<TTestSuiteVariables> conf = ProjectSuite.Variables;

//***********************************************************************

//***********************************************************************
// S/P pour rechercher et �tablir la MainWin, mainFrame, mainMenu, barIcones
bool SP_set_MDE_MainWin_and_Menu() {

    mde.menuFram.win = mde.process.win.WaitVCLObject("fmMenu", 5000);
    if (!mde.menuFram.win.Exists) {
        Log.Error("Menu frame of MDE didn't appear.");
        return 1;
    }
    mdeHeadMenu.win = mde.menuFram.win;

    mde.mainWin.win = mde.menuFram.win.WaitVCLObject("ClientControl", 5000);
    if (!mde.mainWin.win.Exists) {
        Log.Error("Main window didn't appear.");
        return 1;
    }

    mde.barIcon.win = mde.menuFram.win.WaitVCLObject("BarreIcones", 3000);
    if (!mde.barIcon.win.Exists) {
        Log.Error("Icon bar didn't appear.");
        return 1;
    }
    return 0; // Ok
}

//***********************************************************************
// Lance l'application MDE dans tous les cas
bool launch_MDE() {
    Log.Message("Launching MDE application.");
    var<TStringTST> cmdArgs = "/DEV /NOVERSIONCHECK /NOSPLASH /NOCRYPT";
    var<TStringTST> exePath = ProjectSuite.Path + "..\\" + conf.MDEexe_subDir + "\\mde.exe";

    // Lancement de MDE
    mde.process.win = DbgServices.LaunchApplication(exePath, cmdArgs);
    if (!mde.process.win.Exists) {
        Log.Error("Failed to start MDE.exe.");
        return 1;
    }

    // Attente de la fen�tre de login - mot de passe
    var<TWinTST> loginWin = mde.process.win.WaitWindow("TFormLogin", "Identification", -1, 10000);
    if (! loginWin.Exists) {
        Log.Error("Login window didn't appear.");
        return 1;
    }
    Sys.Keys("[F2]");

    // Attente que la fen�tre de login disparaisse
    varI<bool> bOk = loginWin.WaitProperty("Exists", false, 3000);
    if (!bOk) {
        Log.Error("The login window should have disappeared after [F2].");
        return 1;
    }
    Log.Message("User login done.");

    return SP_set_MDE_MainWin_and_Menu();
}

//***********************************************************************
bool launch_MDE_IFNE() {
    var<TProcessTST> mdeIst = Sys.WaitProcess("mde");
    if( ! mdeIst.Exists ) {
        return launch_MDE();
    }else{
        Log.Message("Reusing running MDE application instance.");
        mde.process.win = CAST(TWinTST, mdeIst);
        return SP_set_MDE_MainWin_and_Menu(); 
    }     
}



//****** END OF FILE ***************************************************************