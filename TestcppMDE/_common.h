//*************************************************************************
// Shared by all Javascript modules:
//   - cpp to Java script types.
//   - Test engine global object
//   - Global (exported) application types
//   - Global (exported) application var and function
//*************************************************************************
#ifndef  _COMMON_TST_H
#define  _COMMON_TST_H

#include "cppToJavaScript.h"
#include "cppTestEngine.h"
#include "typesApplicatif.h"
#include "globalExport.h"

#endif
// END OF FILE ********************************************************************************
