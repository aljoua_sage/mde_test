//*****************************************************************************************
// Java Script emulation, from C++ 
//
// Implementation of classes and function that emulate Java script classes and function 
//*****************************************************************************************

#include "cppToJavaScript.h"


// Some string functions, comming from VB script.
int Len( TStringTST _str) { return 0; }
TStringTST Left(TStringTST _str, int len) { return TStringTST(); }
TStringTST Right( TStringTST _str, int len) { return TStringTST(); }
TStringTST Trim( TStringTST _str) { return TStringTST(); }
TStringTST UCase( TStringTST _str) { return TStringTST(); }
TStringTST Mid0( TStringTST _str, int _start) { return TStringTST(); }
int InStr0( TStringTST _str, TStringTST _pattern) { return 0; }
TStringTST Replace( TStringTST _what, TStringTST _searched, TStringTST _replaceBy ) { return TStringTST(); }
bool Contains( TStringTST _str, TStringTST _pattern) { return false; }
varArray<TStringTST> Split( TStringTST _str, TStringTST _sep) { return *(varArray<TStringTST>*)(0); }


TStringTST Array( TStringTST _elem1) { return TStringTST(); }
TStringTST Array( TStringTST _elem1, TStringTST _elem2) { return TStringTST(); }
TStringTST Array( TStringTST _elem1, TStringTST _elem2, TStringTST _elem3) { return TStringTST(); }


// END OF FILE ********************************************************************************
