//*****************************************************************************************
// Widgets_Unit.cpp
//
//    - Common Widgets defininiton and Management
//
//*****************************************************************************************
#pragma once

#include "MdeUI_Export.h" // pour import mde

#include "cppTestEngine.h"

#include "WidgetsExportation.h"

#include "JavaScriptExtens.cpp"  // for toStr...
//USEUNIT JavaScriptExtens


// Classe pour un type d'action (un verbe)
JSobject TTypAction {
};

#ifdef IMPLEMENTATION
//Constantes servant � identifier les diff�rentes actions
const_js<TTypAction> ACT_OPEN = "Open";
#endif


// Un objet UI, avec un nom et un parent
JSobject TUIobject  {
   field<TWinTST>     win = Sys.WaitChild("",0);
   field<TStringTST>  name = "";
   fieldR<TUIobject>  parent = NULLREF(TUIobject);
   DECLAR_METHOD(bool, checkWin, ()) {}
};
   
// Forme ou boite de dialogue
JSobject TDlgForm : TUIobject {
    JSCODE( TUIobject.call(this); )
    field<TStringTST> winClass = "";
    field<TStringTST> caption = "";
    field<TUIobject>  rbase;
    DECLAR_METHOD(bool, checkIsOpen, ()) { return 0;}
    DECLAR_METHOD(bool, checkIsClosed, ()) { return 0; }
    DECLAR_METHOD(bool, checkWin, ()) {}
}; 

// Classe pour une action sur un objet
JSobject TUIaction {
    field<TTypAction> typAction;      // index dans la list du menu parent
    field<TDlgForm>   dlgForm;
};

// Classe de base pour un sub menu
JSobject UIsubMenu {
    fieldI<UIsubMenu> parent = NULLREF(UIsubMenu);
    fieldI<int>       idx = -1;      // index dans la list du menu parent
    field<TStringTST> caption = "";  // libell�
    DECLAR_METHOD(void, click, () ) {}
};

// Un main menu (comme celui de la main win)
JSobject UIMainMenu : UIsubMenu {
    field<TWinTST> win;
};

// Un bouton
JSobject UIButton : TUIobject {
    JSCODE(TUIobject.call(this); )
    field<TStringTST> ID;
    DECLAR_METHOD(bool, checkWin, ()) {return false;}
    DECLAR_METHOD(void, click, ()) {}
};

// Un Edit
JSobject UIEdit : TUIobject{
    JSCODE(TUIobject.call(this); )
    DECLAR_METHOD(void, hitKeys,( TStringTST _keys)) {}
    JSCODE(UIEdit.prototype.checkWin = TUIobject.prototype.checkWin; )
};


//*************************************************************************************
inline bool checkIsOpen(TDlgForm This ) {
   
   // Check if parent window is displayed 
   if (! This.rbase.win.Exists) {
       Log.Error("Main window isn't displayed.");
       return 1;
   }

   // Wait until windows is displayed
   This.win = This.rbase.win.WaitWindow(This.winClass, "*" + This.caption + "*", -1, 10000);
   if (!This.win.Exists) {
      Log.Error("Fail to display dlgForm:" + This.winClass + "::  " + This.caption);
      return 1;
   } 
   return 0; // Ok
}

//*************************************************************************************
inline bool checkIsClosed(TDlgForm This) {

    // Check if parent window is displayed 
    if ( !This.rbase.win.Exists) {
        Log.Error("Process Main window isn't displayed.");
        return 1;
    }

    // Get windows handle (or not) immediatly
    This.win = This.rbase.win.WaitWindow(This.winClass, "*" + This.caption + "*", -1, 0);
    if (! This.win.Exists) return 0;

    varI<bool> bOk = This.win.WaitProperty("Exists", false, 6000 );
    if( !bOk ) {
        Log.Error("Fail to close dlgForm:" + This.winClass + "::  " + This.caption);
        return 1;
    }
    return 0; // Ok
}

//*************************************************************************************
inline bool checkWin(TDlgForm This) {

    // Check if parent window is displayed 
    if (!This.rbase.win.Exists) {
        Log.Error("Process Main window isn't displayed.");
        return 1;
    }

    // Check windows is displayed
    This.win = This.rbase.win.Window(This.winClass, "*" + This.caption + "*", -1);
    if (!This.win.Exists) {
        Log.Error("dlgForm isn't displayed:" + This.winClass + "::  " + This.caption);
        return 1;
    }
    return 0; // Ok
}


//*************************************************************************************
inline bool checkWin(TUIobject This ) {
    varArray<TStringTST> subPath = This.name.split("/");
    var<TWinTST> win = This.parent.win;
    if (!win.Exists) {
        Log.Error("The parent window do not exist for UIobject path: " + This.name );
        return 1;
    }
    for(int i=0; i<subPath.length; i++) {
        win = win.VCLObject( subPath[i] );
        if (!win.Exists) {
            Log.Error("The parent window do not exist for UIobject path: " + This.name);
            return 1;
        }
    }
    This.win = win; // set
}

//*************************************************************************************
// Method: UIButton::checkWin()
inline bool checkWin(UIButton This) {

    varI<bool> bErr = This.parent.checkWin();
    if (bErr) return 1;

    if (!This.win.Exists) {
        // First try with the property "Params"
        This.win = This.parent.win.FindChild("Params", "*" + This.ID + "*", 3); // avec profondeur 3
                                                                                // Second try with the property "names"
        if (!This.win.Exists) This.win = This.parent.win.FindChild("name", "*" + This.ID + "*", 3); // avec profondeur 3

        if (!This.win.Exists) {
            Log.Error("Le bouton n'existe pas: " + This.ID);
            return 1;
        }
    }
    return 0; // Ok
}

//*************************************************************************************
// Method: UIButton::click()
inline void click(UIButton This) {

    varI<bool> bErr = This.checkWin();
    if(bErr) return;
    // Le bouton existe, on peut clicker
    This.win.Click();
}

//*************************************************************************************
// Method: UIEdit::hitKeys( _keys )
inline void hitKeys(UIEdit This, TStringTST _keys ) {
    
    varI<bool> bErr = This.checkWin();
    if (bErr) return;

    // L'Edit existe, on peut clicker
    This.win.Keys(_keys);
}


//*************************************************************************************
// Method: UIsubMenu::click()
inline void click( UIsubMenu This ) {
    varArray<UIsubMenu> chainMenu = new Array();
    var<UIsubMenu> child = This;
    for(int i=0; i<10; i++ ) {
        if (child.idx == -1) break;
        chainMenu[i] = child;
        child = child.parent;
    } 
    var<UIMainMenu> mainMenu = CAST(UIMainMenu,child);
    var<TWinTST> mainMenuWin = mainMenu.win;


    var<TStringTST> menuKey="";
    for(int i=chainMenu.length-1; i>=0; i--) {
        menuKey = menuKey + "[" + toStr(chainMenu[i].idx) + "]";
        if(i>0) menuKey = menuKey + "|";
    } 
    
    // Mettre le focus sur le menu
    if( mainMenuWin.Focused == false) mainMenuWin.SetFocus();
    mainMenuWin.MainMenu.Click(menuKey);
}


//*****************************************************************************************
inline UIsubMenu newMenu( UIsubMenu _parent, int _idx, TStringTST _caption, UIsubMenu _subMenu ) {
   _subMenu.parent = _parent;
   _subMenu.idx = _idx;
   _subMenu.caption = _caption;
   return _subMenu;
}

//*****************************************************************************************
inline TUIaction newAction(TTypAction _typAction, TDlgForm _object) {
   var<TUIaction> action = new TUIaction();
   action.typAction = _typAction;
   action.dlgForm = _object;
   return action;
}

//*****************************************************************************************
inline TDlgForm newTDlgForm(TUIobject _rbase, TStringTST _winClass, TStringTST _caption, TDlgForm _dlgFiche ) {
    _dlgFiche.rbase = _rbase;
    _dlgFiche.winClass = _winClass;
    _dlgFiche.caption = _caption;
    return _dlgFiche;
}

//*****************************************************************************************
inline TUIobject newTUIobject(TUIobject _parent, TStringTST _namePath ) {
   var<TUIobject> uiObj = new TUIobject;
   uiObj.parent = _parent;
   uiObj.name = _namePath;
   return uiObj;
}

//*****************************************************************************************
inline UIButton newUIButton(TUIobject _parent, TStringTST _boutonID) {
   var<UIButton> btn = new UIButton;
   btn.parent = _parent;
   btn.ID = _boutonID;
   return btn;
}

//*****************************************************************************************
inline UIEdit newUIEdit(TUIobject _parent, TStringTST _namePath) {
    var<UIEdit> uiEdit = new UIEdit;
    uiEdit.parent = _parent;
    uiEdit.name = _namePath;
    return uiEdit;
}

// END OF FILE ********************************************************************************
