//*****************************************************************************************
// WidgetExportation.h
//
//   Exportation des Widgets et objets de bases
//
//*****************************************************************************************

#ifndef  _WIDGETEXPORTATION_H
#define  _WIDGETEXPORTATION_H

#include "cppToJavaScript.h"


struct UIsubMenu;
UIsubMenu newMenu(UIsubMenu _parent, int _idx, TStringTST _caption, UIsubMenu _subMenu );

// On d�finit un template pour avoir un type de retour pr�cis.
template<typename typMenu> typMenu newMenu(UIsubMenu *_parent, int _idx, TStringTST _caption, typMenu * _newMenu ) {
    // Dans l'impl�mentation C++ bidon, on r�f�rence la fonction pour b�n�ficier d'un type and link checking.
    newMenu(*_parent, _idx, _caption, *_newMenu);
    return *(typMenu*)(0);
}

struct TUIobject;
struct TDlgForm;
TDlgForm newTDlgForm(TUIobject _rbase, TStringTST _winClass, TStringTST _caption, TDlgForm _dlgFiche);
// On d�finit un template pour avoir un type de retour pr�cis.
template<typename typForm> typForm newTDlgForm(TUIobject _rbase, TStringTST _winClass, TStringTST _caption, typForm *_dlgFiche) {
    // Dans l'impl�mentation C++ bidon, on r�f�rence la fonction pour b�n�ficier d'un type and link checking.
    newTDlgForm(_rbase, _winClass, _caption, *_dlgFiche);
    return *(typForm*)(0);
}

struct TUIaction;
struct TTypAction;
TUIaction newAction(TTypAction typAction, TDlgForm _object);

extern const_js<TTypAction> ACT_OPEN;

#endif
// END OF FILE ********************************************************************************
