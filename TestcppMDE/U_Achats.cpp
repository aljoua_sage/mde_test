
#include "_common.h"

//-------------------------------------------------------------------------------
void T_Achats_0001() { //Cr�ation d'une demande de prix Manuellement

    F_Creat_Doc_Ach_Manuel("DF");        
}


//-------------------------------------------------------------------------------
void T_Achats_0002() { //Cr�ation d'une Commande fournisseur Manuellement
        
    F_Creat_Doc_Ach_Manuel("CF");
}


//-------------------------------------------------------------------------------
void T_Achats_0003() { //Cr�ation d'un BL fournisseur Manuellement
        
    F_Creat_Doc_Ach_Manuel("LF");
   
}

//-------------------------------------------------------------------------------
void T_Achats_0004() { //Cr�ation d'une Facture fournisseur Manuellement
        
    F_Creat_Doc_Ach_Manuel("FF");
}

//-------------------------------------------------------------------------------
void T_Achats_0005() { // Cr�ation d'un retour fournisseur Manuellement

    F_Creat_Doc_Ach_Manuel("RF");        
    
}

//-------------------------------------------------------------------------------
void T_Achats_0006() { // Cr�ation d'un Avoir fournisseur Manuellement

    F_Creat_Doc_Ach_Manuel("AF");        
    
}

//-------------------------------------------------------------------------------
void T_Achats_0007() { // Cr�ation d'un AO SST Manuellement

    F_Creat_Doc_Ach_Manuel("AO");        
    
}

//-------------------------------------------------------------------------------
void T_Achats_0008() {// Cr�ation d'un CS SST Manuellement

    F_Creat_Doc_Ach_Manuel("CS");        
}

//-------------------------------------------------------------------------------
void T_Achats_0009() { // Cr�ation d'un FS SST Manuellement

    F_Creat_Doc_Ach_Manuel("FS");        
}

// -------------------------------------------------------------------------------
void T_Achats_0010() { // Cr�ation d'un AS SST Manuellement

    F_Creat_Doc_Ach_Manuel("AS");        
    
}

//-------------------------------------------------------------------------------
void T_Achats_0011() { // Transfert DF => CF

    var<TCodeTST> CodeDoc =  F_Creat_Doc_Ach_Manuel("DF");
    P_Transfert_Doc_Ach("DF",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0012() { // Transfert CF => LF

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("CF");
    P_Transfert_Doc_Ach("CF",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0013() { // Transfert LF => FF

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("LF");
    P_Transfert_Doc_Ach("LF",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0014() { // Transfert FF => AF

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("FF");
    P_Transfert_Doc_Ach("FF",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0015() { // Transfert AO => CS

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("AO");
    P_Transfert_Doc_Ach("AO",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0016() { // Transfert CS => FF

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("CS");
    P_Transfert_Doc_Ach("CS",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0017() { // Transfert FS => AS

    var<TCodeTST> CodeDoc = F_Creat_Doc_Ach_Manuel("FS");
    P_Transfert_Doc_Ach("FS",CodeDoc);        
    
}

// -------------------------------------------------------------------------------
void T_Achats_0018() { // Import du pr�vu Int�gration ligne de document BL
    
    //  Cr�ation d'un chantier avec devis en base de fac
    //  On copie le devis mod�le et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    
    //  On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    
    // On va sur la zone code chantier
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis [*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier").SetFocus();
    
    // On fait F11 pour ouvrir une fiche chantier en cr�ation
    Sys.Keys("[F11]");
    
    // On attend que la fiche chantier s// ouvre
    mafiche = F_Fiche("TfmChantierV2","Chantier [");
    mafiche.WaitProperty("Visible", true, 50000);
    
    // On stock le code du Chantier
    Sys.Keys("^c");
    var<TCodeTST> Code_chantier = Sys.Clipboard();
    
    // On tabule et on enregistre la fiche chantier
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[F2]");
        
    // On revient sur la zone �tat du document et on le passe � accept�
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat").SetFocus();
    Sys.Keys("a");
    
    // On attend la fen�tre d'avertissement et on confirme
    var<TWinTST> monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Confirmer");
        
    // On attend que le devis passe en consultation
    mafiche = F_Fiche("TfmDocDevCli", "Devis [*] [Consultation]");
            
    //  On Valide le doc
    P_Valide_Devis();
    
    //  On ferme tout
    P_CloseAllForm();

    // On ouvre la liste des chantiers
    Sys.Keys("~c");
    delay(200);
    Sys.Keys("l");
    
    var<TWinTST> maliste = F_Liste("Chantiers");
    F_Liste_Filtre(maliste, Code_chantier );
            
    // On ouvre la fiche chantier
    Sys.Keys("~m");
    
    mafiche = F_Fiche("TfmChantierV2", "Chantier");
    
    // On va sur les BL
    Sys.Keys("~d");
    delay(100);
    Sys.Keys("fff");
    delay(100);
    Sys.Keys("[Right]");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);    
    Sys.Keys("[Down]");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);
    
    // On Cr�er un BL
    Sys.Keys("~c");
    delay(100);
    
    mafiche = F_Fiche("TFch_PCF_Reception", "Bon de r�ception");
    
    // On va sur la zone fournisseur
    Sys.Keys("[Tab]");
    
    // On saisi le fournisseur F1
    Sys.Keys("F1");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    
    // On va sur les lignes
    Sys.Keys("~l");
    delay(500);
    
    // On clic sur le bouton d'import du pr�vu
    F_Fiche_BtClic(mafiche,"btImportEltsPrevus");
    
    // On attend la fen�tre d'import du pr�vu
    mafiche = F_Fiche("TfmElementsPrevusFou", "Importer les �l�ments pr�vus");
    delay(500);
    
    var<TWinTST> stdScroll = mafiche.VCLObject("panGeneral").VCLObject("pnListeCdeResFour").VCLObject("pnElemCdeResFour").VCLObject("scElemCdeResFour");
    stdScroll.Click(5, 24);
    Sys.Keys("^[End]");
    stdScroll.Click(7, 182, skShift);
    
    // On valide
    Sys.Keys("[F2]");
    
    mafiche.WaitProperty("Visible",false,30000);
    
    delay(2000);
    
    // On valide le BL
    Sys.Keys("[F2]");
    delay(1000);
    
    P_CloseAllForm();
          
}

// -------------------------------------------------------------------------------
void T_Achats_0019() { // Import du pr�vu Int�gration ligne de document CS
    
    //  Cr�ation d'un chantier avec devis en base de fac
    //  On copie le devis mod�le et on stock le nouveau code
    var<TCodeTST> newcode = F_Copie_Devis("00000001");
    
    //  On r�ouvre le doc
    var<TWinTST> mafiche = F_Open_Devis(newcode);
    
    // On va sur la zone code chantier
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis [*", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteAffaire").VCLObject("panAffChantier").VCLObject("saiCodeChantier").SetFocus();
    
    // On fait F11 pour ouvrir une fiche chantier en cr�ation
    Sys.Keys("[F11]");
    
    // On attend que la fiche chantier s'ouvre
    mafiche = F_Fiche("TfmChantierV2","Chantier [");
    mafiche.WaitProperty("Visible", true, 50000);
    
    // On stock le code du Chantier
    Sys.Keys("^c");
    var<TCodeTST> Code_chantier = Sys.Clipboard();
    
    // On tabule et on enregistre la fiche chantier
    Sys.Keys("[Tab]");
    delay(200);
    Sys.Keys("[F2]");
        
    // On revient sur la zone �tat du document et on le passe � accept�
    Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl").Window("TfmDocDevCli", "Devis * [Modification]", 1).VCLObject("panGeneral").VCLObject("panPage").VCLObject("panDoc").VCLObject("pcOnglet").VCLObject("tsEnTete").VCLObject("panEnTete").VCLObject("panEnTeteRef").VCLObject("panEnTeteEtat").VCLObject("saiEtat").SetFocus();
    Sys.Keys("a");
    
    // On attend la fen�tre d'avertissement et on confirme
    var<TWinTST> monmessage = F_Message("TApiMessageDlg","Avertissement");
    F_Message_BtClic(monmessage,"Confirmer");
        
    // On attend que le devis passe en consultation
    mafiche = F_Fiche("TfmDocDevCli", "Devis [*] [Consultation]");
            
    //  On Valide le doc
    P_Valide_Devis();
    
    //  On ferme tout
    P_CloseAllForm();

    // On ouvre la liste des chantiers
    Sys.Keys("~c");
    delay(200);
    Sys.Keys("l");
    
    var<TWinTST> maliste = F_Liste("Chantiers");
    F_Liste_Filtre(maliste, Code_chantier);
            
    // On ouvre la fiche chantier
    Sys.Keys("~m");
    
    maliste = F_Fiche("TfmChantierV2", "Chantier");
    
    // On va sur les BL
    Sys.Keys("~d");
    delay(100);
    for(int i=0; i<6; i++) {
        Sys.Keys("s");
        delay(10);
    }
    Sys.Keys("fff");
    delay(100);
    Sys.Keys("[Right]");
    delay(100);
    Sys.Keys("[Down]");
    delay(100);    
    Sys.Keys("[Down]");
    delay(100);
    
    // On Cr�er un CS
    Sys.Keys("~c");
    delay(100);
    
    mafiche = F_Fiche("TFch_PCSst_Cde", "Commande sous-traitant");
    
    // On va sur la zone tiers
    Sys.Keys("[Tab]"); 
    
    // On saisi le tiers
    Sys.Keys("ST1");
    delay(50);
    Sys.Keys("[Tab]");
    delay(50);
    
    // On va sur les lignes
    Sys.Keys("~l");
    delay(500);
    
    // On clic sur le bouton d'import du pr�vu
    F_Fiche_BtClic(mafiche,"btImportEltsPrevus");
    
    // On attend la fen�tre d'import du pr�vu
    mafiche = F_Fiche("TfmElementsPrevusSst", "Importer les �l�ments pr�vus");
    delay(500);
    
    
    var<TWinTST> stdScroll = mafiche.VCLObject("panGeneral").VCLObject("pnListeCdeResSst").VCLObject("pnElemCdeResSst").VCLObject("scElemCdeResSst");
    stdScroll.Click(7, 23);
    stdScroll.Click(9, 41, skShift);
    
    // On valide
    Sys.Keys("[F2]");
    
    mafiche.WaitProperty("Visible",false,30000);
    
    delay(2000);
    
    // On valide le BL
    Sys.Keys("[F2]");
    delay(1000);
    
    P_CloseAllForm();
          
}

// -------------------------------------------------------------------------------
void T_Achats_0020() { // Modif chantier ent�te FF
        
    var<TCodeTST> CodeDoc =  F_Creat_Doc_Ach_Manuel("FF");
    
    //  Ouverture de la liste
    P_Open_Lst_Ach("FF", CodeDoc);
    
    Sys.Keys("~m");
      
    //  On attend que la fiche s// ouvre
    var<TWinTST> mafiche = F_Fiche("TFch_PCF_Facture*", "Facture fournisseur");
    
    //  On saisi le chantier 00000001
    mafiche.VCLObject("Onglet_Document").VCLObject("Onglet_Adresse").VCLObject("Panel_Adresse").VCLObject("Panel_Adresse_Bas").VCLObject("Panel_Adresse_Bas_Gauche").VCLObject("Panel_Posit_Chantier").VCLObject("saiChantier").Keys("00000001[Tab]");
    
    //  Message de confirmation
    var<TWinTST> monmessage = F_Message("TApiMessageDlg", "Facture fournisseur");
    F_Message_BtClic(monmessage,"Oui");
    
    //  Message de fin de traitement
    monmessage = F_Message("TApiMessageDlg", "Sage 100 Multi Devis Entreprise");
    F_Message_BtClic(monmessage,"Ok");
    
    // On valide le document
    Sys.Keys("[F2]");
    delay(500);
    Sys.Keys("[F2]");
    delay(500);
    
    P_CloseAllForm();
              
}

// -------------------------------------------------------------------------------
void T_Achats_0021() { // Modif Date ent�te FF
        
    var<TCodeTST> CodeDoc =  F_Creat_Doc_Ach_Manuel("FF");
    
    //  Ouverture de la liste
    P_Open_Lst_Ach("FF",CodeDoc);
    
    Sys.Keys("~m");
      
    //  On attend que la fiche s// ouvre
    var<TWinTST> mafiche = F_Fiche("TFch_PCF_Facture*", "Facture fournisseur");
    
    //  On saisi le chantier 00000001
    mafiche.VCLObject("Panel_CodeDocument").VCLObject("panEnteteInfos").VCLObject("panEnteteBottom").VCLObject("panEnteteDate").VCLObject("DateDoc").Keys("020116[Tab]");
    
    //  Message de confirmation
    var<TWinTST> monmessage = F_Message("TApiMessageDlg", "Facture fournisseur");
    F_Message_BtClic(monmessage,"Oui");
    
    // On valide le document
    Sys.Keys("[F2]");
    delay(500);
    Sys.Keys("[F2]");
    delay(500);
    
    P_CloseAllForm();
              
}

// -------------------------------------------------------------------------------
void T_Achats_0022() { // Int�gration ligne de LF dans FF
        
    F_Creat_Doc_Ach_Manuel("LF");
    
    //  Ouverture de la liste
    P_Open_Lst_Ach("FF","");
    
    Sys.Keys("[Ins]");
      
    //  On attend que la fiche s'ouvre
    var<TWinTST> mafiche = F_Fiche("TFch_PCF_Facture*", "Facture fournisseur");
    
    mafiche.VCLObject("Panel_CodeDocument").VCLObject("panEnteteInfos").VCLObject("panEnteteTop").VCLObject("panEnTeteCenter").VCLObject("panEnTeteTiers").VCLObject("panSaisCodeTiers").VCLObject("SaisCodeTiers").Keys("F1[Tab]");
    
    // On va sur les lignes
    Sys.Keys("~l");
    delay(1000);
    
    // On clique sur int�gration ligne
    Sys.Keys("~g");
    delay(1000);
    
    // La fen�tre de pointage s'ouvre
    mafiche = F_Fiche("TFrmIntegrationLignesPC", "Pointage des lignes");
    
    // On fait 3 tabulation pour se positionner sur la colonne S de la 1er ligne
    for(int i=0;i<5; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    
    // On selectionne tout
    Sys.Keys(" ");
    delay(50);
    
    // On fait F2 et on confirme
    Sys.Keys("[F2]");
    delay(50);
    
    var<TWinTST> monmessage = F_Message("TApiMessageDlg", "Pointage des lignes");
    F_Message_BtClic(monmessage,"Oui");
    
    mafiche.WaitProperty("Visible",false);
        
    // On valide le document
    Sys.Keys("[F2]");
    delay(500);
    
    P_CloseAllForm();
              
}
//****** END OF FILE ***************************************************************