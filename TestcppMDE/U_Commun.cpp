
#include "_common.h"

#include "JavaScriptExtens.cpp"  // for toStr...
//USEUNIT JavaScriptExtens

// D�claration des Globales
var<TStringTST> G_Rep_Snapshot;
var<TStringTST> G_Rep_CmdSql;
var<TStringTST> G_Rep_Temp;
var<TStringTST> G_Rep_Oracles;
var<TStringTST> G_Rep_Oracles_Reference;
var<TStringTST> G_Rep_Oracles_Test;
var<TStringTST> G_Instance_Sql;
var<TStringTST> G_DataBase;
var<TStringTST> G_User_Sql;
var<TStringTST> G_UserPasseword_Sql;
var<TStringTST> G_Test_Name;
varI<bool> G_P_Init_Automate;
varI<int> G_Temps_Debut;
varI<int> G_Temps_Fin;
varI<bool> G_Restart;
varI<bool> G_IsError;
var<TStringTST>  G_Last_TestNameCount;

//===========================================================================================
// 0 = Conception de cas (Pas de fermeture d'application / pas de Restauration de snaphot...);
// 1 = Ex�cution de l'automate pour g�n�rer les Oracles de r�f�rences 
// 2 = Ex�cution de l'automate pour g�n�rer les Oracles de test + Lancement de la comparaison
//-------------------------------------------------------------------------------------------
varI<int> G_Mode_Automate = 2;
//===========================================================================================

//------------------------------------------------------------------------------- 
// Initialisation des Globales
//-------------------------------------------------------------------------------
void P_Init_Globale() {       
    
    G_DataBase = "MDE_DOS_TEST";                                        // Nom de la base
    G_Rep_Snapshot = ProjectSuite.Path + "Snapshot\\";                  // R�pertoire des Snapshots
    G_Rep_CmdSql = ProjectSuite.Path + "SQL\\";                         // R�pertoire des fichiers SQL
    G_Rep_Temp = ProjectSuite.Path + "Temp\\";                          // R�pertoire Temporaire
    G_Rep_Oracles = ProjectSuite.Path + "Oracles\\";                    // R�pertoire Oracles
    G_Rep_Oracles_Reference = G_Rep_Oracles + "Version_Reference\\";    // R�pertoire des Oracles de R�f�rence
    G_Rep_Oracles_Test = G_Rep_Oracles + "Version_Test\\";              // R�pertoire des Oracles de Test
    G_Instance_Sql = "(local)";                                         // Nom de l'instance SQL
    G_User_Sql = "sa";                                                  // User SQL serveur
    G_UserPasseword_Sql = "sage";                                       // Mot de passe SQL serveur
    G_P_Init_Automate = false;                  // Flag permettant de savoir si on a fait P_Init_Automate
    G_Restart = true;                           // Flag permettant de savoir si on doit faire les traitements de l'event OnLogError
    G_IsError = false;                          // Flag permettant de savoir si on a pris une erreur 
}


//------------------------------------------------------------------------------- 
// T�ches � la fin de l'automate (Synchronisation date syst);
//-------------------------------------------------------------------------------
void P_Fin_Automate() {       
    
    P_Sync_DateHours_System();
}

//------------------------------------------------------------------------------- 
// Premier lancement de l'application pour versionning
//-------------------------------------------------------------------------------
void P_Versionning() {       
    
    if( G_Mode_Automate != 0 ) {
    
      P_FermeApp();
      // Lance l'application si elle n'est pas lanc�e
      if( ! Sys.WaitProcess("mde").Exists ) {
          TestedApps.mde.Run(1, true);

          //En cas de versionning MDE_COMM une fen�tre apparait, il faut attendre qu'elle se ferme
          while( Sys.WaitProcess("mde").WaitWindow("TFrmWaiting", "", -1, 1000).Exists ) {
              aqUtils.Delay(10000);
          }

          //En cas de versionning MDE_MDE une fen�tre apparait, il faut attendre qu'elle se ferme
          while(  Sys.WaitProcess("mde").WaitWindow("TFrmWaiting", "", -1, 1000).Exists ) {
              aqUtils.Delay(10000);
          }
        
          var<TWinTST> loginWin = Sys.Process("mde").WaitWindow("TFormLogin", "Identification", -1, 30000);
          Sys.Keys("[F2]");
          //En cas de versionning MDE_DOS une fen�tre apparait, il faut attendre qu'elle se ferme
          while(  Sys.WaitProcess("mde").WaitWindow("TFrmWaiting", "", -1, 1000).Exists ) {
              aqUtils.Delay(10000);
          }
        
          // On attend que la barre d'ic�ne soit enable (preuve que le dossier est ouvert);
          while(  Sys.Process("mde").VCLObject("fmMenu").Window("TStdToolbar", "", 1).Enabled == false ) {
          }
      }
    }
}

//------------------------------------------------------------------------------- 
// Lancement de l'application
//-------------------------------------------------------------------------------
void P_LanceApp() {       

    // Lance l'application si elle n'est pas lanc�e
    if( ! Sys.WaitProcess("mde").Exists ) {
        TestedApps.mde.Run(1, true);
        //Saisie des login et mot de passe
        var<TWinTST> login = Sys.Process("mde").WaitWindow("TFormLogin", "Identification", -1, 30000);
        login.Keys("[F2]");
        varI<int> Nb_Tentative = 0;
        varI<int> Nb_Tentative_Max = 5;
        
        var<TWinTST> Menu = Sys.Process("mde").WaitVCLObject("fmMenu", 30000);
        while(  Menu.Exists == false && Nb_Tentative < Nb_Tentative_Max ) {
            Nb_Tentative = Nb_Tentative + 1;        
            P_FermeApp();
            P_LanceApp();
            Menu = Sys.Process("mde").WaitVCLObject("fmMenu", 30000);
        }
        
        Menu.WaitProperty("Visible", true, 30000);
        Menu.WaitProperty("Enabled", true, 30000);
        var<TWinTST> Barre_Icone = Menu.WaitVCLObject("BarreIcones", 30000);
        Barre_Icone.WaitProperty("Visible", true, 30000);
        Barre_Icone.WaitProperty("Enabled", true, 30000);
    }

}

//------------------------------------------------------------------------------- 
// Fermeture de l'application
//-------------------------------------------------------------------------------
void P_FermeApp() { 
    
    // Initialise l'objet process
    var<TProcessTST> mde = Sys.WaitProcess("mde", 1000);
    // V�rifie si le process existe et le ferme
    if( mde.Exists ) {
        mde.Terminate();
    }
    
    // On attend que le process soit bien termin� 
    while( mde.Exists) {    
    }
    
}

//------------------------------------------------------------------------------- 
// Restauration de snapshot 
//-------------------------------------------------------------------------------
void P_Restore_Snapshot() {

    if (!F_Find_File(G_Rep_Snapshot + "MDE_DOS_TEST.ss")) {
        Log.Error("Le snapshot " + toStr(Instance) + " n'est pas pr�sent");
        return;
    }
    else {
        //Restauration du Snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Restauration_MDE_DOS.sql";
        P_Exec_Commande(machaine);
    }

    if (!F_Find_File(G_Rep_Snapshot + "sysdb.ss")) {
        Log.Error("Le snapshot " + toStr(Instance) + " n'est pas pr�sent");
        return;
    }
    else {
        //Restauration du Snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Restauration_sysdb.sql";
        P_Exec_Commande(machaine);
    }

}

//------------------------------------------------------------------------------- 
// T�ches au d�but d'un cas de test 
//-------------------------------------------------------------------------------
void P_Debut() {        
    
    // Pour ne pas logger les ev�nements win dans le rapport de testcomplete
    // Car pinalise les pref et mange de la m�moire.
    Log.LockEvents(0);
    // Sur le d�marrage d'un nouveau test on r�initialise la Globale G_IsError 
    G_IsError = false;
    
    G_Temps_Debut = 0;
    G_Temps_Fin = 0;
        
    //Par s�curit�, si G_Restart n'est pas repass�e � true (genre un oubli...) on le remet � true 
    //sur cette proc�dure qui initialise un nouveau cas de test 
    G_Restart = true;
    
    switch(G_Mode_Automate) {
        
        case 0:
        break;

        case 1: 
            P_FermeApp();
            P_Restore_Snapshot();
            //Log.message "Restauration Snapshot : " & Timer
            P_LanceApp();
            //Log.message "Lancement App : " & Timer
        break;        
        case 2:
            P_FermeApp();
            P_Restore_Snapshot();
            P_LanceApp();

        default:
            Log.Error ("Le Mode" + toStr(G_Mode_Automate) + " de l'automate est inconnu.");
        break;
    }//endswitch
}

//------------------------------------------------------------------------------- 
// Retourne le nom et le count du dernier test qui sera � r�aliser pour un Item 
// donn�.
//-------------------------------------------------------------------------------
TStringTST F_Last_TestNameCountToRunInItem( TTestItemTST ti) {
    // NOTE: la fonction est Recursive

    // On regarde si une proc�dure de test est rattachie et que l'item est coch�
    if (!ti.ElementToBeRun.isNothing && ti.Enabled == true) {
        G_Last_TestNameCount = ti.Name + " - " + toStr(ti.Count);
    }

    // Un item peut �tre un groupe. Il faut alors regarder les item enfants
    for (int i = 0; i<ti.ItemCount; i++) {
        // On ne regarde que si le groupe est actif 
        if (ti.Enabled) {
            G_Last_TestNameCount = F_Last_TestNameCountToRunInItem(ti.TestItem(i));
        }
    }

    return G_Last_TestNameCount;
}


//------------------------------------------------------------------------------- 
// Retourne le nom et le count du dernier test qui sera � r�aliser sur le projet 
//-------------------------------------------------------------------------------
TStringTST F_Get_Last_TestNameCount() {

    for (int i = 0; i<Project.TestItems.ItemCount; i++) {
        G_Last_TestNameCount = F_Last_TestNameCountToRunInItem(Project.TestItems.TestItem(i));
    }

    return G_Last_TestNameCount;
}

//------------------------------------------------------------------------------- 
// Proc�dure permettant de cr�er le fichier SQL qui sera exploit� pour faire 
// la diff de donn�es entre la base et son Snapshot 
//-------------------------------------------------------------------------------
void P_Get_File_Sql_Compare() {

    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Get_File_Sql_Compare.sql -v V_Rep_Temp = " + """'" + G_Rep_Temp + "'""";
    P_Exec_Commande(Machaine);
}


//------------------------------------------------------------------------------- 
// Proc�dure permettant d'ex�cuter le fichier de comparaison des datas 
// la diff de donn�es entre la base et son Snapshot 
//-------------------------------------------------------------------------------
void P_Exec_File_Sql_Compare() {

    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_Temp + "File_Sql_Compare.sql" + " -o " + G_Rep_Oracles + G_Test_Name + ".txt";
    P_Exec_Commande(Machaine);
}


//------------------------------------------------------------------------------- 
// Proc�dure permettant de comparer les oracles. 
// La m�thode Files.Compare permet de savoir s'il y a une <> entre 2 fichiers
// Dans le cas ou on ditecte une <> fait en plus un Check pour avoir dans le 
// rapport un lien permettant de visualiser les <>. 
// Le "check" est plus lent que le "compare", c'est pour cela que l'on fait 
// Un compare d'abord.  
// ATTENTION !!! Le check necessite d'avoir un objet de type File dans le Store
// Et d'ajouter le fichier de r�f�rence en vue de la comparaison.
// Avant d'ajouter ce fichier il convient de v�rifier qu'un fichier de m�me nom 
// n'est pas d�j� pr�sent. 
//-------------------------------------------------------------------------------
void P_Compare_File() {

    //Si on n'a pas de fichiers de r�f�rence on ne peut pas faire de comparaison
    if (F_Find_File(G_Rep_Oracles_Reference + G_Test_Name + ".txt")) {

        varI<bool> V_Verif_Test = Files.Compare(G_Rep_Oracles_Reference + G_Test_Name + ".txt", G_Rep_Oracles_Test + G_Test_Name + ".txt", 0, false);
        // Il y a une <> entre les fichiers
        if (V_Verif_Test == false) {

            //Si le fichier est d�j� pr�sent dans le store il faut le supprimer
            if (Files.Contains("Oracle_Ref")) {
                //On supprime le fichier
                Files.Remove("Oracle_Ref");
            }

            // On ajoute le nouveau fichier
            if (!Files.Add(G_Rep_Oracles_Reference + G_Test_Name + ".txt", "Oracle_Ref")) {
                Log.Warning("L'Oracle de r�f�rence n'a pas �t� ajout�");
            }

            //On va faire le check du fichier pour l'avoir dans le rapport, mais cela remonte obligatoirement une
            //erreur et on ne veut pas ex�cuter les traitements pr�sents dans OnLogError donc on passe
            //G_Restart = false et on le r�active juste aprhs
            G_Restart = false;
            Files.Items("Oracle_Ref").Check(G_Rep_Oracles_Test + G_Test_Name + ".txt");
            G_Restart = true;
        }
    }
    else {
        Log.Warning("Le fichier de r�f�rence n'est pas pr�sent");
    }


}


//------------------------------------------------------------------------------- 
// T�ches � la fin d'un cas de test 
//-------------------------------------------------------------------------------
void P_Fin() {       
       
    switch(G_Mode_Automate) {
        
        case 0:
        break;    

        case 1: {
            //Ajouter suppression de l'oracle de R�f�rence
            P_Del_File(G_Rep_Oracles_Reference + G_Test_Name + ".txt");
            // Dans le cas ou on a pris une erreur ou une fen�tre inattendue on utilise la m�thode
            // Runner.Stop dans les events. Cette m�thode entraine le d�clenchement de OnStopTest et
            // et dans OnStopTest on appel P_Fin. Il ne faut pas ex�cuter tous les traitements de P_Fin
            // Si on a pris une erreur. D'ou l'utilit� de la Globale G_IsError qui permet d'identifier un
            // Contexte d'erreur.             
            if( G_IsError == false ) {
                P_CloseAllForm();
                P_FermeApp();
                P_Get_File_Sql_Compare();
                P_Exec_File_Sql_Compare();
                G_Temps_Fin = Timer;
            }
            
            // Pour savoir si on est sur le dernier cas de test on va comparer le nom et l'it�ration de
            // la proc�dure de test courante avec le nom et le nombre d'it�ration � r�aliser du dernier cas de test
            var<TStringTST> V_CurrentTest_NameIteration = Project.TestItems.Current.Name + " - " + toStr(Project.TestItems.Current.Iteration); 
            var<TStringTST> V_LastTest_NameCount = F_Get_Last_TestNameCount();
            if( V_CurrentTest_NameIteration == V_LastTest_NameCount ) {
                P_Fin_Automate();
            }            
        }break;
        
        case 2: {
            // Si l'oracle de r�f�rence n'est pas pr�sent, inutile de faire le test
            if( ! F_Find_File(G_Rep_Oracles_Reference + G_Test_Name + ".txt") ) {
                Log.Error( "L'oracle de r�f�rence n'est pas pr�sent (" + G_Rep_Oracles_Reference + G_Test_Name + ".txt)."); 
            } 
            
            // Dans le cas ou on a pris une erreur ou une fen�tre inattendue on utilise la m�thode
            // Runner.Stop dans les events. Cette m�thode entraine le d�clenchement de OnStopTest et
            // et dans OnStopTest on appel P_Fin. Il ne faut pas ex�cuter tous les traitements de P_Fin
            // Si on a pris une erreur. D'ou l'utilit� de la Globale G_IsError qui permet d'identifier un
            // Contexte d'erreur.             
            if( G_IsError == false ) {
                P_CloseAllForm();
                P_FermeApp();
                P_Get_File_Sql_Compare();
                P_Exec_File_Sql_Compare();
                P_Compare_File();
                G_Temps_Fin = Timer;
            }
            
            // Pour savoir si on est sur le dernier cas de test on va comparer le nom et l'it�ration de
            // la proc�dure de test courante avec le nom et le nombre d'it�ration � r�aliser du dernier cas de test
            var<TStringTST> V_CurrentTest_NameIteration = Project.TestItems.Current.Name + " - " + toStr(Project.TestItems.Current.Iteration);
            var<TStringTST> V_LastTest_NameCount = F_Get_Last_TestNameCount();
            if( V_CurrentTest_NameIteration == V_LastTest_NameCount ) {
                P_Fin_Automate();
            }
        }break;       
        default:
            Log.Error("Le Mode" + toStr(G_Mode_Automate) + " de l'automate est inconnu.");
    }//end switch

}

//------------------------------------------------------------------------------- 
// Fermeture de toutes les fen�tres de l'application 
//-------------------------------------------------------------------------------
void P_CloseAllForm() {       

    // on error resume next
    
    if( Sys.WaitProcess("mde", 1000).Exists ) {
    
        var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
        // On envoie une s�rie de �chap pour tenter de tout fermer
        tfmMenu.Keys("[Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc]");
        // Clic sur l'option Fen�tre | Tout Fermer
        tfmMenu.MainMenu.Click("[13]|[4]");
        // On envoie une s�rie de �chap pour tenter de tout fermer
        tfmMenu.Keys("[Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc][Esc]");
    }
    
    if( err.number != 0 ) {
        P_FermeApp();
    }
    
    // On Error goto 0   

}

//------------------------------------------------------------------------------- 
// Cr�ation de snapshot 
//-------------------------------------------------------------------------------
void P_Create_snapshot() { 

    if( ! F_Find_File(G_Rep_Snapshot + "MDE_DOS_TEST.ss") ) {
        //log.message "Le Snapshot n'est pas pr�sent, il faut le cr�er"        
        
        //Cr�ation du snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Creation_MDE_DOS.sql" + " -v param1 = " + """'" + G_Rep_Snapshot + "'""";       
        P_Exec_Commande( machaine);
    }else{
        //log.message("Le snapshot existe d�j�");
    }
    
    if( ! F_Find_File(G_Rep_Snapshot + "sysdb.ss") ) {
        //log.message "Le Snapshot n'est pas pr�sent, il faut le cr�er"        
        
        //Cr�ation du snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Creation_sysdb.sql" + " -v param1 = " + """'" + G_Rep_Snapshot + "'""";
        P_Exec_Commande(machaine);
    }else{
        //log.message("Le snapshot existe d�j�");
    }
    
}


//------------------------------------------------------------------------------- 
// Suppression de snapshot 
//-------------------------------------------------------------------------------
void P_Delete_Snapshot() { 
    
    // Si un snapshot est pr�sent on le supprime
    if( F_Find_File(G_Rep_Snapshot + "MDE_DOS_TEST.ss") ) {
        //Suppression du Snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Suppression_MDE_DOS.sql";
        P_Exec_Commande(machaine);
    }
    
    // Si un snapshot est pr�sent on le supprime
    if( F_Find_File(G_Rep_Snapshot + "sysdb.ss")  ) {
        //Suppression du Snapshot
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "SNAPSHOT_Suppression_sysdb.sql";
        P_Exec_Commande(machaine);
    }
 
}

//------------------------------------------------------------------------------- 
// Proc�dure permetant de mettre � jour les stat de SQL serveur
// A faire avant cr�ation du snapshot utilis� pour la comparaison.
// Si les stats ne sont pas � jour la comparaison des data sera beaucoup plus 
// longue. 
//-------------------------------------------------------------------------------
void P_MAJ_Stat_SQLSERVER() { 
    
    //si on trouve d�j� un snapshot pas besoin de faire toutes ces manip
    if( ! F_Find_File(G_Rep_Snapshot + "MDE_DOS_TEST.ss") ) {
        //log.message "Le Snapshot n'est pas pr�sent, il faut le cr�er"        
        
        // S'il n'est pas pr�sent, les stat de la base ne sont peut �tre pas � jour.
        // Pour avoir les stat � jour il faut jouer le m�me type de requ�te que celle qui sera exploit�e
        // lors de la comparaison. Donc on va faire �a. PB il nous faut un snapshot pour jouer
        // cette requ�te. Donc on va :
        //       - Cr�er un snapshot de la base qui n'a peut �tre pas les stats � jour
        //       - G�n�rer la requ�te de comparaison 
        //       - Ex�cuter la requ�te de comparaison pour alimenter les stats
        //       - Supprimer le snapshot courant   
        
        P_Create_snapshot();
        // G�n�ration du fichier
        var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "P_MAJ_Stat_SQLSERVER.sql -v G_Rep_Temp = " + """'" + G_Rep_Temp + "'""";
        P_Exec_Commande(machaine);
        //Execution
        P_Exec_File_Sql_Compare();
        P_Delete_Snapshot();
    }

}

//------------------------------------------------------------------------------- 
// R�initialisation des s�quences 
//-------------------------------------------------------------------------------
void P_Reinit_Seq() { 
    
    //Restauration du Snapshot
    var<TStringTST> machaine = "sqlcmd -S " + G_Instance_Sql + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Reinit_seq.sql";
    P_Exec_Commande(machaine);
}

//------------------------------------------------------------------------------- 
// Fonction permettant d'initialiser l'objet fmMenu
//-------------------------------------------------------------------------------
TWinTST F_Menu() {

    var<TWinTST> menuWin = Sys.Process("mde").VCLObject("fmMenu");
    if (!menuWin.Exists) {
        Log.Error("L'objet menu n'a pas �t� charg�");
    }
    return menuWin;
}

//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un menu
//-------------------------------------------------------------------------------
void F_Menu_Click( TStringTST CheminOption) {
    //Index de l'option du menu
    var<TStringTST> OptionClick = "";
    
    //Dans un 1er temps on va initialiser OptionClick � la main avec des if
    // !!!!!!!!!!!!!!!!!!!! Respecter l'ordre des menus !!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
    if( CheminOption == "Initialisation|El�ments|Biblioth�ques" ) { OptionClick = "[1]|[2]|[0]"; }
    if( CheminOption == "Initialisation|El�ments|Unit�s de mesure" ) { OptionClick = "[1]|[2]|[3]"; }
    if( CheminOption == "Initialisation|El�ments|Natures d'�l�ments" ) { OptionClick = "[1]|[2]|[2]"; } 
    if( CheminOption == "Initialisation|El�ments|Bordereaux de prix" ) { OptionClick = "[1]|[2]|[4]"; }
    if( CheminOption == "Initialisation|El�ments|M�tr�s-types" ) { OptionClick = "[1]|[2]|[5]"; }
    if( CheminOption == "Initialisation|El�ments|Textes standards" ) { OptionClick = "[1]|[2]|[7]"; }
    
    if( CheminOption == "Initialisation|Tiers|Familles|Familles Clients" ) { OptionClick = "[1]|[5]|[3]|[0]"; }
    if( CheminOption == "Initialisation|Tiers|Familles|Familles Fournisseurs" ) { OptionClick = "[1]|[5]|[3]|[1]"; }
    if( CheminOption == "Initialisation|Tiers|Familles|Familles Sous-traitants" ) { OptionClick = "[1]|[5]|[3]|[2]"; }
    if( CheminOption == "Initialisation|Secteurs g�ographiques" ) { OptionClick = "[1]|[6]|"; }
    if( CheminOption == "Initialisation|Affaires|Types d'actions commerciales" ) { OptionClick = "[1]|[8]|[2]|"; }
    if( CheminOption == "Initialisation|Affaires|Calendriers-types" ) { OptionClick = "[1]|[8]|[3]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Devis Clients" ) { OptionClick = "[1]|[11]|[1]|[0]|[0]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Commandes Clients" ) { OptionClick = "[1]|[11]|[1]|[0]|[1]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Bons de livraisons Clients" ) { OptionClick = "[1]|[11]|[1]|[0]|[2]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Bons de retour Clients" ) { OptionClick = "[1]|[11]|[1]|[0]|[3]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Situations de travaux" ) { OptionClick = "[1]|[11]|[1]|[0]|[4]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Clients|Groupes Factures Clients" ) { OptionClick = "[1]|[11]|[1]|[0]|[5]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Demandes de prix Fournisseurs" ) { OptionClick = "[1]|[11]|[1]|[1]|[0]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Commandes Fournisseurs" ) { OptionClick = "[1]|[11]|[1]|[1]|[1]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Bons de r�ception Fournisseurs" ) { OptionClick = "[1]|[11]|[1]|[1]|[2]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Bons de retour Fournisseurs" ) { OptionClick = "[1]|[11]|[1]|[1]|[3]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Fournisseurs|Groupes Factures Fournisseurs" ) { OptionClick = "[1]|[11]|[1]|[1]|[4]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Appels d'offre Sous-traitants" ) { OptionClick = "[1]|[11]|[1]|[2]|[0]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Commandes Sous-traitants" ) { OptionClick = "[1]|[11]|[1]|[2]|[1]|"; }
    if( CheminOption == "Initialisation|Documents|Groupes de documents|Documents Sous-traitants|Groupes Factures Sous-traitants" ) { OptionClick = "[1]|[11]|[1]|[2]|[2]|"; }
    if( CheminOption == "Initialisation|Documents|Conditions de r�glement" ) { OptionClick = "[1]|[11]|[6]|"; }
    if( CheminOption == "Initialisation|Chantiers|Postes de travaux" ) { OptionClick = "[1]|[14]|[3]|"; } 
    if( CheminOption == "Initialisation|Comptabiliti|Plan comptable g�n�ral" ) { OptionClick = "[1]|[17]|[1]|"; }
    if( CheminOption == "Initialisation|D�p�ts" ) { OptionClick = "[1]|[15]|"; } 
    if( CheminOption == "Initialisation|Lettres-types" ) { OptionClick = "[1]|[12]|"; } 
    if( CheminOption == "Tiers|Clients/Ma�tres d'ouvrage" ) { OptionClick = "[4]|[3]"; }
    if( CheminOption == "Biblioth�ques|Fabricants" ) { OptionClick = "[2]|[1]|"; } 
    //S�parateur
    if( CheminOption == "Tiers|Fournisseurs" ) { OptionClick = "[4]|[5]"; }
    if( CheminOption == "Tiers|Sous-traitants" ) { OptionClick = "[4]|[6]"; }
    if( CheminOption == "Ventes|Devis Quantitatifs et estimatifs" ) { OptionClick = "[6]|[0]"; }
    
    if( F_Menu().Focused == false ) F_Menu().SetFocus();
    
    //
    if( OptionClick != "" ) { 
        F_Menu().MainMenu.Click(OptionClick);
    }else{
        Log.Error("Le menu n'a pas �t� trouv�");
    }     

}


//------------------------------------------------------------------------------- 
// Fonction permettant d'initialiser la fen�tre principale de l'application
//-------------------------------------------------------------------------------
TWinTST F_Principale() {

    var<TWinTST> PrincipaleWin = Sys.Process("mde").VCLObject("fmMenu").VCLObject("ClientControl");
    if( !PrincipaleWin.Exists ) {
        Log.Error("La fen�tre principale n'a pas �t� charg�e");
    }
    
    if( ! PrincipaleWin.VisibleOnScreen ) {
        PrincipaleWin.SetFocus();
    }
    return PrincipaleWin;
} 
//------------------------------------------------------------------------------- 
// Proc�dure permetant d'activer les composants d'OLE automation sur SQL Server 
//-------------------------------------------------------------------------------
void P_Active_composants_Ole_Automation() {

    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Active_composants_Ole_Automation.sql";
    P_Exec_Commande(Machaine);
}

//------------------------------------------------------------------------------- 
// Proc�dure permetant d'activer les composants xp_cmdshell 
//-------------------------------------------------------------------------------
void P_Active_composants_xp_cmdshell() {

    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Active_composants_xp_cmdshell.sql";
    P_Exec_Commande(Machaine);
}

//------------------------------------------------------------------------------- 
// Livre des proc�dures Sql Server permettant de cr�er et d'�crire dans un fichier 
//-------------------------------------------------------------------------------
void P_Creat_Procedure_Automation_File() {

    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Creat_Procedure_Automation_File.sql";
    P_Exec_Commande(Machaine);
}

//------------------------------------------------------------------------------- 
// G�n�re le fichier Txt contenant la liste des champs pouvant faire l'objet 
// d'une comparaison.

// But : Entre 2 versions des tables ou des champs peuvent avoir �t� ajouter. Il ne faut pas 
// retrouver dans les Oracles des <> li�s � ces ajouts. 
//
// Dans le mode 1 de l'automate, on va donc  g�n�rer un fichier contenant la liste des tables 
// et champs pr�sents.
//
// Dans le mode 2 de l'automate on va au d�but de l'automates, on va :
//	1 - Cr�er une table qui contiendra la liste des tables et champs de la Version N-1
//	2 - Importer dans cette table les donn�es contenu dans le fichier g�n�r� pr�c�demment
//	3 - Tenir compte de ces Tables et champs dans la requete qui permet de faire ressortir les
// data impl�ment�es par nos cas de tests.
//-------------------------------------------------------------------------------
void P_Export_TablesField_List() {

    P_Init_Globale();
    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Export_TablesField_List.sql -v v_path = " + """'" + G_Rep_Oracles_Reference + "TablesField_List.txt'""";
    P_Exec_Commande(Machaine);
}

//------------------------------------------------------------------------------- 
// G�n�re le fichier Txt contenant la liste des champs pouvant faire l'objet 
// d'une comparaison.

// But : Entre 2 versions des tables ou des champs peuvent avoir �t� ajouter. Il ne faut pas 
// retrouver dans les Oracles des <> li�s � ces ajouts. 
//
// Dans le mode 1 de l'automate, on va donc  g�n�rer un fichier contenant la liste des tables 
// et champs pr�sents.
//
// Dans le mode 2 de l'automate on va au d�but de l'automates, on va :
//	1 - Cr�er une table qui contiendra la liste des tables et champs de la Version N-1
//	2 - Importer dans cette table les donn�es contenu dans le fichier g�n�r� pr�c�demment
//	3 - Tenir compte de ces Tables et champs dans la requete qui permet de faire ressortir les
// data impl�ment�es par nos cas de tests.
//-------------------------------------------------------------------------------
void P_Creat_TablesField_List() {

    P_Init_Globale();
    var<TStringTST> Machaine = "sqlcmd -S " + G_Instance_Sql + " -d " + G_DataBase + " -U " + G_User_Sql + " -P " + G_UserPasseword_Sql + " -i " + G_Rep_CmdSql + "Creat_TablesField_List.sql -v v_path = " + """'" + G_Rep_Oracles_Reference + "TablesField_List.txt'""";
    /// Sys.Clipboard = Machaine;  // AJO: Curious !! => supprim�
    P_Exec_Commande(Machaine);
}


//------------------------------------------------------------------------------- 
// T�ches en initialisation de l'automate (Modification date syst, Snapshot ... );
//-------------------------------------------------------------------------------
void P_Init_Automate() {

    //Pour ne pas logger les ev�nements win dans le rapport de testcomplete
    //Car p�nalise les pref et mange de la m�moire.
    Log.LockEvents(0);
    //Initialisation des globales
    P_Init_Globale();
    if (G_Mode_Automate != 0) {

        P_MAJ_DateHours_System("01-01-16", "00:00:00");
        P_Reinit_Seq();
        P_Versionning();
        P_FermeApp();
        P_Active_composants_Ole_Automation();
        P_Active_composants_xp_cmdshell();
        P_Creat_Procedure_Automation_File();
        if (G_Mode_Automate == 1) {
            P_Export_TablesField_List();
        }
        P_Creat_TablesField_List();
        P_MAJ_Stat_SQLSERVER();
        P_Create_snapshot();
    }

    G_P_Init_Automate = true;

}


//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un bouton d'une liste
// ObjListe soit �tre d�fini par une F_Liste
// Btaction : Quitter/Cr�er/Modifier/Supprimer/Copier/Historique/Imprimer...
//-------------------------------------------------------------------------------
void F_Liste_BtClic( TWinTST objListe, TStringTST _Btaction) {
    var<TWinTST> ListBoutonsToolBar = objListe.VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    
    var<TStringTST> Btaction = _Btaction;

    if( Btaction == "Quitter" ) { 
         Btaction = "IDB_BTANNULER";
    }else if( Btaction == "Cr�er" ) {
         Btaction = "IDB_BTCREER";
    }else if( Btaction == "Modifier" ) {
         Btaction = "IDB_BTMODIF";
    }else if (Btaction == "Supprimer" ) {
         Btaction = "IDB_BTSUPPR";
    }else if (Btaction == "Copier" ) {
         Btaction = "IDB_BTCOPIER";
    }else if (Btaction == "Historique" ) {
         Btaction = "IDB_BTHISTO";
    }else if (Btaction == "Imprimer" ) {
         Btaction = "IDB_BTIMPRIMANTE";
    }else{
         Log.Error("L'action du bouton n'est pas d�finie.");
    }
    
    var<TWinTST> Bt = ListBoutonsToolBar.FindChild(Array("Params"),Array("*" + Btaction + "*"));
    Bt.Click();
} 

//------------------------------------------------------------------------------- 
// Fonction permettant de  :
//   - donner le focus � la zone de recherche
//   - Saisir les caract�res servant � filtrer
//   - Mettre un delay pour le temps du filtre
//-------------------------------------------------------------------------------
void F_Liste_Filtre( TWinTST objListe, TCodeTST _filtre ) {

    F_Liste_Verif_Filtre(objListe);

    // Initialisation de l'objet
    var<TWinTST> zone_rch = objListe.FindChild("Name", "*saiQuickSearch*", 2);
    if( ! zone_rch.Exists ) { 
        Log.Error("Zone de recherche non trouv�e");
    }
    
    //On saisit les caract�res
    zone_rch.Keys(_filtre.toString() );
    //On donne un delai
    delay(1000);
}

//------------------------------------------------------------------------------- 
// Proc�dure permettant de  :
//   - V�rifier que la zone de recherche est positionn�e sur la bonne colonne
//   - Modifie le colonnage le cas �ch�ant
//   - Retour le nom du champs de recherche
//-------------------------------------------------------------------------------
TStringTST F_Liste_Verif_Filtre(TWinTST objListe ) {
    
    // Initialisation de l'objet
    var<TWinTST> Label_zone_rch = objListe.FindChild("Name", "*txtQuickSearch*", 2);
    if( ! Label_zone_rch.Exists ) { 
        Log.Error("Le label de la zone de recherche non trouv�");
    }

    // Initialisation du Booleen indiquant si le filtre de la liste est OK 
    varI<bool> Bok = (Label_zone_rch.Caption == "Code") || (Label_zone_rch.Caption == "R�f�rence");

    // Si Bok = 0 la liste n'a pas le bon tri, il faut le changer
    if( Bok == false ) {
      var<TWinTST> stdScroll = objListe.VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
      stdScroll.ClickR(-1, -1);
      stdScroll.PopupMenu.Click("Gestion des vues");
      var<TWinTST> stdPanel = F_Principale().WaitWindow("TViewsListDlg", "Vues", -1, 1000).VCLObject("StdPanel");
      stdPanel.VCLObject("btnModify").Click(-1, -1);
      var<TWinTST> viewDlg = F_Principale().WaitWindow("TViewDlg", "Param�trage d'une vue", -1, 1000);
      var<TWinTST> stdSais = viewDlg.FindChild("Name", "*saiOrderField1*", 5);
      stdSais.Keys("[F4]");
      Sys.Process("mde").Window("TFrmTables", "Champ de tris").VCLObject("StdPanel1").VCLObject("btOK").Click(-1, -1);
      viewDlg.VCLObject("StdPanel").VCLObject("BtnValider").Click(-1, -1);
      stdPanel.VCLObject("btnActivateView").Click(-1, -1);
    }
    
    // Initialisation de l'objet
    Label_zone_rch = objListe.FindChild("Name", "*txtQuickSearch*", 2);
    if( ! Label_zone_rch.Exists ) { 
        Log.Error("Le label de la zone de recherche non trouv�");
    }
    
    return Label_zone_rch.Caption;

}

//------------------------------------------------------------------------------- 
// Fonction permettant de v�rifier la pr�sence d'une liste et de l'initialiser 
// dans un objet  
//-------------------------------------------------------------------------------
TWinTST F_Liste( TStringTST NomListe) {
    var<TWinTST> ObjListe;
    // Dans le  cas ou on exploite le mapping (!!! Les noms doivent commencer par Map_ !!!);
    if( Left(NomListe,4) == "Map_" ) {
        ObjListe = F_Principale().WaitNamedChild(NomListe, 50000);
    }else{    
        // Initialisation de l'objet
        ObjListe = F_Principale().WaitWindow("TStdFormListe", "*" + NomListe + "*", -1, 50000);
    }
    
    if( !ObjListe.Exists ) {
        Log.Error("La liste n'est pas pr�sente");
    } 
    return ObjListe;
} 

//------------------------------------------------------------------------------- 
// Fonction permettant de v�rifier la pr�sence d'une fiche et de l'initialiser 
// dans un objet  
//-------------------------------------------------------------------------------
TWinTST F_Fiche( TStringTST ClasseFiche, TStringTST NomFiche) {
    //En fonction des captions les noms des listes et fiches pose PB
    //La classe g�n�rique des fiches est une Tfm donc si on a rien pr�cis� dans l'appel ("*");
    //On va remplacer ClasseFiche par "Tfm*"
    if( ClasseFiche == "*" ) {
        ClasseFiche = "Tfm*";
    }
    var<TWinTST> ObjFiche;

    if( Left(NomFiche,4) == "Map_" ) {
        ObjFiche = F_Principale().WaitNamedChild(NomFiche, 50000);
    }else{    
        ObjFiche = F_Principale().WaitWindow(ClasseFiche, "*" + NomFiche + "*", -1, 50000);
        //ObjFiche = F_Principale.ActiveWindow() //Todo : A voir avec Jam
    }     
    
    if( ! ObjFiche.Exists ) {
        Log.Error( "La fiche n'est pas pr�sente");
    }
    
    return ObjFiche;      
} 



//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un bouton d'une Fiche
// ObjFiche soit �tre d�fini par une F_Fiche
// Btaction : Quitter/Valider...
//-------------------------------------------------------------------------------
bool F_Fiche_BtClic( TWinTST objFiche, TStringTST  Btaction) {
    if( Btaction == "Quitter" ) {
        Btaction = "btQuitter";
    }else if( Btaction == "Valider" ) {
        Btaction = "btValider";
    }else if( Btaction == "Analytique" ) {
        Btaction = "btAnalytique";
    }else if( Btaction == "btAnalytiqueEnt" ) {
        Btaction = "btAnalytiqueEnt";
    }else if(Btaction == "btImportEltsPrevus" ) {
        Btaction = "btImportEltsPrevus";
    }else{
        Log.Error("L'action du bouton n'est pas d�finie.");    
    }
    
    var<TWinTST> Bt = objFiche.FindChild(Array("name"),Array("*" + Btaction + "*"),3);
    if( ! Bt.Exists ) {        
        Log.Error("Bouton non trouv�");
    }
    
    Bt.Click();
    return true;
} 


//------------------------------------------------------------------------------- 
// Fonction permettant de v�rifier la pr�sence d'un Message  et de l'initialiser 
// dans un objet  
//-------------------------------------------------------------------------------
TWinTST F_Message( TStringTST _winClass, TStringTST _titlePart ) {
    var<TStringTST> ClasseMessage = _winClass;
    if( ClasseMessage == "*" ) { ClasseMessage = "TApiMessageDlg"; } 
    
    var<TWinTST> ObjMessage = Sys.Process("mde").WaitWindow(ClasseMessage, "*" + _titlePart + "*", -1, 50000);
    if( ! ObjMessage.Exists ) {
        Log.Error( "Le message n'est pas pr�sent");
    }
    
    return ObjMessage;      
} 


//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un bouton d'un Message
// ObjMessage soit �tre d�fini par une F_Message
// Btaction : Quitter/Valider...
//-------------------------------------------------------------------------------
bool F_Message_BtClic(TWinTST objFiche, TStringTST Btaction) {
#if 0
    if( Btaction ==  "Oui" ) { 
         Btaction = "Oui";
    }else if (Btaction == "Non" ) {
         Btaction = "Non";
    }else if (Btaction == "Valider" ) {
         Btaction = "btValider";
    }else if (Btaction == "Confirmer" ) {
          Btaction = "Confirmer";
    }else if(Btaction == "Ok" ) {
            Btaction = "Ok";
    }else{
       Log.Error("L'action du bouton n'est pas d�finie.");
    }
    
    var<TWinTST> Bt = objFiche.FindChild(Array("Params"),Array("*" + Btaction + "*"),3);
    if( ! Bt.Exists ) {        
        Log.Error("Bouton non trouv�");
    }
    Bt.Click();
#endif
    return true;    
}

//------------------------------------------------------------------------------- 
// Fonction permettant de v�rifier la pr�sence d'un Apper�u avant impression
// et de l'initialiser dans un objet  
//-------------------------------------------------------------------------------
TWinTST F_Appercu(TStringTST ClasseAppercu ) {
    if( ClasseAppercu == "*" ) { ClasseAppercu = "EdtFrmScreenView"; }
    
    var<TWinTST> ObjAppercu = Sys.Process("mde").WaitVCLObject(ClasseAppercu, 50000);
    if( ! ObjAppercu.Exists ) {
        Log.Error( "L'apper�u n'est pas pr�sent" );
    }
    
    // On attend d'avoir toutes les pages.
    // Les boutons semblent ditruits lors de la construction de l'idition. Cela peut provoquer des erreurs
    // Lors de l'appel � F_Appercu_BtClic
    ObjAppercu.VCLObject("MainPanel").VCLObject("btLast").WaitProperty("Enabled", true, 500000);
    return ObjAppercu;      
} 


//------------------------------------------------------------------------------- 
// Fonction permettant de cliquer sur un bouton d'un Apper�u
// ObjAppergu soit �tre d�fini par une F_Appercu
// Btaction : Quitter/Valider...
//-------------------------------------------------------------------------------
bool F_Appercu_BtClic(TWinTST objFiche, TStringTST Btaction) {
#if 0
    if( Btaction == "Quitter" ) { 
       Btaction = "Quitter";
    }else{
       Log.Error("L'action du bouton n'est pas d�finie.");
    }
    
    var<TWinTST> Bt = objFiche.FindChild(Array("Params"), Array("*" + Btaction + "*"),3);
    if( ! Bt.Exists ) {        
        Log.Error("Bouton non trouv�");
    } 
    
    Bt.Click();
#endif
    return true;
}



//------------------------------------------------------------------------------- 
// Proc�dure permettant d'initialiser la variable globale G_Test_Name qui est 
// notamment exploit�e pour d�finir le nom de l'oracle 
//-------------------------------------------------------------------------------
void F_Get_Test_Name() {  

  var<TTestItemTST> Project_TestItems = Project.TestItems;

  if( Project_TestItems.Current.isNothing ) {
      G_Test_Name = "Test";
  }else{
      G_Test_Name = Project_TestItems.Current.ElementToBeRun.Caption;
      varArray<TStringTST> Tab_G_Test_Name = Split(G_Test_Name,"-");
      G_Test_Name = Trim(Tab_G_Test_Name[Tab_G_Test_Name.length-1]);
  }

}

//------------------------------------------------------------------------------- 
// Proc�dure permettant d'initialiser la variable globale G_Rep_Oracles qui  
// permet de d�finir le r�pertoire ou seront enregistr�s les Oracles 
//-------------------------------------------------------------------------------
void F_Init_Rep_Oracles() { 
    
    switch(G_Mode_Automate) {
        case 0:
        break;    
        case 1:  
            G_Rep_Oracles = G_Rep_Oracles_Reference;
        break;
        case 2:
            G_Rep_Oracles = G_Rep_Oracles_Test;
        break;
        default:
            Log.Error("Le mode de l'automate est incorrect.");
    }
}


//-------------------------------------------------------------------------------
varArray<TWinTST> F_Fiche_Creation_GetSaisies(TWinTST objFiche) {
    return new Array();

#if 0
    if( ! objFiche.Exists ) {
        Log.Error( "La fiche n'est pas pr�sente" );
    }

    //Renvoie la liste des saisies (non ordonn�e), de type TStdSais, enabled et sans caption
    varArray<TWinTST> TabSaisies = objFiche.FindAllChildren(Array("WndClass","Enabled","WndCaption"),Array("TStdSais","true",""),10);
    varI<bool> bok = false;

    //Ordonner le tableau (mais possible qu'il y ait des index non occupis, � cause des TabOrder);
    varArray<TWinTST> TabOrd = newArray();
    for(int i=0; i<TabSaisies.length; i++) {
        varI<int> newindex = TabSaisies[i].TabOrder + 1; 
        TabOrd[newindex] = TabSaisies[i];
    }
 
    // Ordonner le tableau et enlever les vides   
    varArray<TWinTST> NewTabOrd = newArray();
    varI<int> newindex= 0;
    for(int i=0; i<TabOrd.length; i++) {
        // Ici il faut sauter les �l�ments vides
        if( TabOrd[i].Exists ) {
            NewTabOrd[newindex] = TabOrd[i];
            newindex = newindex + 1;
        }
    }
    
    return NewTabOrd;
#endif
} 


//-------------------------------------------------------------------------------
void P_Fiche_Creation_Saisies(TWinTST Fiche) {

    varI<bool> Modification = false; // seems useless

    varArray<TWinTST> Saisies = F_Fiche_Creation_GetSaisies(Fiche);

    for(int i=0; i<Saisies.length; i++) {
        var<TStringTST> ParamsSaisie = Saisies[i].Params;
        var<TStringTST> ParamsDecode = F_Get_ParamsDecode(ParamsSaisie);
        var<TTypSaisie> TypeSaisie = F_Get_TypeSaisieParams(ParamsDecode);
        //Log.Message ParamsSaisie
        //Log.Message ParamsDecode
        
        if( ParamsSaisie == "@CP") {
             Saisies[i].Keys("33700");
        }else if(ParamsSaisie == "@VILLE" ) {
            //On laisse la valeur qui a du �tre initialisi avec le code postale
        }else{    
                if( Contains(ParamsDecode,"Rch") ) {
                    Saisies[i].Keys("[F4]");
                    Saisies[i].Keys("[Enter]");
                }else{
                    // Selon TypeSaisie
                    if( TypeSaisie == C_TypeSaisie_Nombre ) {
                            Saisies[i].Keys("50");
                    }else if( TypeSaisie == C_TypeSaisie_Alpha ) {
                            //todo : A corriger
                        if( Modification == false ) {
                            Saisies[i].Keys("Libell� Alpha");
                        }else{
                            Saisies[i].Keys("Libell� Alpha Modif");
                        }
                    }else if(TypeSaisie == C_TypeSaisie_Multi ) {
                            Saisies[i].Keys("[F4]");
                            Saisies[i].Keys("[Enter]");
                    }else if (TypeSaisie == C_TypeSaisie_Date ) {
                            Saisies[i].Keys("010115");
                    }else if (TypeSaisie == C_TypeSaisie_Inconnu ) {
                            Log.Warning( "Type de saisie inconnue");
                    }
                }//endelse
            
        }//endelse
    }
}

//-------------------------------------------------------------------------------
varArray<TWinTST> F_Fiche_Modif_GetSaisies(TWinTST objFiche) {
  return new Array();
#if 0
    if( ! objFiche.Exists ) {
        Log.Error( "La fiche n'est pas pr�sente" );
    }

    //Renvoie la liste des saisies (non ordonn�e), de type TStdSais, enabled
    varArray<TWinTST> TabSaisies = objFiche.FindAllChildren(Array("WndClass","Enabled"),Array("TStdSais","true"),10);
    varI<bool> bok = false;

    //Ordonner le tableau (mais possible qu'il y ait des index non occupis, � cause des TabOrder);
    varArray<TWinTST> TabOrd = newArray();
    for (int i = 0; i<TabSaisies.length; i++) {
        varI<int> newindex = TabSaisies[i].TabOrder + 1;
        TabOrd[newindex] = TabSaisies[i];
    }

    // Ordonner le tableau et enlever les vides   
    varArray<TWinTST> NewTabOrd = newArray(); 
    varI<int> newindex = 0;
    for (int i = 0; i<TabOrd.length; i++) {
        // Ici il faut sauter les �l�ments vides
        if (TabOrd[i].Exists) {
            NewTabOrd[newindex] = TabOrd[i];
            newindex = newindex + 1;
        }
    }

    return NewTabOrd;
#endif
}

//-------------------------------------------------------------------------------
void P_Fiche_Modif_Saisies(TWinTST Fiche) {

    varArray<TWinTST> Saisies =  F_Fiche_Modif_GetSaisies(Fiche);

    for(varI<int> i=0; i<Saisies.length; i++) {
        var<TStringTST> ParamsSaisie = Saisies[i].Params;
        var<TStringTST> ParamsDecode = F_Get_ParamsDecode(ParamsSaisie);
        var<TTypSaisie> TypeSaisie = F_Get_TypeSaisieParams(ParamsDecode);
        //Log.Message ParamsSaisie
        //Log.Message ParamsDecode
        
        //Il ne faut pas modifier le code de la fiche
        if( ! Contains(ParamsSaisie,"CODE") ) {
        
          if( ParamsSaisie == "@CP" ) {

          }else if(ParamsSaisie == "@VILLE" ) {

          }else{
              //On laisse la valeur qui a d{ �tre initialisi avec le code postale
               if( Contains(ParamsDecode,"Rch") ) {
                       
               }else{
                      if( TypeSaisie == C_TypeSaisie_Nombre ) {
                      
                      }else if(TypeSaisie == C_TypeSaisie_Alpha ) {
                              if( ! Contains(ParamsDecode,"MLN") ) { 
                                Saisies[i].Keys("Libell� Alpha Modif");
                              }else{
                                Saisies[i].Keys("[End] Modif");
                              }
                      }else if (TypeSaisie == C_TypeSaisie_Multi ) {

                      }else if (TypeSaisie == C_TypeSaisie_Date) {

                      }else if (TypeSaisie == C_TypeSaisie_Inconnu) {
                              Log.Warning( "Type de saisie inconnue");
                      }
              }
            
          }
        }
    }//next
}


//------------------------------------------------------------------------------- 
// Retourne le nombre de test qui sera � r�aliser pour un Item donn�.
// Tient compte du fait que l'item soit coch� et qu'une proc�dure de test
// lui soit rattachie. 
//-------------------------------------------------------------------------------
int F_Count_Nb_TestToRunInItem(TTestItemTST ti) {

    varI<int> V_Nb_Test = 0;
    
    // On regarde si une proc�dure de test est rattachie et que l'item est coch�
    if( ! ti.ElementToBeRun.isNothing && ti.Enabled == true ) {
      V_Nb_Test = V_Nb_Test + (1 * ti.Count);    // On tient compte du count d�fini sur l'item
    }
    
    // Un item peut �tre un groupe. Il faut alors regarder les item enfants
    for(varI<int> i=0; i<ti.ItemCount; i++) {
        V_Nb_Test = V_Nb_Test + F_Count_Nb_TestToRunInItem (ti.TestItem(i));
    }
    
    return V_Nb_Test;
}
//------------------------------------------------------------------------------- 
// Retourne le nombre de test qui sera � r�aliser sur le projet.
//-------------------------------------------------------------------------------
int F_Count_Nb_TestToRunInProject() {
    varI<int> V_Nb_Test = 0;

    for (varI<int> i = 0; i<Project.TestItems.ItemCount; i++) {
        V_Nb_Test = V_Nb_Test + F_Count_Nb_TestToRunInItem(Project.TestItems.TestItem(i));
    }

    return V_Nb_Test;
}



//------------------------------------------------------------------------------- 
// Proc�dure permettant d'attendre que la fiche soit ferm�e (Fiche.visible). 
// Le timeout est � passer en param�tre. S'il n'est pas pr�cis� Timeout = 0, on
// le fixe arbitrairement � 60 seconde
//-------------------------------------------------------------------------------
void P_Fiche_WaitClose(TWinTST Fiche, int Timeout) {
    
    if(Timeout == 0 ) { Timeout = 6000; }
    
    if( ! Fiche.WaitProperty("Exists", false, Timeout) ) {
        Log.Error( "La fiche '" + Fiche.Caption + "' ne s'est pas ferm�e");
    }
}

//***  END OF FILE ******************************************************************************************
