//*****************************************************************************************
// Définition (instanciation) de l'application MDE
//
//*****************************************************************************************
#pragma once

#include "cppTestEngine.h"  // pour TProcessTST....

#include "Widgets_Unit.cpp"  // pour UIaction
//USEUNIT Widgets_Unit


JSobject MDE_Appli {
   field<TUIobject>   process = new TUIobject();
   field<TUIobject>   menuFram = new TUIobject();
   field<TUIobject>   mainWin = new TUIobject();
   field<TUIobject>   barIcon = new TUIobject();
   fieldI<int>        count = 12;
};


#ifdef IMPLEMENTATION
var<MDE_Appli> mde = new MDE_Appli();
#endif

// END OF FILE ********************************************************************************
