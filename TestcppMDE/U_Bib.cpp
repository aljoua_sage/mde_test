//USEUNIT U_Bib_Outils
//USEUNIT U_Commun

#include "_common.h"

//-------------------------------------------------------------------------------
void T_Bib_0001() { //Cr�ation d'un ouvrage sans composante

    P_Open_Lst_Elem();
    
    F_Creat_Ouvtx();
    
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();
    
}

//-------------------------------------------------------------------------------
void T_Bib_0002() { //Cr�ation d'un ouvrage avec composantes

    P_Open_Lst_Elem();
    
    F_Creat_Ouvtx();
    
    P_Insert_Compo();
        
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();
    
}

//-------------------------------------------------------------------------------
void T_Bib_0003() { //Modif ouvrage avec insertion de composantes (ref et non ref) / modif de qt� 

    P_Open_Lst_Elem();
    
    var<TCodeTST> CodeElem = F_Creat_Ouvtx();
    
    P_Insert_Compo();
        
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();
    
    //On ouvre la liste des �l�ments  
    P_Open_Lst_Elem();
    
    var<TWinTST> maliste = F_Liste("El�ments");
    F_Liste_Filtre( maliste, CodeElem.toString() );
    //On ouvre une fiche �l�ments en Modification
    Sys.Keys("~m");
    var<TWinTST> mafiche = F_Fiche("TfmElement", "Fiche El�ment");
    mafiche.WaitProperty("Visible",true);
    //On va sur l'onglet composante
    Sys.Keys("~o");
    delay(50);
    //On passe la qt� de la 1er composante � 5
    for(int i=0; i<4; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    Sys.Keys("5");
    //On insert une ligne de composante
    Sys.Keys("^[Ins]");
    delay(50);
    //On va sur la r�f�rence
    Sys.Keys("[Tab]");
    delay(50);
    //On saisi la composante OUVT01
    Sys.Keys("OUVT01");
    delay(50);
    Sys.Keys("[Tab]");
    //On insert une ligne de composante
    Sys.Keys("^[Ins]");
    delay(50);
    //On choisi la nature St ma�onnerie
    Sys.Keys("s");
    delay(50);
    //On va sur le libell�
    Sys.Keys("[Tab][Tab]");
    //On saisi le libell� Non ref st ma�onnerie 
    Sys.Keys("Non ref st ma�onnerie");
    delay(500);
    //On se positionne sur la colonne d�bours�
    for(int i=0; i<7; i++) {
        Sys.Keys("[Tab]");
        delay(50);
    }
    
    //On saisi un d�bours� de 100
    Sys.Keys("100");
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();

}

//-------------------------------------------------------------------------------
void T_Bib_0004() { //Modif ouvrage avec suppression de composantes et ajout d'une fourniture pos�e 

    P_Open_Lst_Elem();
    
    var<TCodeTST> CodeElem = F_Creat_Ouvtx();
    
    P_Insert_Compo();
        
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();
    
    //On ouvre la liste des �l�ments  
    P_Open_Lst_Elem();
    
    var<TWinTST> maliste = F_Liste("El�ments");
    F_Liste_Filtre( maliste, CodeElem.toString() );
    //On ouvre une fiche �l�ments en Modification
    Sys.Keys("~m");
    var<TWinTST> mafiche = F_Fiche("TfmElement", "Fiche El�ment");
    mafiche.WaitProperty("Visible", true);
    //On va sur l'onglet composante
    Sys.Keys("~o");
    delay(50);
    //On supprime les 2 derni�res composantes
    Sys.Keys("^[End]");
    delay(100);
    for(int i=0; i<2; i++) {
        Sys.Keys("^[Del]");
        delay(100);
    }
    
    //On insert une ligne de composante
    Sys.Keys("^[Ins]");
    delay(50);
    //On insert une fourniture pos�e
    Sys.Keys("FPOSE");
    delay(50);
    Sys.Keys("[Tab]");
    //On valide
    Sys.Keys("[F2]");
    P_CloseAllForm();

}    
 
//-------------------------------------------------------------------------------   
void T_Bib_0005() { //Familles �l�ments (cr�ation, modification, suppression);
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "Familles d'�l�ments : Code et Libell�").VCLObject("PanVScroll");
    var<TWinTST> apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    var<TWinTST> stdBout = apiPanel.Window("TStdBout", "", 5);
    stdBout.Click(9, 12);
    var<TWinTST>  tfmFamilleElt = stdScrollBox.Window("TfmFamilleElt", "Famille El�ments [Cr�ation]");
    var<TWinTST>  apiPanel2 = tfmFamilleElt.VCLObject("pnTop");
    apiPanel2.VCLObject("saiReference").Keys("FAM1[Tab]");
    apiPanel2.VCLObject("saiLibelle").Keys("FAM1[Tab]");
    var<TWinTST>  stdPanel = tfmFamilleElt.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(32, 17);
    stdPanel.VCLObject("btQuitter").Click(41, 12);
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(42, 24);
    stdScrollBox.Window("TfmFamilleElt", "Famille El�ments [Consultation]").VCLObject("StdPanel1").VCLObject("btValider").Click(30, 13);
    panel.VCLObject("panLeft").VCLObject("panFilters").VCLObject("panGeneral").VCLObject("panTree").VCLObject("pnBiblio").VCLObject("TTBiblioTree").Window("TApibatTreeView").ClickItem("|Electricit�|FAM1");
    stdBout.Click(16, 11);
    tfmFamilleElt = stdScrollBox.Window("TfmFamilleElt", "Famille El�ments [Cr�ation]");
    apiPanel2 = tfmFamilleElt.VCLObject("pnTop");
    apiPanel2.VCLObject("saiReference").Keys("SOUSFAM1[Tab]");
    apiPanel2.VCLObject("saiLibelle").Keys("SOUSFAM1[Tab]");
    stdPanel = tfmFamilleElt.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(22, 21);
    stdPanel.VCLObject("btQuitter").Click(32, 16);
    apiPanel.Window("TStdBout", "", 6).Click(16, 11);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(124, 490);
    tfmMenu.MainMenu.Click("[2]|[0]");
    panel = stdScrollBox.Window("TStdFormListe", "Familles d'�l�ments : Code et Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(41, 20);
    tfmFamilleElt = stdScrollBox.Window("TfmFamilleElt", "Famille El�ments [Consultation]");
    var<TWinTST>  stdSais = tfmFamilleElt.VCLObject("pnTop").VCLObject("saiLibelle");
    stdSais.Click(119, 8);
    stdSais.Keys(" modif");
    tfmFamilleElt.VCLObject("StdPanel1").VCLObject("btValider").Drag(46, 10, -7, 1);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 6).Click(16, 14);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(145, 499);
    tfmMenu.MainMenu.Click("[2]|[0]");
    panel = stdScrollBox.Window("TStdFormListe", "Familles d'�l�ments : Code et Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(51, 20);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 3).Click(17, 10);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(25, 10);
    apiPanel.Window("TStdBout", "", 6).Click(14, 8);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(173, 515);
}

//-------------------------------------------------------------------------------
void T_Bib_0006() { //Fabricants
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[1]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "Fabricants : Code et Libell�").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 4).Click(17, 14);
    var<TWinTST> tfmFabricant = stdScrollBox.Window("TfmFabricant", "Fabricant [Cr�ation]");
    var<TWinTST> stdSais = tfmFabricant.VCLObject("saiLibelle");
    stdSais.Click(18, 8);
    stdSais.Keys("test");
    var<TWinTST> stdPanel = tfmFabricant.VCLObject("StdPanel1");
    var<TWinTST> stdBout = stdPanel.VCLObject("btValider");
    stdBout.Click(30, 13);
    tfmFabricant.VCLObject("saiCode").Keys("[Tab]");
    stdSais.Keys("test1");
    stdBout.Click(47, 9);
    stdPanel.VCLObject("btQuitter").Click(33, 12);
    apiPanel.Window("TStdBout", "", 5).Click(11, 8);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(185, 359);
    tfmMenu.MainMenu.Click("[2]|[1]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "Fabricants : Code et Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(105, 39);
    tfmFabricant = stdScrollBox.Window("TfmFabricant", "Fabricant");
    stdSais = tfmFabricant.VCLObject("saiLibelle");
    stdSais.Click(73, 8);
    stdSais.Keys(" modif");
    tfmFabricant.VCLObject("StdPanel1").VCLObject("btValider").Click(50, 20);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 4).Click(14, 5);
    tfmFabricant = stdScrollBox.Window("TfmFabricant", "Fabricant [Cr�ation]");
    stdSais = tfmFabricant.VCLObject("saiLibelle");
    stdSais.Click(33, 3);
    stdSais.Keys("test2");
    stdPanel = tfmFabricant.VCLObject("StdPanel1");
    stdBout = stdPanel.VCLObject("btValider");
    stdBout.Click(10, 19);
    stdSais.Click(31, 8);
    stdSais.Keys("test3");
    stdBout.Click(28, 20);
    stdPanel.VCLObject("btQuitter").Click(18, 15);
    apiPanel.Window("TStdBout", "", 5).Click(11, 8);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(202, 385);
    tfmMenu.MainMenu.Click("[2]|[1]");
    panel = stdScrollBox.Window("TStdFormListe", "Fabricants : Code et Libell�").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(112, 66);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 2).Click(12, 10);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(44, 12);
    apiPanel.Window("TStdBout", "", 5).Click(16, 8);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(257, 413);
}

//-------------------------------------------------------------------------------
void T_Bib_0007() { //Fournitures et mat�riaux (cr�ation, modification, copie, suppression);
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 8).Click(13, 13);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Cr�ation]");
    var<TWinTST> apiPanel2 = tfmElement.VCLObject("panEntete");
    apiPanel2.VCLObject("saiNature").Click(172, 4);
    mde.Window("TCbxPopupList").Click(105, 46);
    var<TWinTST> stdSais = apiPanel2.VCLObject("saiReference");
    stdSais.Keys("MAT1[Tab]");
    var<TWinTST> stdSais2 = apiPanel2.VCLObject("saiLibelle");
    stdSais2.Keys("MAT1[Tab]");
    var<TWinTST> apibatPageControl = tfmElement.VCLObject("pcElement");
    apibatPageControl.Click(236, 10);
    apibatPageControl.Keys("[Tab]");
    apibatPageControl.VCLObject("tsTarifFour").VCLObject("panTarifFour").VCLObject("panTarifFourTarif").VCLObject("saiDeb").Keys("50[Tab]");
    var<TWinTST> stdBout = tfmElement.VCLObject("StdPanel1").VCLObject("btValider");
    stdBout.Click(41, 8);
    stdSais.Drag(108, 7, -152, -2);
    stdSais.Keys("'AT2[BS][BS][BS][BS]MAT2[Tab]MAT2[Tab]");
    stdSais2.Keys("[Tab]");
    apibatPageControl.Click(230, 9);
    apibatPageControl.Keys("[Tab]");
    stdBout.Click(49, 9);
    apiPanel.Window("TStdBout", "", 9).Click(14, 3);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(67, 514);
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat&[BS]1");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(245, 26);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(123, 0);
    stdSais.Click(123, 1);
    stdSais.Click(129, 7);
    stdSais.Keys(" modif[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(49, 17);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(13, 12);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(202, 533);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(269, 78);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(12, 15);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(110, 2);
    stdSais.Click(111, 9);
    stdSais.Keys(" copie[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(58, 17);
    apiPanel.Window("TStdBout", "", 9).Click(7, 10);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(148, 516);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat1");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(261, 23);
    stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]").VCLObject("StdPanel1").VCLObject("btValider").Click(19, 20);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 6).Click(6, 16);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(54, 12);
    apiPanel.Window("TStdBout", "", 9).Click(6, 3);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(241, 470);
}

//-------------------------------------------------------------------------------
void T_Bib_0008() { //Mains d'oeuvre (cr�ation, modification, suppression);
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST>apiPanel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 8).Click(14, 15);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Cr�ation]");
    var<TWinTST> apiPanel2 = tfmElement.VCLObject("panEntete");
    var<TWinTST> stdSais = apiPanel2.VCLObject("saiNature");
    stdSais.Click(167, 7);
    mde.Window("TCbxPopupList").Click(76, 98);
    var<TWinTST> stdSais2 = apiPanel2.VCLObject("saiReference");
    stdSais2.Keys("motest1[Tab]");
    var<TWinTST> stdSais3 = apiPanel2.VCLObject("saiLibelle");
    stdSais3.Keys("motest1[Tab]");
    var<TWinTST> apibatPageControl = tfmElement.VCLObject("pcElement");
    apibatPageControl.Click(98, 13);
    var<TWinTST> stdSais4 = apibatPageControl.VCLObject("tsTarif").VCLObject("pnTarifArt").VCLObject("saiDebArt");
    stdSais4.Click(31, 8);
    stdSais4.Keys("10[Tab]");
    var<TWinTST> stdPanel = tfmElement.VCLObject("StdPanel1");
    var<TWinTST> stdBout = stdPanel.VCLObject("btValider");
    stdBout.Click(40, 15);
    stdSais.Click(166, 8);
    mde.Window("TCbxPopupList").Click(120, 111);
    stdSais2.Keys("mo2[Tab]");
    stdSais3.Keys("mo2[Tab]");
    apibatPageControl.Click(110, 16);
    stdSais4.Click(62, 6);
    stdSais4.Keys("20[Tab]");
    stdBout.Click(25, 11);
    stdPanel.VCLObject("btQuitter").Click(48, 13);
    apiPanel.Window("TStdBout", "", 9).Click(9, 7);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(286, 560);
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(240, 368);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(81, 8);
    stdSais.Keys(" modif[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(27, 16);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(9, 9);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(340, 460);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(229, 350);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 6).Click(9, 8);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(34, 11);
    apiPanel.Window("TStdBout", "", 9).Click(10, 8);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(279, 505);
}

//-------------------------------------------------------------------------------
void T_Bib_0009() { //Ouvrage travaux (cr�ation, modification, copie, suppression);
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    var<TWinTST> apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 8).Click(11, 20);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Cr�ation]");
    var<TWinTST> apiPanel2 = tfmElement.VCLObject("panEntete");
    apiPanel2.VCLObject("saiReference").Keys("OYVTEST[BS][BS][BS][BS][BS][BS]UVTEST[Tab]");
    apiPanel2.VCLObject("saiLibelle").Keys("OUVRAGE TEST[Tab]");
    var<TWinTST> apibatPageControl = tfmElement.VCLObject("pcElement");
    apibatPageControl.Click(139, 12);
    var<TWinTST> stdScroll = apibatPageControl.VCLObject("tsCompo").VCLObject("panCompo").VCLObject("panScrollCompo").VCLObject("scCompo");
    stdScroll.Click(162, 29);
    var<TWinTST> scrollSaisieEdit = stdScroll.Window("TScrollSaisieEdit", "", 1);
    scrollSaisieEdit.Keys("[F4]");
    stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente", 1).VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(158, 25);
    scrollSaisieEdit.Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 11).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 10).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", "", 9).Keys("[Tab]");
    stdScroll.Window("TScrollCoche", "", 1).Keys("[Tab]");
    stdScroll.Window("TScrollSaisieEdit", " ").Keys("[Tab]");
    scrollSaisieEdit.Keys("[F4]");
    stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente", 1).VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll").DblClick(154, 59);
    var<TWinTST> stdPanel = tfmElement.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(44, 13);
    stdPanel.VCLObject("btQuitter").Click(26, 19);
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("ouvtest[Enter]");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(179, 17);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]");
    var<TWinTST> stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(148, 3);
    stdSais.Keys(" modif");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(53, 18);
    apiPanel.Window("TStdBout", "", 5).Click(9, 12);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.DblClick(114, 9);
    stdSais.Keys("copie");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(15, 15);
    apiPanel.Window("TStdBout", "", 6).Click(12, 19);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(30, 18);
    apiPanel.Window("TStdBout", "", 9).Click(11, 12);
    tfmMenu.Keys("~b");
    stdScrollBox.Drag(259, 254, -1, 8);
}

//-------------------------------------------------------------------------------
void T_Bib_0010() { //Ouvrage ditailli (cr�ation, modification, copie, suppression);
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll").VCLObject("panStatus");
    var<TWinTST> apiPanel = panel.VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 8).Click(14, 12);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Cr�ation]");
    var<TWinTST> apiPanel2 = tfmElement.VCLObject("panEntete");
    apiPanel2.VCLObject("saiNature").Click(164, 10);
    mde.Window("TCbxPopupList").Click(57, 43);
    apiPanel2.VCLObject("saiReference").Keys("OUVDETAIL[Tab]");
    var<TWinTST> stdSais = apiPanel2.VCLObject("saiLibelle");
    stdSais.Keys("OUVDETAI");
    stdSais.Click(62, 15, skShift);
    stdSais.Keys("L[Tab]");
    var<TWinTST> apibatPageControl = tfmElement.VCLObject("pcElement");
    apibatPageControl.Click(109, 8);
    var<TWinTST> stdScroll = apibatPageControl.VCLObject("tsCompo").VCLObject("panCompo").VCLObject("panScrollCompo").VCLObject("scCompo");
    stdScroll.Click(163, 23);
    stdScroll.Window("TScrollSaisieEdit", "", 1).Click(68, 14);
    stdScroll = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente", 1).VCLObject("PanVScroll").VCLObject("panCli").VCLObject("Scroll");
    stdScroll.Click(221, 125, skCtrl);
    stdScroll.Click(230, 168, skCtrl);
    stdScroll.Click(242, 271, skCtrl);
    stdScroll.Keys("[Enter]");
    var<TWinTST> stdPanel = tfmElement.VCLObject("StdPanel1");
    stdPanel.VCLObject("btValider").Click(43, 9);
    stdPanel.VCLObject("btQuitter").Click(40, 11);
    panel.Click(21, 26);
    apiPanel.Window("TStdBout", "", 9).Click(14, 15);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(77, 530);
    tfmMenu.Click(195, 42);
    mde.Window("#32768", "", 2).Click(75, 61);
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("ouv");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(255, 21);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(109, 11);
    stdSais.Keys(" modif[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(37, 18);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(9, 13);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(92, 550);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("ouv");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(304, 24);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(10, 11);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    apiPanel2 = tfmElement.VCLObject("panEntete");
    stdSais = apiPanel2.VCLObject("saiLibelle");
    stdSais.DblClick(91, 9);
    stdSais.Drag(124, 9, -176, -12);
    stdSais.Keys("^c");
    var<TWinTST> stdSais2 = apiPanel2.VCLObject("saiReference");
    stdSais2.Drag(118, 6, -176, -3);
    stdSais2.Keys("^v");
    stdSais2.Drag(108, 8, -128, -10);
    stdSais2.Keys("OUVDETAIL copie[Tab]");
    stdSais.Keys("[End] copie[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(50, 12);
    apiPanel.Window("TStdBout", "", 9).Click(13, 10);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(88, 538);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("ouv");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(298, 36);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 6).Click(13, 15);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(25, 15);
    apiPanel.Window("TStdBout", "", 9).Click(10, 14);
    tfmMenu.Keys("~b");
    stdScrollBox.Drag(107, 540, 19, -14);
}

//-------------------------------------------------------------------------------
void T_Bib_0011() { //Copie mat�riau et mains d'oeuvre
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(222, 313);
    var<TWinTST> apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(11, 15);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    var<TWinTST> stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(72, 10);
    stdSais.Keys(" copie");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(42, 8);
    apiPanel.Window("TStdBout", "", 9).Click(10, 14);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(100, 561);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(233, 217);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(12, 16);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(112, 5);
    stdSais.Keys(" copie[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(53, 11);
    apiPanel.Window("TStdBout", "", 9).Click(18, 16);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(94, 535);
}

//-------------------------------------------------------------------------------
void T_Bib_0012() { //Tri sur colonne et filtre dans la liste
    var<TWinTST> tfmMenu = Sys.Process("mde").VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    var<TWinTST> scrollHeader = panel.VCLObject("panCli").VCLObject("Scroll").Window("TScrollHeader");
    scrollHeader.Click(247, 5);
    scrollHeader.Click(63, 6);
    scrollHeader.Click(238, 5);
    var<TWinTST> stdSais = panel.VCLObject("panFiltres").VCLObject("saiQuickSearch");
    stdSais.Click(19, 2);
    stdSais.Keys("ouv");
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(12, 13);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(251, 430);
}

//-------------------------------------------------------------------------------
void T_Bib_0013() { //Cr�ation, modification, copie et suppression d'une vue
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    var<TWinTST> stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.ClickR(133, 84);
    stdScroll.PopupMenu.Click("Gestion des vues");
    var<TWinTST> stdPanel = stdScrollBox.Window("TViewsListDlg", "Vues").VCLObject("StdPanel");
    stdPanel.VCLObject("btnNew").Click(50, 17);
    var<TWinTST> viewDlg = stdScrollBox.Window("TViewDlg", "Param�trage d'une vue");
    var<TWinTST> panel2 = viewDlg.VCLObject("panCenter").VCLObject("PanParam");
    var<TWinTST> apiPanel = panel2.VCLObject("panFields");
    var<TWinTST> stdListBox = apiPanel.VCLObject("lbFields");
    stdListBox.VScroll.Pos = 0;
    stdListBox.Click(32, 34);
    apiPanel.VCLObject("ApiPanel2").VCLObject("BtSupprChamps").Click(16, 14);
    panel2.VCLObject("ApiPanel1").VCLObject("ApiPanel3").VCLObject("saiOrderField1").Click(172, 8);
    mde.Window("TFrmTables", "Champ de tris").VCLObject("BackPanel").Window("TDBEntityTreeView").DblClickItem("|Code");
    viewDlg.VCLObject("StdPanel").VCLObject("BtnValider").Click(44, 17);
    stdPanel.VCLObject("btnClose").Click(27, 11);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(18, 11);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(225, 579);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.ClickR(182, 88);
    stdScroll.PopupMenu.Click("Gestion des vues");
    var<TWinTST> viewsListDlg = stdScrollBox.Window("TViewsListDlg", "Vues");
    viewsListDlg.VCLObject("Panel1").VCLObject("lbViews").Click(72, 49);
    stdPanel = viewsListDlg.VCLObject("StdPanel");
    stdPanel.VCLObject("btnModify").Click(32, 16);
    viewDlg = stdScrollBox.Window("TViewDlg", "Param�trage d'une vue");
    var<TWinTST> stdSais = viewDlg.VCLObject("panTop").VCLObject("saiViewName");
    stdSais.Click(142, 7);
    stdSais.Keys(" modif");
    var<TWinTST> stdPanel2 = viewDlg.VCLObject("StdPanel");
    stdPanel2.Click(137, 32);
    stdPanel2.VCLObject("BtnValider").Click(49, 15);
    stdPanel.VCLObject("btnClose").Click(44, 13);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(8, 9);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(219, 571);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : Nouvelle vue modif").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.ClickR(118, 104);
    stdScroll.PopupMenu.Click("Gestion des vues");
    viewsListDlg = stdScrollBox.Window("TViewsListDlg", "Vues");
    stdPanel = viewsListDlg.VCLObject("StdPanel");
    stdPanel.VCLObject("btnCopy").Click(59, 14);
    viewsListDlg.VCLObject("Panel1").VCLObject("lbViews").Click(111, 48);
    stdPanel.VCLObject("btnClose").Click(55, 14);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(11, 14);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(219, 582);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : Nouvelle vue modif").VCLObject("PanVScroll");
    stdScroll = panel.VCLObject("panCli").VCLObject("Scroll");
    stdScroll.ClickR(105, 67);
    stdScroll.ClickR(99, 63);
    stdScroll.PopupMenu.Click("Gestion des vues");
    viewsListDlg = stdScrollBox.Window("TViewsListDlg", "Vues");
    stdListBox = viewsListDlg.VCLObject("Panel1").VCLObject("lbViews");
    stdListBox.Click(109, 63);
    stdPanel = viewsListDlg.VCLObject("StdPanel");
    var<TWinTST> stdBout = stdPanel.VCLObject("btnDelete");
    stdBout.Click(40, 13);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(55, 12);
    stdListBox.Click(65, 47);
    stdBout.Click(35, 17);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(21, 9);
    stdPanel.VCLObject("btnClose").Click(41, 18);
}

//-------------------------------------------------------------------------------
void T_Bib_0014() { //Modification globale des �l�ments
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[5]|[4]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> tfmElementMAJGlobal = stdScrollBox.Window("TfmElementMAJGlobal", "Modifications globales des �l�ments");
    var<TWinTST> apiPanel = tfmElementMAJGlobal.VCLObject("pnGeneral").VCLObject("panCriteresBottom").VCLObject("pcData").VCLObject("tsCaract").VCLObject("panCaracteristique").VCLObject("panCaracteristique_1").VCLObject("panCaracteristiqueRight");
    var<TWinTST> apiPanel2 = apiPanel.VCLObject("pnSuiviCht");
    apiPanel2.VCLObject("pnSuiviChtCoche").VCLObject("cbSuiviChantier").Click(11, 10);
    var<TWinTST> apiPanel3 = apiPanel.VCLObject("pnSuiviStock");
    apiPanel3.VCLObject("pnSuiviStockCoche").VCLObject("cbSuiviStock").Click(8, 10);
    apiPanel = apiPanel2.VCLObject("pnSuiviChtRight");
    apiPanel.Click(229, 12);
    apiPanel3.VCLObject("pnSuiviStockRight").VCLObject("cbGestStock").Click(7, 8);
    apiPanel.VCLObject("cbSuiviCht").Click(7, 12);
    var<TWinTST> stdPanel = tfmElementMAJGlobal.VCLObject("PanBoutons");
    stdPanel.VCLObject("btValider").Click(52, 11);
    mde.Window("TApiMessageDlg", "Modifications globales des �l�ments").Window("TApiPanel").Window("TStdBout", "", 2).Click(45, 12);
    mde.Window("TApiMessageDlg", "Modifications globales des �l�ments").Window("TApiPanel").Window("TStdBout").Click(37, 10);
    stdPanel.VCLObject("btQuitter").Click(40, 10);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(182, 565);
}

//-------------------------------------------------------------------------------
void T_Bib_0015() { //Mise � jour des tarifs
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[5]|[0]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> tfmMajTarifArticle = stdScrollBox.Window("TfmMajTarifArticle", "Mise � jour des tarifs");
    var<TWinTST> apiPanel = tfmMajTarifArticle.VCLObject("pnBottom").VCLObject("panGeneral").VCLObject("panEntete");
    apiPanel.VCLObject("panEnteteTop").VCLObject("saiVariation").Click(206, 8);
    mde.Window("TCbxPopupList").Click(82, 37);
    var<TWinTST> stdSais = apiPanel.VCLObject("panEnteteClient").VCLObject("saiValeur");
    stdSais.Click(18, 5);
    stdSais.Drag(19, 7, 128, -12);
    stdSais.Keys("1.5[Tab]");
    var<TWinTST> stdPanel = tfmMajTarifArticle.VCLObject("Panel");
    stdPanel.VCLObject("btValider").Click(44, 17);
    mde.Window("TApiMessageDlg", "Mise � jour des tarifs").Window("TApiPanel").Window("TStdBout", "", 2).Click(26, 6);
    mde.Window("TApiMessageDlg", "Mise � jour des tarifs").Window("TApiPanel").Window("TStdBout").Click(38, 12);
    stdPanel.VCLObject("btQuitter").Click(25, 22);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(324, 400);
}

void Test1_Bib() {
    var<TProcessTST> mde = Sys.Process("mde");
    var<TWinTST> tfmMenu = mde.VCLObject("fmMenu");
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> stdScrollBox = tfmMenu.VCLObject("ClientControl");
    var<TWinTST> apiPanel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll").VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 8).Click(13, 13);
    var<TWinTST> tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Cr�ation]");
    var<TWinTST> apiPanel2 = tfmElement.VCLObject("panEntete");
    apiPanel2.VCLObject("saiNature").Click(172, 4);
    mde.Window("TCbxPopupList").Click(105, 46);
    var<TWinTST> stdSais = apiPanel2.VCLObject("saiReference");
    stdSais.Keys("MAT1[Tab]");
    var<TWinTST> stdSais2 = apiPanel2.VCLObject("saiLibelle");
    stdSais2.Keys("MAT1[Tab]");
    var<TWinTST> apibatPageControl = tfmElement.VCLObject("pcElement");
    apibatPageControl.Click(236, 10);
    apibatPageControl.Keys("[Tab]");
    apibatPageControl.VCLObject("tsTarifFour").VCLObject("panTarifFour").VCLObject("panTarifFourTarif").VCLObject("saiDeb").Keys("50[Tab]");
    var<TWinTST> stdBout = tfmElement.VCLObject("StdPanel1").VCLObject("btValider");
    stdBout.Click(41, 8);
    stdSais.Drag(108, 7, -152, -2);
    stdSais.Keys("'AT2[BS][BS][BS][BS]MAT2[Tab]MAT2[Tab]");
    stdSais2.Keys("[Tab]");
    apibatPageControl.Click(230, 9);
    apibatPageControl.Keys("[Tab]");
    stdBout.Click(49, 9);
    apiPanel.Window("TStdBout", "", 9).Click(14, 3);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(67, 514);
    tfmMenu.MainMenu.Click("[2]|[3]");
    var<TWinTST> panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat&[BS]1");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(245, 26);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(123, 0);
    stdSais.Click(123, 1);
    stdSais.Click(129, 7);
    stdSais.Keys(" modif[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(49, 17);
    panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons").Window("TStdBout", "", 9).Click(13, 12);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(202, 533);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat");
    panel.VCLObject("panCli").VCLObject("Scroll").Click(269, 78);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 5).Click(12, 15);
    tfmElement = stdScrollBox.Window("TfmElement", "Fiche El�ment [Copie]");
    stdSais = tfmElement.VCLObject("panEntete").VCLObject("saiLibelle");
    stdSais.Click(110, 2);
    stdSais.Click(111, 9);
    stdSais.Keys(" copie[Tab]");
    tfmElement.VCLObject("StdPanel1").VCLObject("btValider").Click(58, 17);
    apiPanel.Window("TStdBout", "", 9).Click(7, 10);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(148, 516);
    tfmMenu.MainMenu.Click("[2]|[3]");
    panel = stdScrollBox.Window("TStdFormListe", "El�ments : D�bours� et prix de vente").VCLObject("PanVScroll");
    panel.VCLObject("panFiltres").VCLObject("saiQuickSearch").Keys("mat1");
    panel.VCLObject("panCli").VCLObject("Scroll").DblClick(261, 23);
    stdScrollBox.Window("TfmElement", "Fiche El�ment [Consultation]").VCLObject("StdPanel1").VCLObject("btValider").Click(19, 20);
    apiPanel = panel.VCLObject("panStatus").VCLObject("panTools").VCLObject("panButtons");
    apiPanel.Window("TStdBout", "", 6).Click(6, 16);
    mde.Window("TApiMessageDlg", "Suppression").Window("TApiPanel").Window("TStdBout", "", 2).Click(54, 12);
    apiPanel.Window("TStdBout", "", 9).Click(6, 3);
    tfmMenu.Keys("~b");
    stdScrollBox.Click(241, 470);
}
// ** END OF FILE *********************************************************************