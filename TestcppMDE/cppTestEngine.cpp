//*****************************************************************************************
// TEST ENGINE (TestComplete) C++ emulation 
//
// These objects emulates the TestComplete API (objects), in C++
// 
// This is a minimal implementation (stubs) to be able to link in C++
//*****************************************************************************************

#include "cppTestEngine.h"

#include "cppToJavaScript.h"

// Instanciation of TestEngine global objects
TLoggerTST Log;
TRunnerTST Runner;
TSystemTST Sys;
TAqUtilTST aqUtils;
TFilesUtilsTST Files;
int Instance;
int Timer;
TProjectTST Project;
TErrorListTST err;
TProjectSuiteTST ProjectSuite;
TTestedAppsListTST TestedApps;
TDbgServicesTST DbgServices;

// Implem TRunTObj methods
bool TRunTObj::WaitProperty(TStringTST _propName, bool _bVal, int _timoutMs) { return false; }
TWinTST TRunTObj::WaitWindow(TStringTST _winClass, TStringTST _title, int xxx, int _timeout) { return TWinTST(); }
TWinTST TRunTObj::Window(TStringTST _winClass, TStringTST _title, int _grpIdx ) { return TWinTST(); }
TWinTST TRunTObj::VCLObject(TStringTST _objName) { return TWinTST(); }
TWinTST TRunTObj::WaitVCLObject(TStringTST _objName, int _timeout) { return TWinTST(); }


// Implem TWinTST methods
void TProcessTST::Terminate(){}

// Implem TMenuTST methods
void TMenuTST::Click(TStringTST  _subMenuPath) {}
void TMenuTST::Close() {}

// Implem TWinTST methods
int TWinTST::wPosition=0; //static member
void TWinTST::Click() {} // pour les boutons ??
void TWinTST::Click(int _x, int _y, int _opt) {}
void TWinTST::ClickR(int _x, int _y) {}
void TWinTST::DblClick(int _x, int _y) {}
void TWinTST::ClickItem(TStringTST _ItemPath) {}
void TWinTST::ClickItem(TStringTST _ItemPath, bool xxx) {}
void TWinTST::DblClickItem(TStringTST _ItemPath) {}
void TWinTST::MouseWheel(int _move) {}
void TWinTST::Drag(int _x, int _y, int _z, int _t) {}
void TWinTST::Keys(TStringTST _keyName) {}
void TWinTST::SetFocus() {}
void TWinTST::Close() {}
void TWinTST::OpenFile(TStringTST _filename, TStringTST _filter) {}

TWinTST TWinTST::WaitNamedChild(TStringTST _nomChild, int _timeout) { return TWinTST(); }
TWinTST TWinTST::FindChild( Array _propName, Array _propVal, int _depth) { return TWinTST(); }
TWinTST TWinTST::FindChild(TStringTST _propName, TStringTST _propVal, int _depth) { return TWinTST(); }
varArray<TWinTST> TWinTST::FindAllChildren(TStringTST arrayX, TStringTST arrayY, int xxx) { return *(varArray<TWinTST>*)(0); }

void TRunnerTST::stop(bool xxx) {}

TProcessTST TSystemTST::WaitProcess(TStringTST _name, int _timeout) { return TProcessTST(); }
TProcessTST TSystemTST::WaitProcess(TStringTST _name) { return TProcessTST(); }
TProcessTST TSystemTST::Process(TStringTST _name) { return TProcessTST(); }
TWinTST TSystemTST::WaitChild(TStringTST ChildName, int _timeout) { return TWinTST(); }
void TSystemTST::Keys(TStringTST _keyName) {}
TStringTST TSystemTST::Clipboard() { return TStringTST(); }

void TFileItemTST::Check(TStringTST filePath) {}

// Par commodité, LaunchApplication retourne une winTST
TWinTST TDbgServicesTST::LaunchApplication(TStringTST exePath, TStringTST argList) { return TWinTST(); }
bool TFilesUtilsTST::Compare(TStringTST file1, TStringTST file2, int xxx, bool yyy) { return false; }
bool TFilesUtilsTST::Contains(TStringTST fileName) { return false; }
void TFilesUtilsTST::Remove(TStringTST fileName) {}
bool TFilesUtilsTST::Add(TStringTST filePath, TStringTST name) { return false; }
TFileItemTST TFilesUtilsTST::Items(TStringTST name) { return TFileItemTST(); }


TTestItemTST TTestItemTST::TestItem(int idx) { return TTestItemTST(); }
TTestItemTST TTestItemTST::Current; //static

void TTestedAppTST::Run(int, bool) {}

void TLoggerTST::Error( TStringTST _messErr) {}
void TLoggerTST::Message( TStringTST  _mess) {}
void TLoggerTST::Warning( TStringTST _messWarn) {}
void TLoggerTST::Picture(int _picture, TStringTST  _xxx) {}
void TLoggerTST::LockEvents(int) {}

void TAqUtilTST::Delay(int dTms) {}


/****************************************************************************************************/
// We put the main here (for linking)
int main()
{
    return 0;
}


// END OF FILE ********************************************************************************
