//*****************************************************************************************
// A few application types and classes to enforce C++ type checking
//
//*****************************************************************************************

#ifndef _TYPESAPPLICATIF_H
#define _TYPESAPPLICATIF_H

#include "cppToJavaScript.h" // for TStringTST...

class TCodeTST {
public:
   explicit TCodeTST( int _iCode) {}
   TCodeTST(const char *_strCode) {}
   TCodeTST(TStringTST _strCode) {}
   TCodeTST & operator = (const TStringTST & _strCode) { static TCodeTST bid; return bid; }
   TStringTST toString() const { return TStringTST(); }
   TCodeTST() {};
};

class TTypSaisie { };

struct TTestSuiteVariables {
    TStringTST MDEexe_subDir;
};


#endif
// END OF FILE ********************************************************************************
