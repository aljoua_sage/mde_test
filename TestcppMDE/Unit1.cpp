
#include "_common.h"


//------------------------------------------------------------------------------- 
// Lorsque une erreur est logg�e
//-------------------------------------------------------------------------------
void GeneralEvents_OnLogError( int Sender, int LogParams ) {
    // Ponctuellement il faut pouvoir ne pas faire les traitements ci-dessous
    // ex : Le check de file retourne une erreur si <> mais on ne veux pas red�marrer
    // Il faut juste poser G_Restart = False avant l'instruction succeptible de retourner une erreur 
    if( G_Restart == true ) {
        // On a pris une erreur 
        G_IsError = true;
        
        // Si on prend une erreur sur le dernier test de l'automate, il faut lancer la proc�dure de fin,
        // M�me si le nombre d'it�ration pr�vues n'a pas �t� encore r�alis�.
        var<TStringTST> V_CurrentTest_Name = Project.TestItems.Current.Name;  
        var<TStringTST> V_LastTest_NameCount = F_Get_Last_TestNameCount();
        if( Contains(V_LastTest_NameCount, V_CurrentTest_Name) != 0 ) { 
            P_Fin_Automate();
        }
        
        // La m�thode Runner.stop(True) permet de quitter le test en cours et d'enchainer sur le prochain test
        // Attention : dans le projet il faut d�sactiver l'option Stop on error
        Runner.stop(true);
    }
}


//------------------------------------------------------------------------------- 
// Au lancement de chaque cas de test
//-------------------------------------------------------------------------------
void GeneralEvents_OnStartTest(int Sender) {

    if( ! G_P_Init_Automate ) {
        //Initialisation de l'automate
        P_Init_Automate();
    }
    
    //Initialise G_Test_Name
    F_Get_Test_Name();
    
    //Initialise G_Rep_Oracles 
    F_Init_Rep_Oracles();
    
    //Initialise un test
    P_Debut();
}

//------------------------------------------------------------------------------- 
// A la fin de chaque cas de test
//-------------------------------------------------------------------------------
void GeneralEvents_OnStopTest(int Sender) {

    P_Fin();
}

void GeneralEvents_OnUnexpectedWindow(int Sender, int Window, int LogParams) {
    // On a pris une erreur 
    G_IsError = true;
    
    // On prend une capture du bureau
    Log.Picture( Sys.Desktop.Picture, "Capture �cran" );   

    // On ferme l'application
    P_FermeApp();
    
    // On g�n�re un Log.error pour passer dans le OnLogError 
    Log.Error( "Une fen�tre inattendue est apparue !!!");
}

//**** END OF FILE ***************************************************************************