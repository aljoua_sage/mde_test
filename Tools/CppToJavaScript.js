//*******************************************
// Traduction d'un VBScript en JavaScript
//*******************************************
var fs = require('fs');
var path = require('path');

//********************************************************************************

function cppSourceToJs( allCpp ) {

    var s = allCpp; 
    // Retrait de toutes directives PP commen�ant par "#"  (include, pragma... )
    // Notes:
    //   - il faut capturer puis restituer le "debut de ligne", qui peut �tre le CR pr�c�dent
    //   - comme "^" match CR et LF, on utilise ( |\t) plut�t que "\s" qui matcherait LF apr�s CR
    s = s.replace(/(^)( |\t)*#.*\r/gm, "$1\r");

    // Remplace les CAST(typ, obj) par obj
    s = s.replace(/\bCAST\s*\(\s*\w+\s*,\s*(\w+)\s*\)/gm, "$1");

    // Retrait des DECLAR_METHOD(....)
    s = s.replace(/(^)( |\t)*\bDECLAR_METHOD\b.*\r/gm, "$1\r");

    // Remplacement de JSCODE(X) par X
    s = s.replace(/\bJSCODE\s*\((.*)\)/gm, "$1");

    // Retrait des inline
    s = s.replace(/\binline\b\s*/gm, "");

    // Remplacement des d�finitions de m�thode
    s = s.replace(/\n(?: |\t)*\w+\s+(\w+)\s*\(\s*(\w+)\s*This\s*,*/gm, "\n$2.prototype.$1 = function METHODSTUBAQZ (");

    // Remplacement des This par this
    s = s.replace(/\bThis\b/gm, "this");
    s = s.replace(/\bTHIS\b/gm, "this");

    // Remplacement des JSobject nomType : deriv { 
    s = s.replace(/\bJSobject\s*(\w+).*{/gm, "function $1() {");

    // Remplacement des field<> nomField, par this.nomField
    s = s.replace(/\bfield\s*<\s*\w+\s*>\s*(\w+)/gm, "this.$1");
    s = s.replace(/\bfieldI\s*<\s*\w+\s*>\s*(\w+)/gm, "this.$1");
    s = s.replace(/\bfieldR\s*<\s*\w+\s*>\s*(\w+)/gm, "this.$1");

    // Remplacement des const_js<typ> nomVar, par var, parce 'const' n'est pas support� par ce JavaScript' 
    s = s.replace(/\bconst_js\s*<\s*\w+\s*>/gm, "var ");


    // Remplacement des var<>, des varI<> et des varArray<>
    s = s.replace(/\bvar\s*<\s*\w+\s*>/gm, "var ");
    s = s.replace(/\bvarI\s*<\s*\w+\s*>/gm, "var ");
    s = s.replace(/\bvarArray\s*<\s*\w+\s*>/gm, "var ");

    // Remplacement des "for(int XXX="  par des "for(varI<int> XXX=""
    s = s.replace(/\bfor\b\s*\(\s*\bint\b(\s*\w+\s*=)/gm, "for( $1");


    // Remplacement des d�finitions de fonctions, mais on rejette else if()
    s = s.replace(/\n(?: |\t)*\w+\s+((?!\bif)\w+\s*\(.*\)\s*{)/gm, "\nfunction $1");
    // Dans les fonctions, retrait du type de chaque argument
    // On itere jusqu'� ce qu'il n'y ait plus d'argument
    for (var i = 0; i < 12; i++) {   // maxi 12 arguments
        var s_old = s;
        // La reg exp est:  function mot ( [mot , ]*] [typ] [mot] [,|)] 
        s = s.replace(/(\bfunction\s+\w+\s*\(\s*(?:\b\w+\b\s*,\s*)*)(\b\w+\b\s*)(\b\w+\b\s*[,)])/gm, "$1$3");
        if (s == s_old) break; // => plus de remplacement
    }   

    // Elimination de " METHODSTUBAQZ "
    s = s.replace(/ METHODSTUBAQZ /gm, "");

    return s;
}



//********************************************************************************
function convert_cppfile(cppFullPath, destPath) {

    var fileName = path.parse(cppFullPath).name;
    if (destPath.length > 0) {
        var endC = destPath[destPath.length - 1];
        if (endC != '\\' || endC != '/') destPath += '\\';
    } 
    var javaScriptFullPath = destPath + fileName + '.js';

    // Open the input VB script file *****************************
    var fileContents = "";
    try {
        fileContents = fs.readFileSync(cppFullPath, 'binary');
    } catch (err) {
        return console.error("Error reading C++ file " + cppFullPath);
    }

    // CONVERT ******************************
    var cppContent = fileContents.toString('latin1');
    var jsContent = cppSourceToJs(cppContent);

    // Read previous converted file
    var old_jscontent = "";
    try {
        var fileJSContents = fs.readFileSync(javaScriptFullPath, 'binary');
        old_jscontent = fileJSContents.toString('latin1');
    } catch (err) {
        old_jscontent = "";
    }
    if (jsContent != old_jscontent) {
        // Writes the javacript file on disk ****************
        fs.writeFile(javaScriptFullPath, jsContent, 'latin1', function (err) {
            if (err) return console.log(err);
        });
        console.info(javaScriptFullPath + ' ...  generated.');
    } else {
        console.info(javaScriptFullPath + ' ...  unchanged.');
    }  
}

//*********************************************************************************
// MAIN 
//*********************************************************************************

convert_cppfile("..\\TestcppMDE\\JsTestEngine.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\JavaScriptExtens.cpp", "..\\js");

//convert_cppfile("..\\TestcppMDE\\Widgets_Declar.cxx", "..\\js");
convert_cppfile("..\\TestcppMDE\\Widgets_Unit.cpp", "..\\js");

convert_cppfile("..\\TestcppMDE\\InitMDEandData.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\MdeUI_Unit.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\MdeUI_Listes_Unit.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\TFMde_VentesDevis.cpp", "..\\js");

//convert_cppfile("..\\TestcppMDE\\MdeUI_Menu_Declar.cxx", "..\\js");
//convert_cppfile("..\\TestcppMDE\\MdeUI_Ventes_Declar.cxx", "..\\js");
convert_cppfile("..\\TestcppMDE\\MdeUI_Menu_Unit.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\MdeUI_Ventes_Unit.cpp", "..\\js");
//convert_cppfile("..\\TestcppMDE\\MdeAppli_Declar.cxx", "..\\js");
convert_cppfile("..\\TestcppMDE\\MdeAppli_Unit.cpp", "..\\js");

convert_cppfile("..\\TestcppMDE\\U_Achats.cpp", "..\\js" );
convert_cppfile("..\\TestcppMDE\\U_Achats_Outils.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Bib.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Bib_Outils.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Chantier.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Commun.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Commun_Params.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Compta.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Initialisation.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_OS_Utils.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Ressources.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Stocks.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Tiers.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Ventes.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\U_Ventes_Outils.cpp", "..\\js");
convert_cppfile("..\\TestcppMDE\\Unit1.cpp", "..\\js");


// END OF FILE *****************************
