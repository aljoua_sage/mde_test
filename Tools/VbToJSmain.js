//*******************************************
// Traduction d'un VBScript en JavaScript
//*******************************************
var fs = require('fs');
var path = require('path');

//********************************************************************************

function vbsTojs(vbs) {

    var s = vbs;

    // Remplacement de Sub et fonction
    s = s.replace(/\bExit\s+Sub\b/gim, "return;");
    s = s.replace(/\bEnd\s+Sub\b/gim, "}");
    s = s.replace(/\bSub\s+(\w+)(\s*[^\(])/gim, "void $1() { $2");  // ajoute () apres les sub d�pourvu de ()
    s = s.replace(/\bSub\s+(\w+)/gim, "void $1");
    s = s.replace(/\bEnd\s+Function\b/gim, "}");
    s = s.replace(/\bFunction\s+(\w)/gim, "$1");

    // Declarations Dim
    s = s.replace(/\bDim\s+(\w+)/gim, "var $1;");

    // Remplacement du commentaire en bout de ligne (pr�c�d� d'au moins un blanc)
    s = s.replace(/(\s+)\'/gim, "$1//");

    // Ajout Point virgule en bout de ligne apr�s appel de fonction
    s = s.replace(/\)\s*\r/gm, ");\r");

    //Replace Call sans parenth�ses
    s = s.replace(/\bCall\s+(\w+)\s*\r/gim, "$1();\r");
    //Replace Call avec parenth�ses
    s = s.replace(/\bCall\s+(\w+)\s*(\(.*\))/gim, "$1$2;");

    //replace Set par rien
    s = s.replace(/\bSet\s+/gim, "");

    //Replace des constantes True, False
    s = s.replace(/\bTrue\b/gim, "true");
    s = s.replace(/\bFalse\b/gim, "false");
    s = s.replace(/\bConst\b/gim, "const");

    //replace du For - Next
    s = s.replace(/\bFor /gim, "for( ");
    s = s.replace(/\bNext\b/gim, "}");
    s = s.replace(/\bWhile\b/gim, "while( ");
    s = s.replace(/\bWend\b/gim, "}");
    s = s.replace(/\bExit\s+For\b/gim, "break;");

    //replace du Then
    s = s.replace(/ Then\b/gim, " ) {");
    s = s.replace(/\bElse\b/gim, "}else{");
    //replace du End if (avant le if)
    s = s.replace(/\bEnd\s+if\b/gim, "}");
    //replace du If 
    s = s.replace(/\bif /gim, "if( ");

    return s;
}



//********************************************************************************
function convert_file(vbscriptFullPath) {
    var vbsExt = path.parse(vbscriptFullPath).ext;
    var javaScriptFullPath = vbscriptFullPath.substring(0, vbscriptFullPath.length-vbsExt.length) + '.js';

    // Open the input VB script file *****************************
    var fileContents = "";
    try {
        fileContents = fs.readFileSync(vbscriptFullPath, 'ascii');
    } catch (err) {
        return console.error("Error reading vbscriptFile file " + vbscriptFullPath);
        return;
    }

    // CONVERT ******************************
    var jsContent = vbsTojs(fileContents);

    // Writes the javacript file on disk ****************
    fs.writeFile(javaScriptFullPath, jsContent, function (err) {
        if (err) return console.log(err);
    });
    console.info(javaScriptFullPath + ' ...  generated.');
}

//*********************************************************************************
// MAIN 
//*********************************************************************************
convert_file( "..\\VBscript\\U_Ventes.svb")

// END OF FILE *****************************
